var utils = utils || {};
var a;

(function ($) {
    utils = {
        scroll_top: function () {
            if ($(".scroll-top").length == 0) {
                $("body").append('<span class="scroll-top" style="display:none;">TOP</span>');
                $(".scroll-top").off("click").click(function () {
                    $("html,body").animate({"scrollTop": 0});
                })
            }
        },
        get_param: function (name) { //파라미터 값 반환
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },
        loading: {
            show: function () {
                if ($(".loading").length < 1) {
                    $("body").prepend("<div class='loading'>로딩</div>");
                }
                $(".loading").fadeIn("fast");
            },
            hide: function () {
                if ($(".loading").length > 0) {
                    $(".loading").fadeOut("fast", function () {
                        $(this).remove();
                    });
                }
            }
        },
        menu_events: function () {
            if ($("body").hasClass("mk-jobs")) {
                var page_url = $(location).attr('pathname');
                var tit = $(".jobs-site.mk-jobs .m-menu-area .custom-select.active>strong").text();
                if(page_url.indexOf("qna") !== -1){
                    tit = "고객센터";
                    if ($("body").hasClass("lang-en")) {
                        tit = "Customer service";
                    } else if ($("body").hasClass("lang-vi")) {
                        tit = "Dịch vụ khách hàng";
                    }
                }
                var html = "";
                html += '<div class="m-prev">';
                html += '    <a href="javascript:history.back();" class="ic-arrow">prev</a>';
                html += '    <span>'+tit+'</span>';
                html += '</div>';
                if ($(".main-slider").length == 0 && $("form[action *= 'job/auth/login.php']").length == 0) {
                    $(".vis-hd").after(html);
                }

            } else {
                //현재 메뉴 on
                var page_url = $(location).attr('pathname');
                var page_nm = page_url.split("/")[2];
                var page_nm02 = page_url.split("/")[3];
                var page_num = utils.get_param("type");


                if (page_nm == "jinoboard") {
                    if (page_url.indexOf("community") != -1) {
                        $(".vis-hd .custom-select>strong>a[href*='" + page_url + "']").closest(".custom-select").addClass("on");
                    } else {
                        $(".vis-hd .custom-select>strong>a[href*='" + page_nm + "']").closest(".custom-select").addClass("on");
                        $(".vis-hd .custom-select>strong>a[href*='community']").closest(".custom-select").removeClass("on");
                    }
                } else {
                    $(".vis-hd .custom-select>strong>a[href*='" + page_nm + "']").closest(".custom-select").addClass("on");
                }

                if (page_nm == "jinoboard") {
                    if (page_num != "") {
                        $(".crs-left-block>ul>li>a[href*='type=" + page_num + "']").parent().addClass("on");
                    } else {
                        $(".crs-left-block>ul>li>a").each(function () {
                            if ($.trim($(this).text()) == $.trim($("#page-local-jinoboard-detail .pg-tit").text())) {
                                $(this).parent().addClass("on");
                                return false;
                            }
                        });
                    }
                } else if (page_nm == "jinotechboard") {
                    if (utils.get_param("id") != "") {
                        $(".crs-left-block>ul>li>a[href*='id=" + utils.get_param("id") + "']").parent().addClass("on");
                    } else {
                        $(".crs-left-block>ul>li>a").each(function () {
                            if ($.trim($(this).text()) == $.trim($("#page-mod-jinotechboard-write .board-title").text())) {
                                $(this).parent().addClass("on");
                                return false;
                            }
                        });
                    }
                } else {
                    if (page_nm02 != null && page_nm02.indexOf("mycourse") != -1) {
                        page_url = "mycourse";
                    }
                    $(".crs-left-block>ul li a[href*='" + page_url + "']").parent().addClass("on");
                }
                if (parseInt(page_num) < 4) {
                    $(".vis-hd .custom-select").removeClass("on");
                }

                var m_sub_txt = $(".vis-hd .custom-select.on>strong").text();
                if ($(".vis-hd .custom-select.on>strong").text() == "" || parseInt(page_num) < 4) {
                    //m_sub_txt = $(".crs-left-block>ul>li.on>a").text();
                    m_sub_txt = "MY";

                    if ($(".tp-search-area").length > 0) {
                        m_sub_txt = "검색";
                        if ($("body").hasClass("lang-en")) {
                            m_sub_txt = "search";
                        } else if ($("body").hasClass("lang-vi")) {
                            m_sub_txt = "Tìm kiếm";
                        }
                    } else if (parseInt(page_num) < 4 && utils.get_param("b_t") === "custom") {
                        m_sub_txt = "고객센터";
                        if ($("body").hasClass("lang-en")) {
                            m_sub_txt = "customer service";
                        } else if ($("body").hasClass("lang-vi")) {
                            m_sub_txt = "Dịch vụ khách hàng";
                        }
                    } else {
                        if (!$("body").hasClass("pagelayout-frontpage")) {
                            var idx = $(".crs-left-block>ul li.on").index();
                            $(".vis-hd .custom-select.no-pc").addClass("selected");
                            $(".vis-hd .custom-select.no-pc>ul>li").eq(idx).addClass("on");
                        }
                    }

                    if ($("body").hasClass("pagelayout-join02")) {
                        m_sub_txt = $(".login-bx .center-tit").text();
                    }

                }
                if (m_sub_txt != "") {
                    $(".m-prev span").text(m_sub_txt);
                }
            }



            //모바일 메뉴 이벤트
            $(".vis-hd .m-menu").click(function () {
                $(".m-menu-area").addClass("on");
                $(".m-menu-area").after("<div class='popbg'></div>");
                $(".vis-hd .popbg").fadeIn("fast");
                if ($("body").hasClass("pagelayout-frontpage")) {
                    $(".main-contents").css({"height": window.innerHeight - $(".main-slider").outerHeight() - 140, "overflow": "hidden"});
                } else {
                    $(".cont").css({"height": window.innerHeight - 140, "overflow": "hidden"});
                }
                $(".popbg").click(function () {
                    $(".m-menu-area").removeClass("on");
                    $(".popbg").fadeOut(function () {
                        $(this).remove();
                        $(".cont, .main-contents").css({"height": "auto", "overflow": "auto"});
                    });
                });
            });
            //강의실 모바일 이벤트
            $(".course-layout .vis-hd .m-menu").off("click").click(function () {
                if (!$(".crs-left-block").hasClass("on")) {
                    $(".crs-left-block").addClass("on");
                    $(".crs-left-block").after("<div class='popbg course'></div>");
                    $(".popbg.course").fadeIn("fast");
                    $(".cont").css({"height": window.innerHeight - 100, "overflow": "hidden"});
                    $(".popbg.course").click(function () {
                        $(".crs-left-block").removeClass("on");
                        $(".popbg.course").fadeOut(function () {
                            $(this).remove();
                            $(".cont, .main-contents").css({"height": "auto", "overflow": "auto"});
                        });
                    });
                } else {
                    $(".crs-left-block").removeClass("on");
                    $(".popbg.course").fadeOut(function () {
                        $(this).remove();
                        $(".cont, .main-contents").css({"height": "auto", "overflow": "auto"});
                    });
                    $(".popbg.course").off("click");
                }

            });
            $(".vis-hd .m-close").click(function () {
                $(".m-menu-area").removeClass("on");
                $(".vis-hd .popbg").fadeOut(function () {
                    $(this).remove();
                    $(".cont, .main-contents").css({"height": "auto", "overflow": "auto"});
                });
            });
            $(".vis-hd .custom-select>strong").click(function () {
                if (window.innerWidth < 1025) {
                    if (!$(this).siblings("ul").is(":visible") && $(this).parent().hasClass("no-pc")) {
                        $(this).parent().siblings().find(">ul").slideUp("fast");
                        $(this).siblings("ul").slideDown("fast", function () {
                            $(".m-scrl").scrollTop($(".vis-hd .custom-select.no-pc").position().top);
                        });

                    } else {
                        $(this).siblings("ul").slideUp("fast");
                    }
                }
            });
            //헤더 개인정보 이벤트
            $(".vis-hd .user-info>p").off("click").click(function () {
                var target = $(this).siblings("ul");
                if (!target.is(":visible")) {
                    target.slideDown(300);
                    if ($(".my-wrap").length == 0) {
                        $("body").append("<div class='my-wrap'></div>");
                    }
                    $(".my-wrap").fadeIn();
//                    if (utils.popup.bodyscroll()) {
//                        $("body").addClass("noScroll");
//                    }

                    $(document).on("mouseup touchend", function (e) {
                        if ($(e.target).closest(".user-info").length == 0 && !$(e.target).hasClass(".user-info")) {
                            target.slideUp(300);
                            $(".my-wrap").fadeOut();
                            $(this).off("mouseup");
                            $(this).off("touchstart");
                            $("body").removeClass("noScroll");
                        }
                    });


                } else {
                    target.slideUp(300);
                    $(".my-wrap").fadeOut();
                    $("body").removeClass("noScroll");
                }
            });
            //헤더 검색버튼 이벤트
            $(".vis-hd .search-evt").off("click").click(function () {
                var target = $(".hd-search-area");
                if (!target.hasClass("on")) {
                    target.addClass("on");
                    if ($(".my-wrap").length == 0) {
                        $("body").append("<div class='my-wrap'></div>");
                    }
                    $(".my-wrap").fadeIn();
                    if (utils.popup.bodyscroll()) {
                        //$("body").addClass("noScroll");
                    }
                    $(document).on("mouseup touchend", function (e) {
                        if ($(e.target).closest(".search-evt").length == 0 && !$(e.target).hasClass(".hd-search-area") && $(e.target).closest(".hd-search-area").length == 0.) {
                            target.removeClass("on");
                            $(".my-wrap").fadeOut();
                            $(this).off("mouseup");
                            $(this).off("touchstart");
                            $("body").removeClass("noScroll");
                        }
                    });

                } else {
                    target.removeClass("on");
                    $(".my-wrap").fadeOut();
                    $("body").removeClass("noScroll");
                }
            });
            $(".hd-search-area>.close-search").click(function () {
                $(".hd-search-area").removeClass("on");
                $(".my-wrap").fadeOut();
                $("body").removeClass("noScroll");
            });
            //헤더 알람 이벤트
            $(".vis-hd .f-r>div.alram>a").off("click").click(function () {
                var target = $(this).siblings(".alram-list");
                if (!target.is(":visible")) {
                    target.slideDown(300);
                    if ($(".my-wrap").length == 0) {
                        $("body").append("<div class='my-wrap'></div>");
                    }
                    $(".my-wrap").fadeIn();
                    if (utils.popup.bodyscroll()) {
                        //$("body").addClass("noScroll");
                    }
                    $(document).on("mouseup touchend", function (e) {
                        if ($(e.target).closest(".alram").length == 0 && !$(e.target).hasClass(".alram")) {
                            target.slideUp(300);
                            $(".my-wrap").fadeOut();
                            $(this).off("mouseup");
                            $(this).off("touchstart");
                            $("body").removeClass("noScroll");
                        }
                    });


                } else {
                    target.slideUp(300);
                    $(".my-wrap").fadeOut();
                    $("body").removeClass("noScroll");
                }
            });
            //서브페이지 메뉴 이벤트
            $(".page-nav>.sub-mn>span").click(function () {
                if (!$(this).siblings("ul").is(":visible")) {
                    $(".page-nav>.sub-mn>ul>li").removeClass("on");
                    $(this).siblings("ul").fadeIn("fast");
                    utils.body_close($(".page-nav>.sub-mn>ul"), ".sub-mn", "", true);

                } else {
                    $(this).siblings("ul").fadeOut("fast");
                }

            });



        }
        , sub_slider_event: {
            slick: function () {
                try {
                    var page_url = $(location).attr('pathname');
                    //서브페이지 모바일 슬라이더
                    $('body:not(.course-layout) .crs-left-block>ul').slick({
                        infinite: false,
                        arrow: true,
                        dots: false,
                        slidesToShow: 4,
                        variableWidth: false,
                        initialSlide: $(".crs-left-block>ul li.on").index(),
                        lazyLoad: 'progressive',
                        responsive: [
                            {
                                breakpoint: 768,
                                settings: 'slick',
                                initialSlide: $(".crs-left-block>ul li.on").index()
                            },
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 2,
                                    initialSlide: $(".crs-left-block>ul li.on").index()
                                }
                            },
                        ]
                    });
                } catch (e) {
                }
            }
            , un_slick: function () {
                try {
                    $('.crs-left-block>ul').slick('unslick');
                } catch (e) {
                }

            }

        }
        , slider_event: function () {

            //메인 슬라이더 이벤트
            try {
                $('.main-slider').slick({
                    autoplay: true,
                    dots: true,
                    autoplaySpeed: 5000,
                    pauseOnHover: true,
                });

            } catch (e) {
            }
            //메인 학습중인 강좌
            try {
                $('.learn-list').slick({
                    autoplay: true,
                    autoplaySpeed: 5000,
                    pauseOnHover: true,
                    lazyLoad: 'progressive'
                });
            } catch (e) {
            }
            
            //추천강좌 슬라이더 이벤트
            try{
                $('.thumb-list.style01.slider').slick({
                    infinite: false,
                    arrow: true,
                    dots: true,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    variableWidth: false,
                    outerEdgeLimit: false,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                                dots: false
                            }
                        }
                    ]
                });
            }catch(e){}
            
            //인기채용정보 슬라이더 이벤트
            try{
                var slidesToShow = [4,3,1];
                var sliderNum = $(".cmpny-bx.slider>li").length;
                if(sliderNum < slidesToShow[0]){
                    slidesToShow[0] = sliderNum;
                }
                if(sliderNum < slidesToShow[1]){
                    slidesToShow[1] = sliderNum;
                }
                $('.cmpny-bx.slider').slick({
                    infinite: false,
                    arrow: true,
                    dots: true,
                    slidesToShow: slidesToShow[0],
                    slidesToScroll: slidesToShow[0],
                    variableWidth: false,
                    outerEdgeLimit: false,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: slidesToShow[1],
                                slidesToScroll: slidesToShow[1]
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: slidesToShow[2],
                                slidesToScroll: slidesToShow[2],
                                dots: false
                            }
                        }
                    ]
                });
            }catch(e){}
            
            try{
                $('.jobs-cmpny-list.slider-evt').slick({ 
                    infinite: false,
                    arrow: true,
                    dots: true,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    variableWidth: false,
                    outerEdgeLimit: false,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                                dots: false
                            }
                        }
                    ]
                });
            }catch(e){}
            
            
            try{
                $('.jobs-cmpny-list.slider-evt').slick({ 
                    infinite: false,
                    arrow: true,
                    dots: true,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    variableWidth: false,
                    outerEdgeLimit: false,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                                dots: false
                            }
                        }
                    ]
                });
            }catch(e){}


            //구인구직 메인 썸네일 슬라이더
            try {
                $('.ply-list.slider>ul').slick({
                    infinite: false,
                    arrow: true,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    variableWidth: false,
                    outerEdgeLimit: false,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            } catch (e) {
            }
            
            //강좌 상세 탭 모바일 이벤트
            try {
                $(".crs-tab").addClass("li-num" + $(".crs-tab li").length);
                var tab_num = $(".crs-tab li").length;
                $('.crs-tab').slick({
                    infinite: false,
                    arrow: false,
                    dots: false,
                    slidesToShow: tab_num,
                    variableWidth: false,
                    outerEdgeLimit: false,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 3,
                                arrow: true
                            }
                        }
                    ]
                });
            } catch (e) {

            }
            //메인 공지 이벤트
            try {
                $('.noti-list').slick({
                    infinite: true,
                    arrow: false,
                    dots: false,
                    autoplay: true,
                    slidesToShow: 1,
                    autoplaySpeed: 3000,
                    pauseOnHover: true,
                    vertical: true
                });
            } catch (e) {
            }

        }, popup: {
            close_pop: function (target) { // 팝업 닫기 이벤트
                //window.parent.postMessage("OpenTabs", "*"); //하단메뉴 보이기.
                if (target.is(".popbg")) {
                    target.fadeOut();

                    var thisLayer = $(".layerpop").last();
                    thisLayer.fadeOut('fast', function () {
                        target.remove();
                        thisLayer.remove();
                        if ($(".layerpop:not(.event-pop)").length == 0) {
                            $("body").css({
                                "overflow": "auto",
                                "padding-right": 0
                            });
                        }
                    });

                } else {
                    target.closest(".layerpop").fadeOut('fast', function () {
                        target.closest(".layerpop").remove();
                    });
                    var thisBg = $(".popbg").last();
                    thisBg.fadeOut("", function () {
                        $(this).remove();
                        thisBg.remove();
                        if ($(".layerpop:not(.event-pop)").length == 0) {
                            $("body").css({
                                "overflow": "auto",
                                "padding-right": 0
                            });
                        }
                    })

                }
                $(".main-wrap").css("max-height", "none");

                try {
                    target.off("click");
                } catch (e) {
                }
            },
            popup_position: function (width, height) { //팝업 위치 조절
                var top = (window.innerHeight - height) / 2;
                if (top <= 0) {
                    top = "10px";
                }
                var left = "calc(50% - " + width + " / 2)";
                return {
                    top: top,
                    left: left
                }
            },
            bodyscroll: function () {
                var windowH = window.innerHeight;
                var scrollH = $("body").prop("scrollHeight");
                var scrollOn = true;
                if (windowH == scrollH || utils.check_mobile()) {
                    scrollOn = false;
                }
                return scrollOn;

            },
            call_windowpop: function (url, w, h, name) {
                var curX = window.screenLeft;
                var curY = window.screenTop;
                var curWidth = document.body.clientWidth;
                var curHeight = document.body.clientHeight;

                var nLeft = curX + (curWidth / 2) - (w / 2);

                var param = 'status=no,width=' + w + ', left=' + nLeft;
                if (h != "") {
                    var nTop = curY + (curHeight / 2) - (h / 2);
                    param += ', height=' + h + ', top=20';
                } else {
                    var nTop = curY + (curHeight / 2) - (800 / 2);
                    param += ', height=' + 800 + ',top=20';
                }
                var pop = window.open(url, name, param);
                return pop;
                //pop.focus();
            },
            call_layerpop: function (url, settings, param) { //레이어팝업 콜 이벤트
                window.parent.postMessage("CloseTabs", "*"); //하단메뉴 가리기.
                var inlitParam = {
                    width: "60%",
                    height: "60%",
                    bg: true,
                    method: "get",
                    callbackFn: function () {}
                };
                var setting = $.extend(inlitParam, settings);
                if (param == null) {
                    param = "";
                }

                $.ajax({
                    url: url,
                    data: param,
                    type: setting.method,
                    success: function (result) {
                        var data = $(result).addClass("wrap");

                        if ($(result).find(".layerpop").length > 0 || $(result).is(".layerpop")) {
                            if ($(result).find(".layerpop").length > 0) {
                                data = $(result).find(".layerpop");
                            }


                            if (setting.bg && $('div[data-url="' + url + '"]').length == 0) {
                                $("body").append("<div class='popbg' data-url='" + url + "'></div>");
                                $(".popbg").fadeIn();

                            } else {
                                $(".layerpop").last().css("position", "absolute");
                            }
                            if ($("div[data-pop-url='" + url + "']").length == 0) {
                                $("body").append(data);
                                $(".layerpop").last().attr("data-pop-url", url);
                            }
                            $(".layerpop").find(".pop-close").attr("href", "javascript:void(0);");

                            setting.callbackFn();
                            var thisLayer = $(".layerpop").last();

                            thisLayer.css({
                                "width": setting.width,
                                "height": setting.height
                            });
                            var leftTop = utils.popup.popup_position(setting.width, thisLayer.outerHeight());
                            if (utils.check_mobile()) {
                                var leftTop = utils.popup.popup_position(setting.width, thisLayer.height() + 70);
                            }
                            var top = leftTop.top;
                            var overflow = "hidden",
                                    position = "fixed",
                                    mg_bt = "10px";
                            if (parseInt(leftTop.top) < 15) {
                                overflow = "auto";
                                position = "absolute";
                                //top = 10 + $(window).scrollTop();
                                thisLayer.css("max-height", "none");
                                $(".main-wrap").css("max-height", window.innerHeight);

                            }
                            //예외처리
                            if (thisLayer.find(".bg-box").length != 0) {
                                overflow = "auto";
                                position = "absolute";
                            }
                            if (utils.check_mobile()) {
                                mg_bt = "70px";
                            }
                            if (setting.bg == true) {
                                if (utils.popup.bodyscroll()) {
                                    $("body").css({
                                        "padding-right": "17px"
                                    });
                                }
                                $("body").css({
                                    "overflow": overflow
                                });
                            }
                            thisLayer.css({
                                "top": top,
                                "left": leftTop.left,
                                "position": position,
                                "margin-bottom": mg_bt
                            }).fadeIn('');
                            if (window.innerWidth < 769 && thisLayer.css("position") === "absolute") {
                                $("html,body").scrollTop("0");
                            }

                            $(".layerpop .pop-close, .popbg").click(function (e) {
                                utils.popup.close_pop($(this));
                            });
                            if ($(result).find(".layerpop").hasClass("info_confirm")) {
                                $(".popbg").off("click");
                            }

                        } else if ($(result).hasClass("bbsPop")) {
                            var result = $(result).hide();
                            $("body").append(result);
                            $(result).fadeIn();
                            $(".bbsPopTitle a").click(function () {
                                $(this).closest(".bbsPop").fadeOut("fast", function () {
                                    $(this).remove();
                                });
                            });
                            setting.callbackFn();
                            try {
                                $('.scrtBtn').swipe({
                                    swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                                        $(this).fadeOut();
                                    },
                                    threshold: 0
                                });
                            } catch (e) {

                            }
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //alert(textStatus + " : " + errorThrown);
                        return false;
                    }
                });

            },
            call_confirm: function (title, contents, button, callbackFn) {
                if (button == null) {
                    button = ""
                }
                ;
                if (callbackFn == null) {
                    callbackFn = function () {};
                }

                var pop = "";
                pop += '<div class="layerpop" data-url="confirm">';
                pop += '<div class="pop-title bg">';
                pop += title;
                pop += '<span class="close">닫기</span>';
                pop += '</div>';
                pop += ' <div class="pop-contents">';
                pop += '<div class="txt text-center">';
                pop += contents;
                pop += ' </div>';
                pop += '<div class="text-center btn-group">';
                pop += button;
                pop += ' </div>';
                pop += ' </div></div>';



                if ($('div[data-url="confirm-bg"]').length == 0) {
                    $("body").append("<div class='popbg' data-url='confirm-bg'></div>");
                    $(".popbg").fadeIn();

                } else {
                    $(".layerpop").last().css("position", "absolute");
                }
                $("body").append(pop);

                callbackFn();
                var thisLayer = $(".layerpop").last();

                thisLayer.css({
                    "width": 390,
                    "height": "auto"
                });
                var leftTop = utils.popup.popup_position('340px', thisLayer.height());
                if (utils.check_mobile()) {
                    var leftTop = utils.popup.popup_position('340px', thisLayer.height() + 70);
                }
                var top = leftTop.top;
                var overflow = "hidden",
                        position = "fixed",
                        mg_bt = "10px";
                if (leftTop.top == "10px") {
                    overflow = "auto";
                    position = "absolute";
                    thisLayer.css("max-height", "none");
                    $(".main-wrap").css("max-height", window.innerHeight);
                }
                if (utils.check_mobile()) {
                    mg_bt = "70px";
                }
                if (utils.popup.bodyscroll()) {
                    $("body").css({
                        "padding-right": "17px"
                    });
                }
                $("body").css({
                    "overflow": overflow
                });
                thisLayer.css({
                    "top": top,
                    "left": leftTop.left,
                    "position": position,
                    "margin-bottom": mg_bt
                }).fadeIn('');

                $(".layerpop .pop-close, .popbg").click(function () {
                    utils.popup.close_pop($(this));
                });
                $(".layerpop *").on("click", function () {
                    var leftTop = utils.popup.popup_position('340px', thisLayer.outerHeight());
                    thisLayer.css({
                        "top": leftTop.top,
                        "left": leftTop.left
                    });
                });

            }
        },
        body_close: function ($target, $parents, clss, fade) {
            if (fade == null) {
                fade = false;
            }
            if (clss == null) {
                clss = "";
            }
            $(document).on("mouseup touchend", function (e) {
                if ($(e.target).closest($parents).length == 0) {
                    if (clss == "") {
                        if (!fade) {
                            $target.hide();
                        } else {
                            $target.fadeOut("fast");
                        }
                    } else {
                        $target.removeClass(clss);
                    }
                    $(this).off("mouseup");
                    $(this).off("touchstart");
                }
            });
        },
        check_mobile: function () { //모바일 true, 피씨 false 반환
            var filter = "win16|win32|win64|macintel|mac";
            if (navigator.platform) {
                if (0 > filter.indexOf(navigator.platform.toLowerCase())) {
                    return true;
                } else {
                    return false;
                }
            }
        },
        call_sync_t: function () {
            try {
                window.Android.getAppInfo();
            } catch (err) {

            }
        },
        star_event: function () {
            $(".star-event").each(function () {
                var on_num = $(this).attr("data-num");
                for (var i = 0; i < 5; i++) {
                    var clss = "";
                    if (i < on_num) {
                        clss = "on"
                    }
                    $(this).append("<span class='" + clss + "'></span>");
                }
            });

            $(".star-event.hover>span").hover(function () {
                var idx = $(this).index();
                for (var i = 0; i <= 5; i++) {
                    if (i <= idx) {
                        $(this).parent().find("span").eq(i).addClass("on");
                    } else {
                        $(this).parent().find("span").eq(i).removeClass("on");
                    }
                }
            }, function () {
                var num = $(this).parent().attr("data-num");
                for (var i = 0; i <= 5; i++) {
                    if (i < num) {
                        $(this).parent().find("span").eq(i).addClass("on");
                    } else {
                        $(this).parent().find("span").eq(i).removeClass("on");
                    }
                }
            });
            $(".star-event.hover>span").click(function () {
                var idx = $(this).index() + 1;
                $(this).parent().attr("data-num", idx);
            });
        },
        barchart_event: function () {
            $(".bar-event").each(function () {
                var per = $(this).attr("data-num");
                $(this).find("span").outerWidth(per);
            });
        },
        is_banner: function () {
            //메인 배너 있는경우 클래스 추가
            var bn = $(".ft-banner").length;
            if (bn > 0 && $(".ft-banner").is(":visible")) {
                $("body").addClass("is-banner");
            } else {
                $("body").removeClass("is-banner");
            }
            $(".ft-banner .close-bn").off("click").click(function () {
                $(".ft-banner").fadeOut("fast");
                $("body").removeClass("is-banner");
                return false;
            });
        },
        top_position: function () {
            if (window.innerWidth < 789 && $(".m-arrow").length != 0) {
                if (!$(".m-arrow").parent().hasClass("on")) {
                    var bt = $(".m-arrow").parent().outerHeight() + 35;
                    $(".cont .group").css("padding-bottom", 140);
                    $(".scroll-top").css("bottom", bt);
                } else {
                    var bt = $(".m-arrow").parent().outerHeight() + 10;
                    $(".cont .group").css("padding-bottom", parseInt($(".sel-crs-list").outerHeight()) + 40);
                    $(".scroll-top").css("bottom", bt);
                }
            }
        },
        common_event: function () {
            //구인구직 언어변경 클래스 추가
            if ($("html").attr("lang") == "vi") {
                $("body").addClass("lang-vi")
            } else if ($("html").attr("lang") == "en") {
                $("body").addClass("lang-en")
            }
            if (utils.get_param("mode") != "edit") {
                setTimeout(function () {
                    $("#page-local-jinoboard-write #id_title").val("");
                }, 1000);
                $(".thumb-board>li .txt .tit a .ic-new").parent().addClass("has-new");
            }

            $(".sel-crs-list, .bk-detail .bk-txt>p").append("<span class='m-arrow'></span>");
            $(".sel-crs-list, .bk-txt").closest(".group").addClass("c-detail");
            $(".m-arrow").click(function () {
                if ($(this).parent().hasClass("on")) {
                    $(this).parent().removeClass("on");
                    var bt = $(this).parent().outerHeight() + 35;
                    $(".cont .group").css("padding-bottom", 140);
                    $(".scroll-top").css("bottom", bt);
                } else {
                    $(this).parent().addClass("on");
                    var bt = $(this).parent().outerHeight() + 10;

                    $(".cont .group").css("padding-bottom", parseInt($(".sel-crs-list").outerHeight()) + 40);
                    $(".scroll-top").css("bottom", bt);
                }
            });
            var w = 100 / $("#page-local-jinoboard-index2 .mk-c-tab.col4>li").length;
            $("#page-local-jinoboard-index2 .mk-c-tab>li").css("width", w + "%");
            var w = 100 / $("body[id *= 'page-local-mypage-mycourse'] .mk-c-tab>li").length;
            $("body[id *= 'page-local-mypage-mycourse'] .mk-c-tab>li").css("width", w + "%");
            //스크롤 탑 이벤트
            this.scroll_top();
            //메인 베너 이벤트
            this.is_banner();
            //custom select
//            $(".custom-select > strong").off("click").click(function () {
//                $(this).parent().siblings(".custom-select").removeClass("on")
//                $(this).parent().toggleClass("on");
//                utils.body_close($(this).parent(), ".custom-select", "on");
//            });
            $(".custom-select>ul>li, .page-nav>.sub-mn>ul>li").hover(function () {
                if (window.innerWidth > 1024) {
                    $(this).parent().addClass("hover");
                }
            }, function () {
                if (window.innerWidth > 1024) {
                    $(this).parent().removeClass("hover");
                }
            });
            $(".page-nav>.sub-mn>ul>li>a").click(function () {
                $(this).parent().addClass("on").siblings().removeClass("on");
                if (window.innerWidth < 1025) {
                    $(this).parent().siblings().removeClass("selected");
                }
            });

//            $(".custom-select>ul>li").click(function(){
//                var txt = $(this).text();
//                $(this).addClass("selected").siblings().removeClass("selected");
//                $(this).closest(".custom-select").children("strong").text(txt);
//                $(this).closest(".custom-select").removeClass("on")
//            });
            //썸네일 하트 이벤트
            /*
             $(document).on("click", ".thumb-list.style01>li .ic-like", function () {
             var cid = $(this).attr('data-target');
             $.ajax({
             url: "/local/lmsdata/like.ajax.php",
             type: 'POST',
             dataType: 'json',
             data: {classid: cid
             },
             success: function (data) {
             var target = $('.heart' + cid);
             target.html(data.score);
             
             if (data.value > 1) {
             $("#like" + cid).addClass("on");
             } else {
             $("#like" + cid).removeClass("on");
             }
             },
             error: function (jqXHR, textStatus, errorThrown) {
             console.log(jqXHR.responseText);
             }
             });
             });*/
            //탭이벤트
            $(document).on("click", ".tab-event > *", function () {
                var $class = "on";
                var this_target = $($(this).attr("data-target"));
                $(this).addClass($class);
                $(this).siblings().removeClass($class);
                this_target.addClass($class);
                this_target.siblings().removeClass($class);
                if($(this).parents(".bar-tab-grp").length != 0){
                    return false;
                }
            });
            //강좌상세 탭 이벤트
            $(document).on("click", ".crs-tab.tab-event li", function () {
                var $class = "on";
                var this_target = $($(this).attr("data-target"));
                $(this).closest(".crs-tab").find("li").removeClass($class);
                $(this).addClass($class);

                this_target.addClass($class);
                this_target.siblings().removeClass($class);

                if ($(this).index() == 0) {
                    $(".crs-tab-cont ~ .pg-tit, .crs-tab-cont ~ .crs-bx").show();
                } else {
                    $(".crs-tab-cont ~ .pg-tit, .crs-tab-cont ~ .crs-bx").hide();
                }
                return false;
            });
            //toggle
            $(".toggle-event > *").click(function () {
                $(this).toggleClass("on");
                $(this).siblings().removeClass("on");
            });
            //custom file
            var tg = '.custom-file-box input[type=file], .bbsFile2 input[type=file], .bssFile input[type=file]';
            $(document).on('change', tg, function () {
                if (window.FileReader) {
                    var filename = $(this)[0].files[0].name;
                } else {
                    var filename = $(this).val().split('/').pop().split('\\').pop();
                }
                $(this).parent().find('input[type=text]').val(filename);
            });

            //윈도우팝업 닫기이벤트
            $(".wrap.layerpop .close, .window.layerpop .close").click(function () {
                window.close();
            });
            //메인팝업 닫기 이벤트
            $(".layerpop.main-pop .pop-close").click(function () {
                $(this).closest(".layerpop").fadeOut("fast", function () {
                    $(this).remove();
                });
            });

            //전체검색페이지 position 지정
            if ($(".tp-search-area").length > 0) {
                $(".cont").css("position", "relative");
                $(".cont>.group").css("position", "static");
            }

            //수강완료강좌 클래스 추가
            $(".thumb-list.style04.course li .wp .img .lb").closest("li").addClass("hastp");
            //그래프 있는 썸네일 클래스 추가
            $(".thumb-list.style04.course .bar-area").closest("li").addClass("hasgp");

            //강좌 상세 모바일 강좌타이틀 추가
            var c_tit = $(".course-info .crs-txt .tit").text();
            $(".course-info .crs-img").before("<div class='m-tit'>" + c_tit + "</div>");
            //강좌 상세 별점, sns공유 위치 변경
            if (window.innerWidth <= 768) {
                var star = $(".course-info .crs-txt .star-area");
                var share = $(".page-nav.has-icon .f-r");
                $(".course-info .m-tit").append(star);
                $(".course-info .m-tit").append(share);
            } else {
                var star = $(".course-info .m-tit .star-area");
                var share = $(".course-info .m-tit .f-r");
                $(".course-info .crs-txt .tit").after(star);
                $(".page-nav.has-icon").append(share);
            }
            //검색 상세 더보기 버튼 위치 변경
            if (window.innerWidth <= 768) {
                $(".btn-more").each(function () {
                    $(this).prev(".crs-bx").append($(this));
                });
            } else {
                $(".crs-bx .btn-more").each(function () {
                    $(this).parent().after($(this));
                });
            }
            //교재 리스트 관련강좌 위치 변경
            if (window.innerWidth <= 768) {
                $(".crs-list > li .txt").each(function () {
                    $(this).after($(this).find(".m-out"));
                });

            } else {
                $(".crs-list > li").each(function () {
                    $(this).find(".txt").append($(this).find(".m-out"));
                });
            }
            utils.top_position();

            var my_course_num = $(".my-box.half .my-crs-list ul>li").length;
            if (my_course_num > 4) {
                $(".my-box.half").addClass("num" + my_course_num);
            }

            //테이블 정렬 이벤트
            $("th .ic-sort").closest("th").addClass("th-sort");
            $(".th-sort").click(function () {
                $(this).find(".ic-sort").toggleClass("down");
            });

            //숫자만 입력
            $(".dt-grp input").keyup(function (e) {
                $(this).val($(this).val().replace(/[^0-9]/g, ""));
            });
            //구인구직 내정보 이력서현황 리스트 위치 변경
            if (window.innerWidth <= 768) {
                $(".resume-bx>ul").append($(".resume-bx>ul>li span.ic-file").parent());
                $(".resume-bx>ul").append($(".resume-bx>ul>li span.ic-video").parent());
            } else {
                $(".resume-bx>ul>li:eq(0)").after($(".resume-bx>ul>li span.ic-file").parent());
                $(".resume-bx>ul>li:eq(2)").after($(".resume-bx>ul>li span.ic-video").parent());
            }
            //구인구직 채용공고 상세 모바일 지금지원하기 버튼 위치 변경
            if($(".mk-jobs.jobs-site .btn-md-float").length != 0){
                $("body").addClass("has-float-btn")
            }
            if($("body").hasClass("pagelayout-edu-full") || $(".mk-jobs.jobs-site .img-grp").length > 0){
                $("body").addClass("evt-page");
                $(".m-ft-link li a").attr("target","")
                $(".m-prev span").text("event");
                $(".m-ft-link li").eq(0).find("a").attr("href", $(".link-tp li").eq(0).find("a").attr("href"));
                $(".m-ft-link li").eq(1).find("a").attr("href", $(".link-tp li").eq(1).find("a").attr("href"));
            }
            

        },
        main_pop_responsive: function () {
            $(".layerpop.main-pop").each(function () {
                var left = parseInt($(this).css("left"));
                var w = parseInt($(this).outerWidth());
                if ($("body").outerWidth() < left + w) {
                    $(this).css({"left": "auto", "right": "10px"});
                }
            });
        },
        init: function () {
            this.barchart_event();
            this.menu_events();
            this.slider_event();
            this.star_event();
            this.common_event();
            this.main_pop_responsive();
            if (window.innerWidth <= 768) {
                utils.sub_slider_event.slick();
            }
            $("a[href='#none']").click(function () {
                return false;
            });

        }

    }

    $(function () {
        try {
            $("body").css("visibility", "hidden");

            document.fonts.ready.then(function () {
                utils.init();
                $("body").css("visibility", "visible");
            });

        } catch (e) {
            $("body").css("visibility", "visible");
            utils.init();

        } finally {
            setTimeout(function () {
                $("body").css("visibility", "visible");
            }, 3000)
        }
    });
    var old_scr, tm, tm02;
    $(window).scroll(function () {
        utils.is_banner();

        //페이지 상단 이동 버튼 이벤트
        var scr = $(window).scrollTop();
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if (window.innerWidth >= 1025) {
            if (scr > 50) {
                $(".scroll-top").fadeIn("fast")
            } else {
                $(".scroll-top").fadeOut("fast")
            }
        }
        //모바일 하단 버튼 이벤트
        if (tm != null) {
            clearTimeout(tm);
        }
        if (window.innerWidth < 1025 && (scrollHeight - scrollPosition) / scrollHeight > 0.15) {
            if (scr > 20) {
                $("body").addClass("on-button");
                $(".m-ft-link").fadeIn("fast");
                $(".scroll-top").fadeIn("fast");
                utils.top_position();
            }

            tm = setTimeout(function () {
                $(".m-ft-link").fadeOut("fast");
                $("body").removeClass("on-button");
                $(".scroll-top").fadeOut("fast");
                utils.top_position();
            }, 3000);
        }

        if ((scrollHeight - scrollPosition) / scrollHeight < 0.18) {
            clearTimeout(tm);
            tm = setTimeout(function () {
                $(".m-ft-link").fadeOut("fast");
                $("body").removeClass("on-button");
                $(".scroll-top").fadeOut("fast");
                utils.top_position();
            }, 3000);
            /*clearTimeout(tm);
             $(".scroll-top").hide();
             $(".m-ft-link").hide();
             $("body").removeClass("on-button");
             utils.top_position();*/
        }
        old_scr = scr;

    });
    var slideON = false;
    $(window).resize(function () {

        //서브페이지 lnb 모바일 슬라이더 이벤트 제어
        if (window.innerWidth <= 768) {
            if (slideON == false) {
                utils.sub_slider_event.slick();
                slideON = true;
            }

        } else {
            utils.sub_slider_event.un_slick();
            slideON = false;
        }

        //강좌 상세 별점, sns공유 위치 변경
        if (window.innerWidth <= 768) {
            var star = $(".course-info .crs-txt .star-area");
            var share = $(".page-nav.has-icon .f-r");
            $(".course-info .m-tit").append(star);
            $(".course-info .m-tit").append(share);
        } else {
            var star = $(".course-info .m-tit .star-area");
            var share = $(".course-info .m-tit .f-r");
            $(".course-info .crs-txt .tit").after(star);
            $(".page-nav.has-icon").append(share);
        }
        //검색 상세 더보기 버튼 위치 변경
        if (window.innerWidth <= 768) {
            $(".btn-more").each(function () {
                $(this).prev(".crs-bx").append($(this));
            });
        } else {
            $(".crs-bx .btn-more").each(function () {
                $(this).parent().after($(this));
            });
        }
        //교재 리스트 관련강좌 위치 변경
        if (window.innerWidth <= 768) {
            $(".crs-list > li .txt").each(function () {
                $(this).after($(this).find(".m-out"));
            });

        } else {
            $(".crs-list > li").each(function () {
                $(this).find(".txt").append($(this).find(".m-out"));
            });
        }
        //구인구직 내정보 이력서현황 리스트 위치 변경
        if (window.innerWidth <= 768) {
            $(".resume-bx>ul").append($(".resume-bx>ul>li span.ic-file").parent());
            $(".resume-bx>ul").append($(".resume-bx>ul>li span.ic-video").parent());
        } else {
            $(".resume-bx>ul>li:eq(0)").after($(".resume-bx>ul>li span.ic-file").parent());
            $(".resume-bx>ul>li:eq(2)").after($(".resume-bx>ul>li span.ic-video").parent());
        }
        utils.top_position();
        utils.main_pop_responsive();
    });


})(jQuery);


