$(window).resize(function () {
    $("#myMenu").css('min-height', $("#page-content").height() + "px");
//   $(".smallMenu").height($("#page-content").height());
});


window.addEventListener("touchmove", function (event) {
    if ($(event.target).parents(".editing_move").length > 0) {
        event.preventDefault();
    }
}, {passive: false})
$(document).ready(function () {
    if ($("body").hasClass("path-mod")) {
        var isIframe = window.location != window.parent.location;
        if (!isIframe) {
            $("body").addClass("no-pop");
        }
    }
    
    if($(".add-episode").length != 0){
        var btn = $(".add-episode").clone();
        $(".bt-btn-area").append(btn);
    }


    $("body").append("<div id='m_bg'></div>");
    $(".m_header_menu").click(function () {
        $("#block-region-side-pre").css("left", "0");
        $("#m_bg").height($("body").prop("scrollHeight")).show();
    });
    $("#m_bg").click(function () {
        $("#block-region-side-pre").css("left", "-100%");
        $(this).hide();
    });

    $("fieldset.clearfix legend").off("click").click(function (e) {
        if (!$(e.target).is("a") && !$(e.target).hasClass(".helptooltip") && $(e.target).parents(".helptooltip").length == 0) {
            $(this).parent().toggleClass("collapsed");
        }
    });

    $(".thread-style .thread-style-lists li .thread-content>.post-title").each(function () {
        if ($(this).find("img[src*=attachment]").length != 0) {
            $(this).addClass("has-file")
        }
    });

    $(".generaltable, .forumheaderlist, .userenrolment, .gradereport-grader-table, #categoryquestions").wrap(function () {
        if (!$(this).parent('div').hasClass('table-scroll')) {
            return "<div class='table-scroll dragscroll'></div>";
        }
    });
    //atto editor 포커스 되지 않은 상태에서 이미지등록 시 오류 발생 문제 해결
    $(document).on("click", ".atto_image_button", function (e) {
        $(this).closest(".fitem").find(".editor_atto_content_wrap *:not(img)").focus();
        $(".openimagebrowser").focus();
    });
    $(document).on("touchstart", ".atto_image_button", function (e) {
        if (/Android/i.test(navigator.userAgent)) {
            $(this).closest(".fitem").find(".editor_atto_content_wrap *:not(img)").click().focus();
        }
    });
    try {
        if (!check_mobile()) {
            $(".popup").draggable();
        }
    } catch (e) {
    }

//    $(document).on("click", "table textarea, table input[type=text], table input[type=number], table select", function(){
//        $(this).focus();
//    });
//    
    //게시판 리스트 클릭 시 상세페이지 이동
    $(".thread-style .thread-style-lists li").click(function () {
        var link = $(this).find(".post-title a").attr("href");
        if (link) {
            location.href = $(this).find(".post-title a").attr("href");
        }
    })


    /*오른쪽 블럭 길이 늘어났을때 좌측 영역과 높이 맞추기*/
    $("#block-region-side-post .root_node ").one("click", function () {
        setTimeout(function () {
            $("#block-region-side-post p.tree_item.branch").click(function (e) {
                if ($(this).attr("aria-expanded") == "false") {
                    $("section#region-main, #myMenu").height($("#block-region-side-post").height() + $(this).find("+ ul").height());
                } else {
                    $("section#region-main, #myMenu").height($("#block-region-side-post").height() - $(this).find("+ ul").height());
                }
            });
        }, '3500');
    });

    /** iframe 높이 자동 조절 **/
    if ($("iframe#vod_viewer").length != 0) {
        $("iframe#vod_viewer").load(function () {
            var contents_height = $("iframe#vod_viewer").contents().find("body").height();
            $("iframe#vod_viewer").height(contents_height);
        });
        $(window).resize(function () {
            var contents_height = $("iframe#vod_viewer").contents().find("body").height();
            $("iframe#vod_viewer").height(contents_height);
        });
    }

    /*메인 공지사항/학사관리 탭*/
    $(".notice-header h3").click(function () {
        $(".notice-header h3").removeClass("on");
        $(this).addClass("on")
        $(".notice ul").hide()
        var num = $(this).index();
        $(".notice ul:eq(" + num + ")").fadeIn();
    });
    /*메인 공지사항/학사관리 탭*/



    //메뉴 높이 조절
    $("#block-region-side-pre").css("min-height", $("#region-main.span10").height());

    var loginHeight = $("html").height() - 110;
    if (loginHeight < 800) {
        loginHeight = 850;
    }
    ;
    $("#page-login-index #region-main").height(loginHeight);

    //메뉴 높이 조절
    $("#myMenu, #block-region-side-post").css('min-height', $("#page-content").height() + "px");


    //모바일메뉴 버튼 추가
    $("nav .container-fluid .brand").before('<div id="mMenuBtn"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></div>');

    /*서브메뉴*/

    $(".dropdown.langmenu  a").on({
        focus: function () {
            $(this).parent().addClass("open");
        },
        blur: function () {
//           console.log("focusout");    
//           $(this).parent().removeClass("open");
        }
    })


    $(".mooc_header").append("<div class='left'> > </div>");
    $(".mooc_header").append("<div class='right'> < </div>");

    $(".mooc_header .left").click(function () {
        var hwidth = Number($(".mooc_header").width()) - 50;
        var hleft = Number($(".mooc_header ul").css("left").split("px")[0]);
        console.log(hleft);
        if (0 > hleft)
            $(".mooc_header ul").animate({left: "+=111"}, "fast");
    });

    $(".mooc_header .right").click(function () {
        var hwidth = Number($(".mooc_header").width()) - 50;
        var hleft = Number($(".mooc_header ul").css("left").split("px")[0]);
        console.log(hleft);
        if (0 >= hleft && hwidth > Math.abs(hleft) && hwidth < 1320)
            $(".mooc_header ul").animate({left: "-=111"}, "fast");
    });


    /*메뉴*/
    $("#myMenu > li > a").on({
        focus: function (e) {
            var target = $(this).find("h3");
            e.preventDefault();
            var thisLi = target.parents("li");
            if (!thisLi.hasClass("on")) {
                thisLi.addClass("on");
                thisLi.find(".open i").attr("class", "fa fa-caret-up");
            }
        }, mousedown: function (e) {
            mymenu_click($(this).find("h3"));
            return false;
        }, click: function () {
            var target = $(this).find("h3");
            var thisLi = target.parents("li");
            if (!thisLi.hasClass('link')) {
                return false;
            } else {
                return true;
            }
        }
    });

    /*    상단 유저메뉴 */
    $("#custom_user_menu > li > a").on({
        mousedown: usermenu_click,
        focus: usermenu_click,
        click: function () {
            return false;
        }
    });
    $("#custom_user_menu > li .block").on({
        mouseover: function () {
            $("#custom_user_menu > li > a").off("mousedown");
            $("body").off("click");
        },
        mouseleave: function () {
            $("#custom_user_menu > li > a").on("mousedown", usermenu_click);
        }
    });
    $(".usermenu").click(function () {
        $("#custom_user_menu li .block").fadeOut('fast');
        $("#custom_user_menu li").removeAttr("style");
    });
    $("a.last_pop").focusout(function () {
        $("#custom_user_menu li .block").fadeOut('fast');
        $("#custom_user_menu li").removeAttr("style");
    });



    /*모바일메뉴*/
    $("#mMenuBtn").click(function () {
        mymenu_mobile_click($(this));
    });


    //현재 메뉴 활성화
    //My page

    var mypage = $("#myMenu > li h3.mymenu-mypage");

    if ($("body").attr("id") == "page-my-index") {
        mymenu_click(mypage);
        $(".mymenu-page-my-index").addClass("on");
    } else if ($("body").attr("id") == "page-my-allcourse") {
        mymenu_click(mypage);
        $(".mymenu-page-user-course").addClass("on");
    } else if ($("body").attr("id") == "page-user-files") {
        mymenu_click(mypage);
        $(".mymenu-page-user-files").addClass("on");
    } else if ($("body").hasClass("path-local-courselist")) {
        mymenu_click(mypage);
        $(".mymenu-path-local-courselist").addClass("on");
    } else if ($("body").hasClass("path-local-repository")) {
        mymenu_click(mypage);
        $(".mymenu-path-local-repository").addClass("on");
    } else if ($("body").hasClass("path-calendar")) {
        mymenu_click(mypage);
        $(".mymenu-path-calendar").addClass("on");
    } else if ($("body").hasClass("path-message")) {
        mymenu_click(mypage);
        $(".mymenu-path-message").addClass("on");
    } else if ($("body").hasClass("path-local-board")) {
        mymenu_click(mypage);
        $(".mymenu-path-local-board").addClass("on");
    } else if ($("body").hasClass("path-local-bookmark")) {
        mymenu_click(mypage);
        $(".mymenu-path-local-board").addClass("on");
    } else if ($("body").hasClass("path-local-evaluation")) {
        mymenu_click(mypage);
        $(".mymenu-path-local-evaluation").addClass("on");
    } else if ($("body").hasClass("path-grade-report")) {
        mymenu_click(mypage);
        $(".mymenu-path-grade-report").addClass("on");
    } else if ($("body").attr("id") == "page-user-edit") {
        mymenu_click(mypage);
        $(".mymenu-page-user-edit").addClass("on");
    } else if ($("body").attr("id") == "page-local-lmsdata-grade_tibero") {
        mymenu_click(mypage);
        $(".mymenu-page-local-lmsdata-grade_tibero").addClass("on");
    } else if ($("body").attr("id") == "page-local-attendance-index") {
        mymenu_click(mypage);
        $(".mymenu-page-local-attendance-index").addClass("on");
    } else if ($("body").attr("id") == "page-local-apply-assistant") {
        mymenu_click(mypage);
        $(".mymenu-page-local-apply-assistant").addClass("on");
    }

    if ($("body").attr("id") == "page-local-courselist-courseoverview") {
        $(".mymenu-page-local-courselist-courseoverview").addClass("on");
        $(".mymenu-path-local-courselist").removeClass("on");
    }


    //교과 과정
    var courselist1 = $("#myMenu > li h3.mymenu-okregular");

    if ($("body").attr("id") == "page-local-okregular-my") {
        mymenu_click(courselist1);
        $(".mymenu-page-local-okregular-my").addClass("on");
    } else if ($("body").attr("id") == "page-local-okregular-apply" || $("body").attr("id") == "page-local-okregular-courseinfo") {
        mymenu_click(courselist1);
        $(".mymenu-page-local-okregular-apply").addClass("on");
    }

    //비교과 과정
    var courselist2 = $("#myMenu > li h3.mymenu-okirregular");

    if ($("body").attr("id") == "page-local-okirregular-my") {
        mymenu_click(courselist2);
        $(".mymenu-page-local-okirregular-my").addClass("on");
    } else if ($("body").attr("id") == "page-local-okirregular-apply" || $("body").attr("id") == "page-local-okirregular-courseinfo") {
        mymenu_click(courselist2);
        $(".mymenu-page-local-okirregular-apply").addClass("on");
    } else if ($("body").attr("id") == "page-local-okirregular-complete") {
        mymenu_click(courselist2);
        $(".mymenu-page-local-okirregular-complete").addClass("on");
    }

    //이러닝 과정
    var courselist3 = $("#myMenu > li h3.mymenu-oklearning");

    if ($("body").attr("id") == "page-local-oklearning-my") {
        mymenu_click(courselist3);
        $(".mymenu-page-local-oklearning-my").addClass("on");
    } else if ($("body").attr("id") == "page-local-oklearning-apply" || $("body").attr("id") == "page-local-oklearning-courseinfo") {
        mymenu_click(courselist3);
        $(".mymenu-page-local-oklearning-apply").addClass("on");
    } else if ($("body").attr("id") == "page-local-oklearning-course_add") {
        mymenu_click(courselist3);
        $(".mymenu-page-local-oklearning-course_add").addClass("on");
    }

    //이용안내
    var guide = $("#myMenu > li h3.mymenu-jinoboard");

    if ($("body").hasClass("path-local-jinoboard-1")) {
        mymenu_click(guide);
        $(".mymenu-path-local-jinoboard-1").addClass("on");
    } else if ($("body").hasClass("path-local-jinoboard-2")) {
        mymenu_click(guide);
        $(".mymenu-path-local-jinoboard-2").addClass("on");
    } else if ($("body").hasClass("path-local-jinoboard-3")) {
        mymenu_click(guide);
        $(".mymenu-path-local-jinoboard-3").addClass("on");
    } else if ($("body").hasClass("path-local-jinoboard-5")) {
        mymenu_click(guide);
        $(".mymenu-path-local-jinoboard-5").addClass("on");
    }

});

function mymenu_click(target) {
    $("#myMenu").removeClass("smallMenu");
    var thisLi = target.parents("li");
    if (!thisLi.hasClass("on")) {
        thisLi.addClass("on");
        thisLi.find(".open i").attr("class", "fa fa-caret-up");
    } else {
        thisLi.removeClass("on");
        thisLi.find(".open i").attr("class", "fa fa-caret-down");
    }
}

function mymenu_block_click(target) {
    var block = target.parent(".block");
    target = target.parent(".block").find(".content");
    if (!target.hasClass("hidden")) {
        target.addClass("hidden");
        block.addClass("hidden");
    } else {
        target.removeClass("hidden");
        block.removeClass("hidden");
    }
    $(".smallMenu").height($("#page-content").height());
}

function mymenu_mobile_click(target) {
    $("body").append("<div id='opacity'></div>");
    $("#opacity").show();
    $("#myMenu").css("left", "0px");
    $("#opacity").click(function () {
        $("#opacity").fadeOut();
        $("#myMenu").css("left", "-5000px");
    });

}

function usermenu_click() {
    $("#custom_user_menu li .block").hide();
    $("#action-menu-0").removeClass("show");

    $("#custom_user_menu>li").css("background", "#4f5b61");
    $(this).parent("li").css({"background": "#414b50"});

    $(this).parent("li").find(".block").fadeIn("fast", function () {

        $("#custom_user_menu").mouseover(function () {
            $("body").off("click");
        });
        $("#custom_user_menu").mouseout(function () {
            $("body").not("#custom_user_menu").one("click", function () {
                $("#custom_user_menu .block").fadeOut("fast");
                $("#custom_user_menu li").removeAttr("style");
            });
        });

    });
}
var pop;
function openPop(pWidth, pHeight) {
    var top = (screen.availHeight - pHeight) / 2;
    var left = (screen.availWidth - pWidth) / 2;
    pop = window.open('http://open.jinotech.com/oklass/index.htm', 'pop', "left=" + left + ",top=" + top + ",toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,width=" + pWidth + ",height=" + pHeight);


}


function mymenu_on(id) {
    var target = $('.mymenu-menu' + id);
    $("#myMenu").removeClass("smallMenu");
    var thisLi = target.parents("li");
    if (!thisLi.hasClass("on")) {
        thisLi.addClass("on");
        thisLi.find(".open i").attr("class", "fa fa-caret-up");
    } else {
        thisLi.removeClass("on");
        thisLi.find(".open i").attr("class", "fa fa-caret-down");
    }
}