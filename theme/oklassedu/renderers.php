<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    theme_academi
 * @copyright  2015 onwards Nephzat Dev Team (http://www.nephzat.com)
 * @authors    Nephzat Dev Team , nephzat.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('classes/core_renderer.php');
require_once('classes/course_renderer.php');
require_once('classes/core_output.php');

/*class theme_ust_core_renderer extends core_renderer {
    public function user_picture(stdClass $user, array $options = null) {
        $username = trim($user->username);
        $base64_user_jpg = file_get_contents('http://edu.ust.ac.kr/com/selectImageByteString3.do?memberNo='.$username); 
	$base64_user_jpg = str_replace('base64("', '', $base64_user_jpg, $count);
        if(!$count) {
            $htmlfm .='<img src="'.$CFG->wwwroot.'/theme/image.php/standard/core/1398322866/u/f1" alt="user picture" title="user picture" class="userpicture defaultuserpic" width="100" height="100">';
            return $htmlfm;
        } else {
            $userjpg = str_replace('")', '', $base64_user_jpg);
            $htmlfm .='<img src="'.$userjpg.'" alt="user picture" title="user picture" class="userpicture defaultuserpic" width="100" height="100">';
            return $htmlfm;
        }
    }
}

class theme_ust_core_renderer_ajax extends core_renderer_ajax {
    public function user_picture(stdClass $user, array $options = null) {
        $username = trim($user->username);
        $base64_user_jpg = file_get_contents('http://edu.ust.ac.kr/com/selectImageByteString3.do?memberNo='.$username); 
	$base64_user_jpg = str_replace('base64("', '', $base64_user_jpg, $count);
        if(!$count) {
            $htmlfm .='<img src="'.$CFG->wwwroot.'/theme/image.php/standard/core/1398322866/u/f1" alt="user picture" title="user picture" class="userpicture defaultuserpic" width="100" height="100">';
            return $htmlfm;
        } else {
            $userjpg = str_replace('")', '', $base64_user_jpg);
            $htmlfm .='<img src="'.$userjpg.'" alt="user picture" title="user picture" class="userpicture defaultuserpic" width="100" height="100">';
            return $htmlfm;
        }
    }
}

require_once($CFG->dirroot.'/mod/assign/renderer.php');
class theme_ust_mod_assign_renderer extends mod_assign_renderer {
    public function user_picture(stdClass $user, array $options = null) {
        $username = trim($user->username);
        $base64_user_jpg = file_get_contents('http://edu.ust.ac.kr/com/selectImageByteString3.do?memberNo='.$username); 
	$base64_user_jpg = str_replace('base64("', '', $base64_user_jpg, $count);
        if(!$count) {
            $htmlfm .='<img src="'.$CFG->wwwroot.'/theme/image.php/standard/core/1398322866/u/f1" alt="user picture" title="user picture" class="userpicture defaultuserpic" width="100" height="100">';
            return $htmlfm;
        } else {
            $userjpg = str_replace('")', '', $base64_user_jpg);
            $htmlfm .='<img src="'.$userjpg.'" alt="user picture" title="user picture" class="userpicture defaultuserpic" width="100" height="100">';
            return $htmlfm;
        }
    }
}*/