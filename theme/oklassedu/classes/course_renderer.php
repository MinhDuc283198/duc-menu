<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is built using the bootstrapbase template to allow for new theme's using
 * Moodle's new Bootstrap theme engine
 *
 * @package     theme_oklassedu
 * @copyright   2015 Nephzat Dev Team, nephzat.com
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . "/course/renderer.php");

class theme_oklassedu_core_course_renderer extends core_course_renderer {
    public function course_section_cm_edit_actions($actions, cm_info $mod = null, $displayoptions = array()) {
        global $CFG;

        if (empty($actions)) {
            return '';
        }

        if (isset($displayoptions['ownerselector'])) {
            $ownerselector = $displayoptions['ownerselector'];
        } else if ($mod) {
            $ownerselector = '#module-'.$mod->id;
        } else {
            debugging('You should upgrade your call to '.__FUNCTION__.' and provide $mod', DEBUG_DEVELOPER);
            $ownerselector = 'li.activity';
        }

        if (isset($displayoptions['constraintselector'])) {
            $constraint = $displayoptions['constraintselector'];
        } else {
            $constraint = '.course-content';
        }

        $menu = new action_menu();
        $menu->set_owner_selector($ownerselector);
        $menu->set_constraint($constraint);
        $menu->set_alignment(action_menu::TR, action_menu::BR);
        $icon =  html_writer::empty_tag('img', array('src' => $this->output->pix_url('i/settings')));
        $menu->set_menu_trigger($icon);
        if (isset($CFG->modeditingmenu) && !$CFG->modeditingmenu || !empty($displayoptions['donotenhance'])) {
            $menu->do_not_enhance();

            // Swap the left/right icons.
            // Normally we have have right, then left but this does not
            // make sense when modactionmenu is disabled.
            $moveright = null;
            $_actions = array();
            foreach ($actions as $key => $value) {
                if ($key === 'moveright') {

                    // Save moveright for later.
                    $moveright = $value;
                } else if ($moveright) {

                    // This assumes that the order was moveright, moveleft.
                    // If we have a moveright, then we should place it immediately after the current value.
                    $_actions[$key] = $value;
                    $_actions['moveright'] = $moveright;

                    // Clear the value to prevent it being used multiple times.
                    $moveright = null;
                } else {

                    $_actions[$key] = $value;
                }
            }
            $actions = $_actions;
            unset($_actions);
        }
        foreach ($actions as $action) {
            if ($action instanceof action_menu_link) {
                $action->add_class('cm-edit-action');
            }
            $menu->add($action);
        }
        $menu->attributes['class'] .= ' section-cm-edit-actions commands';

        // Prioritise the menu ahead of all other actions.
        $menu->prioritise = true;

        return $this->render($menu);
    }
    public function frontpage_my_courses($type, $searchtype, $searchvalue, $year, $term, $currentpage, $myperpage) {
        $perpage = 5;
        global $USER, $CFG, $DB;

        if (!isloggedin() or isguestuser()) {
            return '';
        }

        $output = '';
        if (!empty($CFG->navsortmycoursessort)) {
            // sort courses the same as in navigation menu
            $sortorder = 'visible DESC,'. $CFG->navsortmycoursessort.' ASC';
        } else {
            $sortorder = 'visible DESC,sortorder ASC';
        }
        
        
        $sql_where[] = " c.id <> :siteid ";
        $params = array('siteid'=>SITEID);
        
        $sql_wheres = "";
        if($year != 0){
            $sql_where[] = " lc.year = :year ";
            $params['year'] = $year;
            if($term != 0){
                $sql_where[] = " lc.term = :term ";
                $params['term'] = $term;
            }
        }
        
        if(!empty($searchvalue)) {
            switch($searchtype) {
                case 0: // 강의명
                    $sql_where[] = $DB->sql_like('c.fullname', ':kor_lec_name');
                    $params['kor_lec_name'] = '%'.$searchvalue.'%';
                    break;
                case 1: // 책임교수명
                    $sql_where[] = '( '.$DB->sql_like('ur.firstname', ':profname_kr').' or '.$DB->sql_like('ur.lastname', ':profname_en').')';
                    $params['profname_kr'] = '%'.$searchvalue.'%';
                    $params['profname_en'] = '%'.$searchvalue.'%';
                    break;
                default:
                    break;
            }
        }
        
        $sql_select = "SELECT c.*";
        $sql_from = "FROM {course} c
                JOIN (SELECT DISTINCT e.courseid
                FROM {enrol} e
                JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid = :userid)
                WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
                ) en ON (en.courseid = c.id)
                JOIN {lmsdata_class} lc ON lc.course = c.id
                LEFT JOIN {context} ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = 50)
                LEFT JOIN {user} ur ON ur.id = lc.prof_userid";
        
        if(!empty($sql_where)) {
            $sql_wheres = ' WHERE '.implode(' and ', $sql_where);
        }else {
            $sql_wheres = '';
        }
        
        $sql_orderby = "ORDER BY visible DESC,sortorder ASC";

        $params['contextlevel'] = CONTEXT_COURSE;
        $params['userid']  = $USER->id;
        $params['active']  = ENROL_USER_ACTIVE;
        $params['enabled'] = ENROL_INSTANCE_ENABLED;
        $params['now1']    = round(time(), -2); // improves db caching
        $params['now2']    = $params['now1'];
        
        $currentdate = time();

        $courses_regular_count = $DB->get_records_sql($sql_select.$sql_from.$sql_wheres.$sql_orderby, $params);
        $courses_regular = $DB->get_records_sql($sql_select.$sql_from.$sql_wheres.$sql_orderby, $params, ($currentpage-1)*$myperpage, $myperpage);
        //$courses_irregular = $DB->get_records_sql($sql_select.$sql_from.$sql_wheres." AND lc.isnonformal <> 0 AND lc.timestart <= ".$currentdate." AND lc.timeend >= ".$currentdate." ".$sql_orderby, $params);
        
        $rhosts   = array();
        $rcourses = array();
        if (!empty($CFG->mnet_dispatcher_mode) && $CFG->mnet_dispatcher_mode==='strict') {
            $rcourses = get_my_remotecourses($USER->id);
            $rhosts   = get_my_remotehosts();
        }

        if (!empty($courses_regular) || !empty($rcourses) || !empty($rhosts)) {
            $mytotalcount = count($courses_regular_count);
            $chelper = new coursecat_helper();
            if ($mytotalcount > $CFG->frontpagecourselimit) {
                // There are more enrolled courses than we can display, display link to 'My courses'.
                $totalcount = count($courses_regular_count);
                $courses = array_slice($courses, 0, $CFG->frontpagecourselimit, true);
                $chelper->set_courses_display_options(array(
                        'viewmoreurl' => new moodle_url('/my/'),
                        'viewmoretext' => new lang_string('mycourses')
                    ));
            } else {
                // All enrolled courses are displayed, display link to 'All courses' if there are more courses in system.
                $chelper->set_courses_display_options(array(
                        'viewmoreurl' => new moodle_url('/course/index.php'),
                        'viewmoretext' => new lang_string('fulllistofcourses')
                    ));
                $totalcount = $DB->count_records('course') - 1;
            }
            $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
                    set_attributes(array('class' => 'frontpage-course-list-enrolled'));
            
            $pageparams = array('totalcount'=>count($courses_regular_count), 'currentpage'=> $currentpage, 'myperpage' => $myperpage, 'year' => $year, 'trem' => $term,'searchtype' => $searchtype, 'searchvalue' => $searchvalue);
            $output .= $this->coursecat_courses($chelper, $courses_regular, $totalcount, $type, $pageparams);

            // MNET
            if (!empty($rcourses)) {
                // at the IDP, we know of all the remote courses
                $output .= html_writer::start_tag('div', array('class' => 'courses'));
                foreach ($rcourses as $course) {
                    $output .= $this->frontpage_remote_course($course);
                }
                $output .= html_writer::end_tag('div'); // .courses
            } elseif (!empty($rhosts)) {
                // non-IDP, we know of all the remote servers, but not courses
                $output .= html_writer::start_tag('div', array('class' => 'courses'));
                foreach ($rhosts as $host) {
                    $output .= $this->frontpage_remote_host($host);
                }
                $output .= html_writer::end_tag('div'); // .courses
            }
        }
        return $output;
    }
    
     /**
     * Renders the list of courses
     *
     * This is internal function, please use {@link core_course_renderer::courses_list()} or another public
     * method from outside of the class
     *
     * If list of courses is specified in $courses; the argument $chelper is only used
     * to retrieve display options and attributes, only methods get_show_courses(),
     * get_courses_display_option() and get_and_erase_attributes() are called.
     *
     * @param coursecat_helper $chelper various display options
     * @param array $courses the list of courses to display
     * @param int|null $totalcount total number of courses (affects display mode if it is AUTO or pagination if applicable),
     *     defaulted to count($courses)
     * @return string
     */
    protected function coursecat_courses(coursecat_helper $chelper, $courses_regular, $totalcount = null, $type, $pageparams = array()) {
        global $CFG, $USER, $DB;
        if ($mytotalcount === null) {
            $mytotalcount = count($pageparams['totalcount']);
        }
        if (!$totalcount) {
            // Courses count is cached during courses retrieval.
            return '';
        }

        if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_AUTO) {
            // In 'auto' course display mode we analyse if number of courses is more or less than $CFG->courseswithsummarieslimit
            if ($totalcount <= $CFG->courseswithsummarieslimit) {
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED);
            } else {
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_COLLAPSED);
            }
        }

        // prepare content of paging bar if it is needed
        $paginationurl = $chelper->get_courses_display_option('paginationurl');
        $paginationallowall = $chelper->get_courses_display_option('paginationallowall');
        if ($totalcount > count($courses)) {
            // there are more results that can fit on one page
            if ($paginationurl) {
                // the option paginationurl was specified, display pagingbar
                $perpage = $chelper->get_courses_display_option('limit', $CFG->coursesperpage);
                $page = $chelper->get_courses_display_option('offset') / $perpage;
                $pagingbar = $this->paging_bar($totalcount, $page, $perpage,
                        $paginationurl->out(false, array('perpage' => $perpage)));
                if ($paginationallowall) {
                    $pagingbar .= html_writer::tag('div', html_writer::link($paginationurl->out(false, array('perpage' => 'all')),
                            get_string('showall', '', $totalcount)), array('class' => 'paging paging-showall'));
                }
            } else if ($viewmoreurl = $chelper->get_courses_display_option('viewmoreurl')) {
                // the option for 'View more' link was specified, display more link
//                $viewmoretext = $chelper->get_courses_display_option('viewmoretext', new lang_string('viewmore'));
//                $morelink = html_writer::tag('div', html_writer::link($viewmoreurl, $viewmoretext),
//                        array('class' => 'paging paging-morelink'));
            }
        } else if (($totalcount > $CFG->coursesperpage) && $paginationurl && $paginationallowall) {
            // there are more than one page of results and we are in 'view all' mode, suggest to go back to paginated view mode
            $pagingbar = html_writer::tag('div', html_writer::link($paginationurl->out(false, array('perpage' => $CFG->coursesperpage)),
                get_string('showperpage', '', $CFG->coursesperpage)), array('class' => 'paging paging-showperpage'));
        }

        // display list of courses
        $attributes = $chelper->get_and_erase_attributes('courses');
        $content = html_writer::start_tag('div', $attributes);

        if (!empty($pagingbar)) {
            $content .= $pagingbar;
        }
        $content .= html_writer::start_tag("div",array('class'=>'table_group'));
        if(!empty($courses_regular)){
            $usergroup = $DB->get_record('lmsdata_user', array('userid' => $USER->id), 'usergroup');
           
            $content .= html_writer::start_tag("div",array('class'=>'scrolldiv'));
            $content .= html_writer::start_tag('table',array('class'=>'generaltable regular-courses'));
            $content .= html_writer::start_tag('thead');
                $content .= html_writer::start_tag('tr');
                    $content .= html_writer::start_tag('th', array('width'=>'7%'));
                        $content .= get_string('stats_years', 'local_lmsdata');
                    $content .= html_writer::end_tag('th');
                    $content .= html_writer::start_tag('th', array('width'=>'7%'));
                        $content .= get_string('stats_terms', 'local_lmsdata');
                    $content .= html_writer::end_tag('th');
                    $content .= html_writer::start_tag('th', array('width'=>'50%'));
                        $content .= get_string('stats_coursename', 'local_lmsdata');
                    $content .= html_writer::end_tag('th');
                    $content .= html_writer::start_tag('th', array('width'=>'20%'));
                        $content .= get_string('teacher', 'local_lmsdata');
                    $content .= html_writer::end_tag('th');
                    $content .= html_writer::start_tag('th', array('width'=>'8%'));
                        $content .= get_string('stats_learninghistory', 'local_lmsdata');
                    $content .= html_writer::end_tag('th');
                    $content .= html_writer::start_tag('th', array('width'=>'8%'));
                        $content .= get_string('stats_learningprogress', 'local_lmsdata');
                    $content .= html_writer::end_tag('th');
                    if($usergroup == 'rs'){
                        $content .= html_writer::start_tag('th', array('width'=>'13%'));
                            $content .= get_string('star_score', 'theme_oklassedu');
                        $content .= html_writer::end_tag('th');
                    }
                $content .= html_writer::end_tag('tr');
            $content .= html_writer::end_tag('thead');
            $content .= html_writer::start_tag('tbody');
            foreach ($courses_regular as $course) {
                $content .= $this->coursecat_courselist($chelper, $course, $usergroup);
            }
            $content .= html_writer::end_tag('tbody');
            $content .= html_writer::end_tag('table');
            $content .= html_writer::end_tag("div");
            $content .= my_print_paging_navbar($pageparams['totalcount'], $pageparams['currentpage'], $pageparams['myperpage'], $CFG->wwwroot.'/my/index.php',
                        array('year' => $pageparams['year'], 'trem' => $pageparams['term'],'searchtype' => $pageparams['searchtype'], 'searchvalue' => $pageparams['searchvalue'])); 
        }
        $content .= html_writer::end_tag("div");
        
        $content .= html_writer::start_tag('div', array('class'=>'course_group')); 
//        foreach ($courses_irregular as $course) {
//            $coursecount ++;
//            $classes = ($coursecount%2) ? 'odd' : 'even';
//            if ($coursecount == 1) {
//                $classes .= ' first';
//            }
//            if ($coursecount >= count($courses)) {
//                $classes .= ' last';
//            }
//            if ($type == 0){
//                $content .= $this->coursecat_coursebox($chelper, $course, $classes);
//            }else{
//                $content .= $this->coursecat_coursesumnail($chelper, $course, $classes);;
//            }
//            
//        }
//        
//        if(!$courses_irregular){
//            $content .=  html_writer::tag('div', get_string('novalidcourses'), array('class'=>'mypage_irregular_novalidcourses'));
//        }
        
        if (!empty($pagingbar)) {
            $content .= $pagingbar;
        }
        if (!empty($morelink)) {
            $content .= $morelink;
        }
        $content .= html_writer::end_tag('div');
        $content .= html_writer::end_tag('div'); // .courses
        
        return $content;
    }
	
    protected function coursecat_courselist(coursecat_helper $chelper, $course, $usergroup, $additionalclasses = '') {
        global $CFG, $DB;
        if (!isset($this->strings->summary)) {
            $this->strings->summary = get_string('summary');
        }
        if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
            return '';
        }
        if ($course instanceof stdClass) {
            require_once($CFG->libdir. '/coursecatlib.php');
            $course = new course_in_list($course);
        }
        
        $lmsdata = $DB->get_record_sql('select * FROM {lmsdata_class} WHERE courseid = :courseid ', array('courseid'=>$course->id));
        
        // course name
        if (current_language() =='ko'){
            $coursename = $chelper->get_course_formatted_name($course);
        }else{            
            $coursename = $lmsdata->eng_lec_name;
        }
        $coursenamelink = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)), $coursename, array('class' => $course->visible ? '' : 'dimmed'));
        foreach ($course->get_course_contacts() as $userid => $coursecontact) {
            //선생님 표시 변경
            $prof_name[] = $coursecontact['username'];
        }
        $name .= html_writer::tag('span',implode(", ", $prof_name),array('class'=>'mypage-teachername'));
        $learningstats = get_string('learningstats', 'theme_oklassedu');
        $progressstats = get_string('progressstats', 'theme_oklassedu');
        
        $content = '';
        
        $content .= html_writer::start_tag('tr');
            $content .= html_writer::start_tag('td');
                $content .= html_writer::tag('div', $lmsdata->year, array());
            $content .= html_writer::end_tag('td');
            $content .= html_writer::start_tag('td');
                $content .= html_writer::tag('div', $lmsdata->term, array());
            $content .= html_writer::end_tag('td');
            $content .= html_writer::start_tag('td', array('class'=>'title'));
                $content .= html_writer::tag('div', $coursenamelink, array());
            $content .= html_writer::end_tag('td');
            $content .= html_writer::start_tag('td');
                $content .= html_writer::tag('div', $name, array());
            $content .= html_writer::end_tag('td');
            $content .= html_writer::start_tag('td');
                $content .= html_writer::tag('div', '<input type="button" class="ct-btn white" style="font-size: 14px" onclick="location.href=\''.$CFG->wwwroot.'/course/report/statistics/modules.php?id='.$course->id.'\'" value="'.$learningstats.'">', array());
            $content .= html_writer::end_tag('td');
            $content .= html_writer::start_tag('td');
            $content .= html_writer::tag('div', '<input type="button" class="ct-btn white" style="font-size: 14px" onclick="location.href=\''.$CFG->wwwroot.'/course/report/statistics/progress.php?id='.$course->id.'\'" value="'.$progressstats.'">', array());
            $content .= html_writer::end_tag('td');
            if($usergroup  == 'rs'){
                $content .= html_writer::start_tag('td');
                require_once($CFG->dirroot . '/local/coursepoint/lib.php');
                require_once($CFG->dirroot . '/local/coursepoint/index.php');
                $star = course_star_tag($course);
                $content .= $star;
                $content .= html_writer::end_tag('td');
            }
        $content .= html_writer::end_tag('tr');
        
        return $content;
    }
    
    protected function coursecat_coursesumnail(coursecat_helper $chelper, $course, $additionalclasses = '') {
        global $CFG, $DB;
        if (!isset($this->strings->summary)) {
            $this->strings->summary = get_string('summary');
        }
        if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
            return '';
        }
        if ($course instanceof stdClass) {
            require_once($CFG->libdir. '/coursecatlib.php');
            $course = new course_in_list($course);
        }
        $content = '';
        $classes = trim('coursesumnail clearfix '. $additionalclasses);

        // .coursebox
        $content .= html_writer::start_tag('div', array(
            'class' => $classes,
            'data-courseid' => $course->id,
            'data-type' => self::COURSECAT_TYPE_COURSE,
        ));
        //like 기능
        require_once($CFG->dirroot . '/local/coursepoint/lib.php');
        require_once($CFG->dirroot . '/local/coursepoint/index.php');
        $like = course_like_tag($course);
        
        //$content .= $like; 
        
        $content .= html_writer::start_tag('div', array('class' => 'content'));
        $content .= $this->coursecat_coursesumnail_content($chelper, $course);
        $content .= html_writer::end_tag('div'); // .content
        
        $content .= html_writer::end_tag('div'); // .coursebox
        return $content;
    }
    
     protected function coursecat_coursebox(coursecat_helper $chelper, $course, $additionalclasses = '') {
        global $CFG, $DB;
        if (!isset($this->strings->summary)) {
            $this->strings->summary = get_string('summary');
        }
        if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
            return '';
        }
        if ($course instanceof stdClass) {
            require_once($CFG->libdir. '/coursecatlib.php');
            $course = new course_in_list($course);
        }
        $content = '';
        $classes = trim('coursebox clearfix '. $additionalclasses);
        if ($chelper->get_show_courses() >= self::COURSECAT_SHOW_COURSES_EXPANDED) {
            $nametag = 'h3';
        } else {
            $classes .= ' collapsed';
            $nametag = 'div';
        }

        // .coursebox
        $content .= html_writer::start_tag('div', array(
            'class' => $classes,
            'data-courseid' => $course->id,
            'data-type' => self::COURSECAT_TYPE_COURSE,
        ));

        $content .= html_writer::start_tag('div', array('class' => 'info'));
        
        //교과,비교과,온라인 구분
//        $isnonformal = $DB->get_record_sql('select isnonformal FROM {lmsdata_class} WHERE course = :courseid ', array('courseid'=>$course->id));
//        switch ($isnonformal->isnonformal) {
//            case '1' : $isnonformal_text = get_string('irregular','theme_oklassedu'); break;
//            case '2' : $isnonformal_text = get_string('elearning','theme_oklassedu'); break;
//        }
        $isnonformal_text = get_string('irregular','theme_oklassedu');
        $concss = html_writer::tag('span',$isnonformal_text, array('class' => 'isnonformal division_1'));
        // course name
        if (current_language() =='ko'){
            $coursename = $concss.' '.$chelper->get_course_formatted_name($course);
        }else{
            
            $lmsdata = $DB->get_record('lmsdata_class', array('course' =>$course->id ));
            
            $coursename = $concss.' '.$lmsdata->eng_lec_name;
        }
        
        $coursenamelink = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),
                                            $coursename, array('class' => $course->visible ? '' : 'dimmed'));
        $content .= html_writer::tag($nametag, $coursenamelink, array('class' => 'coursename'));
        $activitynew = $this->new_activity($course);
//        if($activitynew){
//            $content .= html_writer::tag('span','new',array('class'=>'course_activity_new'));
//        }
        // If we display course in collapsed form but the course has summary or course contacts, display the link to the info page.
        
        $learningstats = get_string('learningstats', 'theme_oklassedu');
        $progressstats = get_string('progressstats', 'theme_oklassedu');
        
        $content .= html_writer::start_tag('div', array('class' => 'moreinfo'));
        $content .= '<button type="submit" class="ct-btn white" style="font-size: 14px"><a href="'.$CFG->wwwroot.'/course/report/statistics/modules.php?id='.$course->id.'">'. $learningstats.'</a></button>';
        $content .= '<button type="submit" class="ct-btn white" style="font-size: 14px"><a href="'.$CFG->wwwroot.'/course/report/statistics/progress.php?id='.$course->id.'">'.$progressstats.'</a></button>';
        if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
            if ($course->has_summary() || $course->has_course_contacts() || $course->has_course_overviewfiles()) {
                $url = new moodle_url('/course/info.php', array('id' => $course->id));
                $image = html_writer::empty_tag('img', array('src' => $this->output->pix_url('i/info'),
                    'alt' => $this->strings->summary));
                $content .= html_writer::link($url, $image, array('title' => $this->strings->summary));
                // Make sure JS file to expand course content is included.
                $this->coursecat_include_js();
            }
        }
        $content .= html_writer::end_tag('div'); // .moreinfo
        //라이크 기능 확인 필요 
//        //like 기능
//        require_once($CFG->dirroot . '/local/coursepoint/lib.php');
//        require_once($CFG->dirroot . '/local/coursepoint/index.php');
//        $like = course_like_tag($course);
//        
//        //$content .= $like;
//        $star = course_star_tag($course);
//        $content .= $star;
        // print enrolmenticons
        if ($icons = enrol_get_course_info_icons($course)) {
            $content .= html_writer::start_tag('div', array('class' => 'enrolmenticons'));
            foreach ($icons as $pix_icon) {
                $content .= $this->render($pix_icon);
            }
            $content .= html_writer::end_tag('div'); // .enrolmenticons
        }

        $content .= html_writer::end_tag('div'); // .info

        $content .= html_writer::start_tag('div', array('class' => 'content'));
        $content .= $this->coursecat_coursebox_content($chelper, $course);
        $content .= html_writer::end_tag('div'); // .content
        
        $content .= html_writer::start_tag('div',array('class'=>'mypage_course_newmod'));
        $content .= $this->coursecat_newactivities($course);
        $content .= html_writer::end_tag('div');

        $content .= html_writer::end_tag('div'); // .coursebox
        return $content;
    }
    
    protected function coursecat_coursesumnail_content(coursecat_helper $chelper, $course) {
        //coursebox 수정
        global $CFG, $DB;
        if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
            return '';
        }
        if ($course instanceof stdClass) {
            require_once($CFG->libdir. '/coursecatlib.php');
            $course = new course_in_list($course);
        }
        $content = '';

        // display course overview files
        $contentimages = $contentfiles = '';
        if($course->get_course_overviewfiles()){
            foreach ($course->get_course_overviewfiles() as $file) {
                $isimage = $file->is_valid_image();
                $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                        '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                        $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
                if ($isimage) {
                    $contentimages .= html_writer::tag('div',
                            html_writer::empty_tag('img', array('src' => $url, 'alt' => $course->fullname, 'title' => $course->fullname)),
                            array('class' => 'courseimage'));
                } else {
                    $image = $this->output->pix_icon(file_file_icon($file, 24), $file->get_filename(), 'moodle');
                    $filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
                            html_writer::tag('span', $file->get_filename(), array('class' => 'fp-filename'));
                    $contentfiles .= html_writer::tag('span',
                            html_writer::link($url, $filename),
                            array('class' => 'coursefile fp-filename-icon'));
                }
            }
        }else{
            $url = '/theme/oklassedu/pix/no_img22.jpg';
            $contentimages .= html_writer::tag('div',
                     html_writer::empty_tag('img', array('src' => $url, 'alt' => $course->fullname, 'title' => $course->fullname)),
                    array('class' => 'courseimage'));            
        }
        $content .= $contentimages. $contentfiles;
        
        if ($chelper->get_show_courses() >= self::COURSECAT_SHOW_COURSES_EXPANDED) {
            $nametag = 'h4';
        } else {
            $classes .= ' collapsed';
            $nametag = 'div';
        }
        
        //교과,비교과,온라인 구분
//        $isnonformal = $DB->get_record_sql('select isnonformal FROM {lmsdata_class} WHERE course = :courseid ', array('courseid'=>$course->id));
//        switch ($isnonformal->isnonformal) {
//            case '1' : $isnonformal_text = get_string('irregular','theme_oklassedu'); break;
//            case '2' : $isnonformal_text = get_string('elearning','theme_oklassedu'); break;
//        }
        $isnonformal_text = get_string('irregular','theme_oklassedu');
        $concss = html_writer::tag('span',$isnonformal_text, array('class' => 'isnonformal division_1'));
        // course name
        if (current_language() =='ko'){
            $coursename = $concss.' '.$chelper->get_course_formatted_name($course);
        }else{
            
            $lmsdata = $DB->get_record('lmsdata_class', array('course' =>$course->id ));
            
            $coursename = $concss.' '.$lmsdata->eng_lec_name;
        }
        // course name
//        $coursename = $concss.' '.$chelper->get_course_formatted_name($course);
        $coursenamelink = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),
                                            $coursename, array('class' => $course->visible ? '' : 'dimmed'));
        $content .= html_writer::tag($nametag, $coursenamelink, array('class' => 'coursename'));
        
	$lmsdata = $DB->get_record('lmsdata_class', array('course' =>$course->id ));
        if($lmsdata->year != 0){
            $course_date = $lmsdata->year.get_string('year','theme_oklassedu')." ";
            if($lmsdata->term != 0){
                $course_date .= $lmsdata->term.get_string('term','theme_oklassedu')." ";
                $content .= html_writer::start_tag('ul', array('class' => 'coursedate'));
                $content .= html_writer::tag('li', $course_date);
                $content .= html_writer::end_tag('ul'); // .coursedate
            }
        }

        // display course contacts. See course_in_list::get_course_contacts()
        if ($course->has_course_contacts()) {
            $content .= html_writer::start_tag('ul', array('class' => 'teachers'));
            
            foreach ($course->get_course_contacts() as $userid => $coursecontact) {
                //선생님 표시 변경
                $name .= html_writer::tag('span',$coursecontact['username'],array('class'=>'mypage-teachername'));
                //$rolename = $coursecontact['rolename'].' : ';
                
            }
            $rolename = get_string('teacher','theme_oklassedu').': ';
            $content .= html_writer::tag('li', $rolename.$name);
            $content .= html_writer::end_tag('ul'); // .teachers
        }

        // display course category if necessary (for example in search results)
        if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT) {
            require_once($CFG->libdir. '/coursecatlib.php');
            if ($cat = coursecat::get($course->category, IGNORE_MISSING)) {
                $content .= html_writer::start_tag('div', array('class' => 'coursecat'));
                $content .= get_string('category').': '.
                        html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $cat->id)),
                                $cat->get_formatted_name(), array('class' => $cat->visible ? '' : 'dimmed'));
                $content .= html_writer::end_tag('div'); // .coursecat
            }
        }

        return $content;
    }
    
     /**
     * Returns HTML to display course content (summary, course contacts and optionally category name)
     *
     * This method is called from coursecat_coursebox() and may be re-used in AJAX
     *
     * @param coursecat_helper $chelper various display options
     * @param stdClass|course_in_list $course
     * @return string
     */
    protected function coursecat_coursebox_content(coursecat_helper $chelper, $course) {
        //coursebox 수정
        global $CFG, $DB;
        if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
            return '';
        }
        if ($course instanceof stdClass) {
            require_once($CFG->libdir. '/coursecatlib.php');
            $course = new course_in_list($course);
        }
        $content = '';

        // display course summary
        if ($course->has_summary()) {
            $content .= html_writer::start_tag('div', array('class' => 'summary'));
            $content .= $this->get_course_formatted_summary($course,
                    array('overflowdiv' => true, 'noclean' => true, 'para' => false));
            $content .= html_writer::end_tag('div'); // .summary
        }

        // display course overview files
        $contentimages = $contentfiles = '';
        if($course->get_course_overviewfiles()){
            foreach ($course->get_course_overviewfiles() as $file) {
                $isimage = $file->is_valid_image();
                $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                        '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                        $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
                if ($isimage) {
                    $contentimages .= html_writer::tag('div',
                             html_writer::empty_tag('img', array('src' => $url, 'alt' => $course->fullname, 'title' => $course->fullname)),
                            array('class' => 'courseimage'));
                }
            }
        }else{
            $url = '/theme/oklassedu/pix/no_img105.jpg';
            $contentimages .= html_writer::tag('div',
                     html_writer::empty_tag('img', array('src' => $url, 'alt' => $course->fullname, 'title' => $course->fullname)),
                    array('class' => 'courseimage'));            
        }
        $content .= $contentimages. $contentfiles;
		
	$lmsdata = $DB->get_record('lmsdata_class', array('courseid' =>$course->id ));
        if($lmsdata->year != 0){
            $course_date = $lmsdata->year.get_string('year','theme_oklassedu')." ";
            if($lmsdata->term != 0){
                $course_date .= $lmsdata->term.get_string('term','theme_oklassedu')." ";
                $content .= html_writer::start_tag('ul', array('class' => 'coursedate'));
                $content .= html_writer::tag('li', $course_date);
                $content .= html_writer::end_tag('ul'); // .coursedate
            }
        }

        // display course contacts. See course_in_list::get_course_contacts()
        if ($course->has_course_contacts()) {
            $content .= html_writer::start_tag('ul', array('class' => 'teachers'));
            
            foreach ($course->get_course_contacts() as $userid => $coursecontact) {
                //선생님 표시 변경
                $name .= html_writer::tag('span',$coursecontact['username'],array('class'=>'mypage-teachername'));
                //$rolename = $coursecontact['rolename'].' : ';
                
            }
            $rolename = get_string('teacher','theme_oklassedu').': ';
            $content .= html_writer::tag('li', $rolename.$name);
            $content .= html_writer::end_tag('ul'); // .teachers
        }
        
//        if($course->isnonformal == 1){
//        $tagname = get_string('course_tag','local_lmsdata').': ';
//        $content .= html_writer::start_tag('ul', array('class' => 'tag'));
//        $content .= html_writer::tag('li', $tagname.$lmsdata->tag);
//        $content .= html_writer::end_tag('ul'); // .coursedate
//        }

        // display course category if necessary (for example in search results)
        if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT) {
            require_once($CFG->libdir. '/coursecatlib.php');
            if ($cat = coursecat::get($course->category, IGNORE_MISSING)) {
                $content .= html_writer::start_tag('div', array('class' => 'coursecat'));
                $content .= get_string('category').': '.
                        html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $cat->id)),
                                $cat->get_formatted_name(), array('class' => $cat->visible ? '' : 'dimmed'));
                $content .= html_writer::end_tag('div'); // .coursecat
            }
        }

        return $content;
    }
    
     /**
     * Returns given course's summary with proper embedded files urls and formatted
     *
     * @param course_in_list $course
     * @param array|stdClass $options additional formatting options
     * @return string
     */
    protected function get_course_formatted_summary($course, $options = array()) {
        //summary를 가져온다
        global $CFG;
        require_once($CFG->libdir. '/filelib.php');
        if (!$course->has_summary()) {
            return '';
        }
        $options = (array)$options;
        $context = context_course::instance($course->id);

        //$summary .=get_string('summary').' : ';
        $summary .= file_rewrite_pluginfile_urls($course->summary, 'pluginfile.php', $context->id, 'course', 'summary', null);
        $summary = format_text($summary, $course->summaryformat, $options, $course->id);
        if (!empty($this->searchcriteria['search'])) {
            $summary = highlight($this->searchcriteria['search'], $summary);
        }
        return $summary;
    }
    
    protected function coursecat_newactivities($course){
        //공지, 과제, 퀴즈, 토론 4가지 주요 학습활동에 대해 카운팅을 가져온다.
        global $CFG;
        $content = '';
        $content .= html_writer::start_tag('ul');
        $newactivities = array('jinotechboard','assign','quiz','forum');
        foreach($newactivities as $newactivity){
            $modname = ($newactivity=='jinotechboard')? get_string('boardtype:notice','mod_'.$newactivity):get_string('pluginname','mod_'.$newactivity);
            $count = $this->coursecat_newactivity_count($course,$newactivity);
//            if($count->total>0){
//                $url = new moodle_url($CFG->wwwroot.'/mod/'.$newactivity.'/index.php?id='.$course->id);
//                $content .= html_writer::tag('li', '<a href="'.$url.'">'.$modname.' ('.$count->new.'/'.$count->total.')</a>');
//            }
        }

        
        $content .= html_writer::end_tag('ul');
        return $content;
    }
    
    
    protected function coursecat_newactivity_count($course,$modulename=''){
        
        global $CFG, $DB;
        
        $count = new stdClass();
        
        $sql = ' SELECT count(*) 
              FROM {course_modules} cm
              JOIN {'.$modulename.'} at ON at.id = cm.instance 
              JOIN {modules} mo ON mo.id = cm.module
              JOIN {course} c ON c.id = cm.course ';
        $where[]  = ' cm.course = :course ';
        $where[]  = ' cm.visible = :visible1 ';
        $where[]  = ' mo.visible = :visible2 ';
        
        $params = array(
                'course' => $course->id,
                'visible1' => 1,
                'visible2' => 1
            );
        
        if($modulename){
            $where[]  = ' mo.name = :modulename ';
            $params['modulename'] = $modulename;
        }
        
        if($modulename=='jinotechboard') {
            $where[] = ' at.type = :type ';
            $params['type'] = '1';
        }
               
        $sqlwhere = ' WHERE '.implode(' AND ', $where);
        $count->total = $DB->count_records_sql($sql.$sqlwhere, $params);
        
        $where[] = ' cm.added >= :newdate ';
        $newdate = time() - (24*60*60*7);
        $params['newdate'] = $newdate;
                
        $sqlwhere = ' WHERE '.implode(' AND ', $where);
        $count->new = $DB->count_records_sql($sql.$sqlwhere, $params);
        
        return $count;
    }
    
    private function new_activity($course) {
        global $CFG, $USER, $DB;

        if ($USER->lastcourseaccess[$course->id] > 0) {
            $lastaccess = $USER->lastcourseaccess[$course->id];
        } else {
            $lastaccess = 0;
        }

        $sql = "SELECT count(*) FROM {course_modules} " .
                "WHERE course = :courseid AND added > :lastaccess";

        $params = array(
            'courseid' => $course->id,
            'lastaccess' => $lastaccess);

        $activity = $DB->count_records_sql($sql, $params);
        
        return $activity;
    }
    
    public function frontpage_available_courses() {
        /* available courses */
        global $CFG, $OUTPUT;
        require_once($CFG->libdir. '/coursecatlib.php');

        $chelper = new coursecat_helper();
        $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->set_courses_display_options(array(
            'recursive' => true,
            'limit' => $CFG->frontpagecourselimit,
            'viewmoreurl' => new moodle_url('/course/index.php'),
            'viewmoretext' => new lang_string('fulllistofcourses')
        ));

        $chelper->set_attributes(array('class' => 'frontpage-course-list-all'));
        $courses = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
        $totalcount = coursecat::get(0)->get_courses_count($chelper->get_courses_display_options());

        $rcourseids = array_keys($courses);
        $acourseids = array_chunk($rcourseids, 4);
        $newcourse = get_string('availablecourses');

        $header = '<div id="frontpage-course-list">
        <h2>'.$newcourse.'</h2>
        <div class="courses frontpage-course-list-all">';

        $footer = '
        </div>
        </div>';

        $content = '';

        if (count($rcourseids) > 0) {
            foreach ($acourseids as $courseids) {
                $content .= '<div class="row-fluid">';
                $rowcontent = '';

                foreach ($courseids as $courseid) {
                    $course = get_course($courseid);

                    $no = get_config('theme_oklassedu', 'patternselect');
                    $nimgp = (empty($no)||$no == "default") ? 'cs00/no-image' : 'cs0'.$no.'/no-image';

                    $noimgurl = $OUTPUT->pix_url($nimgp, 'theme');
                    $courseurl = new moodle_url('/course/view.php', array('id' => $courseid ));

                    if ($course instanceof stdClass) {
                        require_once($CFG->libdir. '/coursecatlib.php');
                        $course = new course_in_list($course);
                    }

                    $imgurl = '';
                    $context = context_course::instance($course->id);

                    foreach ($course->get_course_overviewfiles() as $file) {
                        $isimage = $file->is_valid_image();
                        $imgurl = file_encode_url("$CFG->wwwroot/pluginfile.php",
                            '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                            $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
                        if (!$isimage) {
                            $imgurl = $noimgurl;
                        }
                    }

                    if (empty($imgurl)) {
                        $imgurl = $noimgurl;
                    }

                    $rowcontent .= '<div class="span3">
                        <div class="fp-coursebox">
                        <div class="fp-coursethumb">
                        <a href="'.$courseurl.'">
                        <img src="'.$imgurl.'" width="243" height="165" alt="'.$course->fullname.'">
                        </a>
                        </div>
                        <div class="fp-courseinfo">
                        <h5><a href="'.$courseurl.'">'.$course->fullname.'</a></h5>
                        </div>
                        </div>
                        </div>';
                }
                $content .= $rowcontent;
                $content .= '</div>';
            }
        }

        $coursehtml = $header.$content.$footer;
        echo $coursehtml;

        if (!$totalcount && !$this->page->user_is_editing() && has_capability('moodle/course:create', context_system::instance())) {
            // Print link to create a new course, for the 1st available category.
            echo $this->add_new_course_button();
        }
    }

    public function promoted_courses() {
        global $CFG, $OUTPUT, $DB;

        $pcourseenable = theme_oklassedu_get_setting('pcourseenable');
        if (!$pcourseenable) {
            return false;
        }

        $featuredcontent = '';
        /* Get Featured courses id from DB */
        $featuredids = theme_oklassedu_get_setting('promotedcourses');
        $rcourseids = (!empty($featuredids)) ? explode(",", $featuredids) : array();
        if (empty($rcourseids)) {
            return false;
        }

        $hcourseids = theme_oklassedu_hidden_courses_ids();

        if (!empty($hcourseids)) {
            foreach ($rcourseids as $key => $val) {
                if (in_array($val, $hcourseids)) {
                    unset($rcourseids[$key]);
                }
            }
        }

        foreach ($rcourseids as $key => $val) {
            $ccourse = $DB->get_record('course', array('id' => $val));
            if (empty($ccourse)) {
                unset($rcourseids[$key]);
                continue;
            }
        }

        if (empty($rcourseids)) {
            return false;
        }

        $fcourseids = array_chunk($rcourseids, 4);
        $totalfcourse = count($fcourseids);
        $promotedtitle = theme_oklassedu_get_setting('promotedtitle', 'format_text');
        $promotedtitle = theme_oklassedu_lang($promotedtitle);

        $featuredheader = '<div class="custom-courses-list" id="Promoted-Courses">
							  <div class="container-fluid">
								<div class="titlebar with-felements">
									<h2 style="margin-bottom:20px;">'.$promotedtitle.'</h2>
									<div class="slidenav pagenav">
										<button class="nav-item nav-prev slick-prev">
										<i class="fa fa-chevron-right"></i><i class="fa fa-chevron-left"></i>
										</button>
										<button class="nav-item nav-next slick-next">
										<i class="fa fa-chevron-right"></i><i class="fa fa-chevron-left"></i>
										</button>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="promoted_courses" data-crow="'.$totalfcourse.'">';

        $featuredfooter = ' </div>
                            </div>
                            </div>';

        if (!empty($fcourseids)) {
            foreach ($fcourseids as $courseids) {
                $rowcontent = '<div><div class="row-fluid">';

                foreach ($courseids as $courseid) {
                    $course = get_course($courseid);
                    $no = get_config('theme_oklassedu', 'patternselect');
                    $nimgp = (empty($no)|| $no == "default") ? 'cs00/no-image' : 'cs0'.$no.'/no-image';

                    $noimgurl = $OUTPUT->pix_url($nimgp, 'theme');

                    $courseurl = new moodle_url('/course/view.php', array('id' => $courseid ));

                    if ($course instanceof stdClass) {
                        require_once($CFG->libdir. '/coursecatlib.php');
                        $course = new course_in_list($course);
                    }

                    $imgurl = '';

                    $summary = theme_oklassedu_strip_html_tags($course->summary);
                    $summary = theme_oklassedu_course_trim_char($summary, 75);

                    $context = context_course::instance($course->id);
                    $nostudents = count_role_users(5, $context);

                    foreach ($course->get_course_overviewfiles() as $file) {
                        $isimage = $file->is_valid_image();
                        $imgurl = file_encode_url("$CFG->wwwroot/pluginfile.php",
                        '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                        $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
                        if (!$isimage) {
                            $imgurl = $noimgurl;
                        }
                    }

                    if (empty($imgurl)) {
                        $imgurl = $noimgurl;
                    }

                        $coursehtml = '<div class="span3">
                            <div class="course-box">
                            <div class="thumb"><a href="'.$courseurl.'">
							<img src="'.$imgurl.'" alt="'.$course->fullname.'"></a></div>
                            <div class="info">
                            <h5><a href="'.$courseurl.'">'.$course->fullname.'</a></h5>
                            </div>
                            </div>
                            </div>';

                        $rowcontent .= $coursehtml;

                }

                    $rowcontent .= '</div></div>';
                    $featuredcontent .= $rowcontent;
            }

        }

        $featuredcourses = $featuredheader.$featuredcontent.$featuredfooter;
        return $featuredcourses;
    }
    
    function local_course_like__array() {
        alert(2);
    }

    
    /**
     * 파일(resource)을 강좌에 추가했을 때 filter/docuview를 적용하기 위해 override
     * 
     * @global type $CFG
     * @global type $DB
     * @param cm_info $mod
     * @param type $displayoptions
     * @return type
     */
    public function course_section_cm_name_title(cm_info $mod, $displayoptions = array()) {
        global $CFG, $DB;
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            // Nothing to be displayed to the user.
            return $output;
        }
        $url = $mod->url;
        if (!$url) {
            return $output;
        }

        //Accessibility: for files get description via icon, this is very ugly hack!
        $instancename = $mod->get_formatted_name();
        $altname = $mod->modfullname;
        // Avoid unnecessary duplication: if e.g. a forum name already
        // includes the word forum (or Forum, etc) then it is unhelpful
        // to include that in the accessible description that is added.
        if (false !== strpos(core_text::strtolower($instancename),
                core_text::strtolower($altname))) {
            $altname = '';
        }
        // File type after name, for alphabetic lists (screen reader).
        if ($altname) {
            $altname = get_accesshide(' '.$altname);
        }

        // For items which are hidden but available to current user
        // ($mod->uservisible), we show those as dimmed only if the user has
        // viewhiddenactivities, so that teachers see 'items which might not
        // be available to some students' dimmed but students do not see 'item
        // which is actually available to current student' dimmed.
        $linkclasses = '';
        $accesstext = '';
        $textclasses = '';
        if ($mod->uservisible) {
            $conditionalhidden = $this->is_cm_conditionally_hidden($mod);
            $accessiblebutdim = (!$mod->visible || $conditionalhidden) &&
                has_capability('moodle/course:viewhiddenactivities', $mod->context);
            if ($accessiblebutdim) {
                $linkclasses .= ' dimmed';
                $textclasses .= ' dimmed_text';
                if ($conditionalhidden) {
                    $linkclasses .= ' conditionalhidden';
                    $textclasses .= ' conditionalhidden';
                }
                // Show accessibility note only if user can access the module himself.
                $accesstext = get_accesshide(get_string('hiddenfromstudents').':'. $mod->modfullname);
            }
        } else {
            $linkclasses .= ' dimmed';
            $textclasses .= ' dimmed_text';
        }

        // Get on-click attribute value if specified and decode the onclick - it
        // has already been encoded for display (puke).
        $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);

        $groupinglabel = $mod->get_grouping_label($textclasses);

        // Display link itself.
        $activitylink = html_writer::empty_tag('img', array('src' => $mod->get_icon_url(),
                'class' => 'iconlarge activityicon', 'alt' => ' ', 'role' => 'presentation')) . $accesstext .
                html_writer::tag('span', $instancename . $altname, array('class' => 'instancename'));
        if ($mod->uservisible) {
            $output .= html_writer::link($url, $activitylink, array('class' => $linkclasses, 'onclick' => $onclick)) .
                    $groupinglabel;
        } else {
            // We may be displaying this just in order to show information
            // about visibility, without the actual link ($mod->uservisible)
            $output .= html_writer::tag('div', $activitylink, array('class' => $textclasses)) .
                    $groupinglabel;
        }
        
        // 요기부터 filter/docuview 적용
        // filter/docuview 가 설치되어 있는지 체크
        if(file_exists($CFG->dirroot.'/filter/docuview/lib.php')) {
            require_once ($CFG->dirroot.'/filter/docuview/lib.php');
            
            if($mod->modname == 'resource') { // resource 인 경우만 적용
                $modcontext = context_module::instance($mod->id);
                
                $exts = filter_docuview_get_supported_extensions();

                $fs = get_file_storage();
                $files = $fs->get_area_files($modcontext->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
                foreach($files as $file) {
                    $filename = $file->get_filename();
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if(in_array($ext, $exts)) {
                        $output .= ' '.filter_docuview_viewurl($file->get_id(), $filename);
                    }
                }
            }
        }
        
        return $output;
    }
}