<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's oklassedu theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if (is_siteadmin()) {
    
    $ADMIN->add('themes', new admin_category('theme_oklassedu', 'oklassedu'));
    
    /* General Settings */
    $temp = new admin_settingpage('theme_oklassedu_general', get_string('themegeneralsettings', 'theme_oklassedu'));

    // Logo file setting.
    $name = 'theme_oklassedu/logo';
    $title = get_string('logo','theme_oklassedu');
    $description = get_string('logodesc', 'theme_oklassedu');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Footer Logo file setting.
    $name = 'theme_oklassedu/footerlogo';
    $title = get_string('footerlogo','theme_oklassedu');
    $description = get_string('footerlogodesc', 'theme_oklassedu');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'footerlogo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Small logo file setting.
    $name = 'theme_oklassedu/smalllogo';
    $title = get_string('smalllogo', 'theme_oklassedu');
    $description = get_string('smalllogodesc', 'theme_oklassedu');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'smalllogo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Show site name along with small logo.
    $name = 'theme_oklassedu/sitename';
    $title = get_string('sitename', 'theme_oklassedu');
    $description = get_string('sitenamedesc', 'theme_oklassedu');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Custom CSS file.
    $name = 'theme_oklassedu/customcss';
    $title = get_string('customcss', 'theme_oklassedu');
    $description = get_string('customcssdesc', 'theme_oklassedu');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Promoted courses Block title.
    $name = 'theme_oklassmoocs/promotedtitle';
    $title = get_string('pcourses', 'theme_oklassmoocs') . ' ' . get_string('title', 'theme_oklassmoocs');
    $description = get_string('promotedtitledesc', 'theme_oklassmoocs');
    $default = 'lang:promotedtitledefault';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_oklassmoocs/promotedcourses';
    $title = get_string('pcourses', 'theme_oklassmoocs');
    $description = get_string('pcoursesdesc', 'theme_oklassmoocs');
    $default = array();

    $courses[0] = '';
    $cnt = 0;
    if ($ccc = get_courses('all', 'c.sortorder ASC', 'c.id,c.shortname,c.visible,c.category')) {
        foreach ($ccc as $cc) {
            if ($cc->visible == "0" || $cc->id == "1") {
                continue;
            }
            $cnt++;
            $courses[$cc->id] = $cc->shortname;
            // Set some courses for default option.
            if ($cnt < 8) {
                $default[] = $cc->id;
            }
        }
    }
    $coursedefault = implode(",", $default);
    $setting = new admin_setting_configtextarea($name, $title, $description, $coursedefault);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    // Promoted Courses End.

    $ADMIN->add('theme_oklassedu', $temp);
    
    /* Footer Settings start */
    $temp = new admin_settingpage('theme_oklassedu_footer', get_string('footerheading', 'theme_oklassedu'));

    // Footer Block1.
    $name = 'theme_oklassedu_footerblock1heading';
    $heading = get_string('footerblock', 'theme_oklassedu') . ' 1 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    /* Footer Content */
    $name = 'theme_oklassedu/footnote';
    $title = get_string('footnote', 'theme_oklassedu');
    $description = get_string('footnotedesc', 'theme_oklassedu');
    $default = 'lang:footnotedefault';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    /* Footer Block1. */

    /* Footer Block2. */
    $name = 'theme_oklassedu_footerblock2heading';
    $heading = get_string('footerblock', 'theme_oklassedu') . ' 2 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    $name = 'theme_oklassedu/footerbtitle2';
    $title = get_string('footerblock', 'theme_oklassedu') . ' ' . get_string('title', 'theme_oklassedu') . ' 2 ';
    $description = get_string('footerbtitle_desc', 'theme_oklassedu');
    $default = 'lang:footerbtitle2default';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_oklassedu/footerblink2';
    $title = get_string('footerblink', 'theme_oklassedu') . ' 2';
    $description = get_string('footerblink_desc', 'theme_oklassedu');
    $default = get_string('footerblink2default', 'theme_oklassedu');
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    /* Footer Block2 */

    /* Footer Block3 */
    $name = 'theme_oklassedu_footerblock3heading';
    $heading = get_string('footerblock', 'theme_oklassedu') . ' 3 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    $name = 'theme_oklassedu/footerbtitle3';
    $title = get_string('footerblock', 'theme_oklassedu') . ' ' . get_string('title', 'theme_oklassedu') . ' 3 ';
    $description = get_string('footerbtitle_desc', 'theme_oklassedu');
    $default = 'lang:footerbtitle3default';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Facebook,Pinterest,Twitter,Google+ Settings */
    $name = 'theme_oklassedu/fburl';
    $title = get_string('fburl', 'theme_oklassedu');
    $description = get_string('fburldesc', 'theme_oklassedu');
    $default = get_string('fburl_default', 'theme_oklassedu');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_oklassedu/twurl';
    $title = get_string('twurl', 'theme_oklassedu');
    $description = get_string('twurldesc', 'theme_oklassedu');
    $default = get_string('twurl_default', 'theme_oklassedu');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_oklassedu/gpurl';
    $title = get_string('gpurl', 'theme_oklassedu');
    $description = get_string('gpurldesc', 'theme_oklassedu');
    $default = get_string('gpurl_default', 'theme_oklassedu');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_oklassedu/pinurl';
    $title = get_string('pinurl', 'theme_oklassedu');
    $description = get_string('pinurldesc', 'theme_oklassedu');
    $default = get_string('pinurl_default', 'theme_oklassedu');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    /* Footer Block3. */

    /* Footer Block4. */
    $name = 'theme_oklassedu_footerblock4heading';
    $heading = get_string('footerblock', 'theme_oklassedu') . ' 4 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);


    /* Address , Phone No ,Email */
    $name = 'theme_oklassedu/address';
    $title = "주소";
    $description = '';
    $default = "주소";
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_oklassedu/phoneno';
    $title = get_string('phoneno', 'theme_oklassedu');
    $description = '';
    $default = get_string('defaultphoneno', 'theme_oklassedu');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_oklassedu/count';
    $title = "계좌번호";
    $description = '';
    $default = 계좌번호;
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    /* Footer Block4 */

    $ADMIN->add('theme_oklassedu', $temp);
    /*  Footer Settings end */

   

    // Slideshow Settings Start.
    $temp = new admin_settingpage('theme_oklassedu_slideshow', get_string('slideshowheading', 'theme_oklassedu'));
    $temp->add(new admin_setting_heading('theme_oklassedu_slideshow', get_string('slideshowheadingsub', 'theme_oklassedu'), format_text(get_string('slideshowdesc', 'theme_oklassedu'), FORMAT_MARKDOWN)));

    // Display Slideshow.
    $name = 'theme_oklassedu/toggleslideshow';
    $title = get_string('toggleslideshow', 'theme_oklassedu');
    $description = get_string('toggleslideshowdesc', 'theme_oklassedu');
    $yes = get_string('yes');
    $no = get_string('no');
    $default = 1;
    $choices = array(1 => $yes, 0 => $no);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Number of slides.
    $name = 'theme_oklassedu/numberofslides';
    $title = get_string('numberofslides', 'theme_oklassedu');
    $description = get_string('numberofslides_desc', 'theme_oklassedu');
    $default = 3;
    $choices = array(
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
    );
    $temp->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    $numberofslides = get_config('theme_oklassedu', 'numberofslides');
    for ($i = 1; $i <= $numberofslides; $i++) {

        // This is the descriptor for Slide One.
        $name = 'theme_oklassedu/slide' . $i . 'info';
        $heading = get_string('slideno', 'theme_oklassedu', array('slide' => $i));
        $information = get_string('slidenodesc', 'theme_oklassedu', array('slide' => $i));
        $setting = new admin_setting_heading($name, $heading, $information);
        $temp->add($setting);

        // Slide Image.
        $name = 'theme_oklassedu/slide' . $i . 'image';
        $title = get_string('slideimage', 'theme_oklassedu');
        $description = get_string('slideimagedesc', 'theme_oklassedu');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide' . $i . 'image');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // Slide Caption.
        $name = 'theme_oklassedu/slide' . $i . 'caption';
        $title = get_string('slidecaption', 'theme_oklassedu');
        $description = get_string('slidecaptiondesc', 'theme_oklassedu');
        $default = 'lang:slidecaptiondefault';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // Slide Link text.
        $name = 'theme_oklassedu/slide' . $i . 'urltext';
        $title = get_string('slideurltext', 'theme_oklassedu');
        $description = get_string('slideurltextdesc', 'theme_oklassedu');
        $default = 'lang:knowmore';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // Slide destination link

        $name = 'theme_oklassedu/slide' . $i . 'url';
        $title = get_string('slideurl', 'theme_oklassedu');
        $description = get_string('slideurldesc', 'theme_oklassedu');
        $default = 'http://www.example.com/';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);
    }

    $ADMIN->add('theme_oklassedu', $temp);
    /* Slideshow Settings End */
    
}
