<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The two column layout.
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);
///////////////////////////    

global $DB;

$separator_self = explode(DIRECTORY_SEPARATOR, $_SERVER['REQUEST_URI']);
$topmenu_self = array_pop($separator_self);
$url_org = $_SERVER["REQUEST_URI"];
$url = str_replace($CFG->wwwroot, '', $_SERVER["REQUEST_URI"]);
$url_exp = explode('?', $url)[0];
$url_exp1 = explode('?', $url)[1];
$dir_u = explode('/', $url_exp);
$path_depth = count($dir_u);
$d_name = null;
if ($path_depth > 2) {
    $d_name = $dir_u[$path_depth - 2];
}
$f_name = $dir_u[$path_depth - 1];

if (empty($d_name)) {
    $search = "/" . $f_name;
} else {
    if ($d_name == 'jinoboard') {
        if ($f_name == 'list.php' || $f_name == 'mylist.php') {
            $search = "%" . $d_name . "/%id=" . $_REQUEST['id'];
        } else {
            $search = "%" . $d_name . "/%id=" . $_REQUEST['board'];
        }
    } else if ($f_name == 'course_list.php') {
        $search = "%" . $d_name . "/%menu_gubun=" . $_REQUEST['menu_gubun'];
    } else if ($f_name == 'application.php') {
        $search = "%" . $d_name . "/%menu_gubun=" . $_REQUEST['menu_gubun'];
    } else if ($f_name == 'course_list_detail.php') {
        $search = "%" . $d_name . "/%menu_gubun=" . $_REQUEST['menu_gubun'];
    } else if ($f_name == 'application_edit.php') {
        $search = "%" . $d_name . "/mycourse.php";
    } else if ($d_name == 'evaluation') {
        if ($f_name == 'survey.php') {
            $search = "%" . $d_name . "/index.php?type=2%";
        } else {
            $search = "%" . $d_name . "/" . $f_name . "%";
        }
    } else {
        $search = "%" . $d_name . "/" . $f_name . '%';
    }
}

// Set default (LTR) layout mark-up for a two column page (side-pre-only).
$regionmain = 'span10 pull-right';
$sidepre = 'span2 desktop-first-column';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmain = 'span10';
    $sidepre = 'span2 pull-right';
}

echo $OUTPUT->doctype();

$first_menus = $DB->get_records('main_menu', array('depth' => 1, 'isused' => 1), 'step asc');
$second_menus = $DB->get_records('main_menu', array('depth' => 2, 'isused' => 1), 'step asc');
$third_menus = $DB->get_records('main_menu', array('depth' => 3, 'isused' => 1), 'step asc');
$myusergroup = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id));
if (empty($myusergroup)) {
    $myusergroup = 'rs';
}
if ($myusergroup == 'sa') {
    $myusergroup = 'pr';
}

$sqlmenu = 'SELECT * 
                    FROM {main_menu} mm 
                    JOIN {main_menu_name} mmn on mmn.menuid = mm.id 
                    WHERE mm.url LIKE :urls and mmn.lang = :lang and mm.isused = 1 order by step asc';
$parentsql = 'SELECT mm.id, mmn.name, mm.url 
                    FROM {main_menu} mm 
                    JOIN {main_menu_name} mmn on mmn.menuid = mm.id 
                    WHERE mm.id = :parent and mmn.lang = :lang and mm.depth = :depth and mm.isused = 1 order by step asc';
$parentsql2 = 'SELECT mm.id, mmn.name, mm.url 
                    FROM {main_menu} mm 
                    JOIN {main_menu_name} mmn on mmn.menuid = mm.id 
                    WHERE mm.url LIKE :urls and mm.parent = :parent and mmn.lang = :lang and mm.depth = :depth and mm.isused = 1 order by step asc';
$menumenuname = '';
?> 

<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false ? "Master Korean" : "Master Korean jobs"; ?></title>
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0"/>--> 
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet"/>
    </head>

    <body <?php echo $OUTPUT->body_attributes('custom'); ?>>
        <div id="wrap"> 
            <?php echo $OUTPUT->standard_top_of_body_html() ?>
            <!-- header start -->
            <?php require_once(dirname(__FILE__) . '/includes/header.php'); ?>   
            <div class="cont">
                <?php
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
                echo $OUTPUT->course_content_footer();
                ?>
            </div>
            <!-- footer start -->
            <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>   
            <!-- footer end --> 
            <?php echo $OUTPUT->standard_end_of_body_html() ?>

        </div>

    </div>
</body>
</html>
