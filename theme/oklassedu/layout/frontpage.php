<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo get_visang_config('javascript_header'); ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/theme/oklassedu/style/icon/css/all.min.css"  />
<!--    <link rel="preconnect" href="https://fonts.gstatic.com">-->
    <link rel="preconnect" href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,700;1,700&display=swap" rel="stylesheet">
    <title>Master Korean</title>
    <?php require_once(dirname(__FILE__) . '/homepage/css.php'); ?>
    <style>
        .bg-course-recommned {
            background: url("/theme/oklassedu/pix/banner/banner-2.webp") no-repeat center;
        }
        .bg-course-employer {
            background: url("/theme/oklassedu/pix/banner/banner-3.webp") no-repeat center;
        }

        .bg-course-popular {
            background: url("/theme/oklassedu/pix/banner/banner-1.webp") no-repeat center;
        }
        .job-logo {
            background:url("/theme/oklassedu/pix/images/hd_m_logo_job.png") no-repeat center;
        }
        .blog-logo {
            background:url("/theme/oklassedu/pix/images/mk-blog.webp") no-repeat center;
            height: 20px;
            background-size: contain !important;
        }
        .blog-logo a {
            opacity: 0;
        }
    </style>
</head>
<body>
    <?php echo get_visang_config('javascript_body'); ?>

    <?php require_once(dirname(__FILE__) . '/homepage/repo.php'); ?>

    <!--  menu  -->
    <?php require_once(dirname(__FILE__) . '/homepage/header.php'); ?>
    <!--    end menu-->

    <!--  banner  -->
    <?php require_once(dirname(__FILE__) . '/homepage/banner.php'); ?>
    <!--    end banner-->

    <!--  popular  -->
    <?php require_once(dirname(__FILE__) . '/homepage/course-popular.php'); ?>
    <!--    end popular-->

    <!--  recommend  -->
    <?php require_once(dirname(__FILE__) . '/homepage/course-recommend.php'); ?>
    <!--    end recommend-->

    <!--  employer  -->
    <?php require_once(dirname(__FILE__) . '/homepage/employer.php'); ?>
    <!--    end employer-->

    <!--  feedback  -->
    <?php require_once(dirname(__FILE__) . '/homepage/feedback.php'); ?>
    <!--    end feedback-->

    <!--  feedback  -->
    <?php require_once(dirname(__FILE__) . '/homepage/footer.php'); ?>
    <!--    end feedback-->

    <?php require_once(dirname(__FILE__) . '/homepage/js.php'); ?>

    <script type="text/javascript">
        if ('loading' in HTMLImageElement.prototype) {const images = document.querySelectorAll('img[loading="lazy"]'); images.forEach(img => { img.src = img.dataset.src; }); } else { const script = document.createElement('script');script.src = '/theme/oklassedu/javascript/new-js/lazysizes.min.js';document.body.appendChild(script);}
    </script>
    <div class="main-content-none">
        <?php echo $OUTPUT->main_content(); ?>
    </div>

    <?php echo get_visang_config('javascript_footer'); ?>
</body>
</html>
