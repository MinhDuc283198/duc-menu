<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The two column layout.
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);

// Set default (LTR) layout mark-up for a two column page (side-pre-only).
$regionmain = 'span10 pull-right';
$sidepre = 'span2 desktop-first-column';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmain = 'span10';
    $sidepre = 'span2 pull-right';
}

echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet"/>
    </head>

    <body <?php echo $OUTPUT->body_attributes('custom'); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html() ?>
        <?php require_once(dirname(__FILE__) . '/includes/header.php'); 
        $today = time();
        $board = $DB->get_record_sql('SELECT jb.type, jb.name, jb.engname, jb.newday,(SELECT timemodified FROM {jinoboard_contents} WHERE jb.type = board and userid = :userid ORDER BY timemodified DESC LIMIT 1) as timemodified
                                            FROM {jinoboard} jb WHERE jb.type = 2',array('userid'=>$USER->id));
        $userinfos = $DB->get_records_sql('SELECT * FROM {user} u JOIN {lmsdata_user} lu on u.id=lu.userid WHERE lu.userid = :userid',array('userid'=>$USER->id));
        foreach($userinfos as $userinfo){
            $prof = $userinfo->usergroup == 'pr' ? 'pr' : '';
        }

        ?>   
        <div class="cont">
            <div class="group">

                <div class="crs-left-block">
                    <ul>
                        <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mypage.php"><?php echo get_string('myinfo','theme_oklassedu');?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mycourse.php<?php if($prof=='pr'){ echo '?type=1'; }?>"><?php echo get_string('mycourse','theme_oklassedu');?></a></li>
                        <li><a href="<?php echo $CFG->job_url; ?>/local/job/coupon/mycoupons.php"><?php echo get_string('mycoupons','theme_oklassedu');?></a></li>
                        <?php if($userinfo->statuscode){ ?>
                        <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/inter.php "><?php echo get_string('Interestinformation', 'local_job'); ?></a></li>
                        <?php } ?>
                        <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/notification.php"><?php echo get_string('notice2','theme_oklassedu');?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot; ?>/local/jinoboard/index.php?type=2"><?php echo get_string('qna','local_mypage');?> <?php if($board->newday){ echo ($board->timemodified + (86400*$board->newday))>$today ? '<span class="ic-new">N</span>' : '' ;} ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mypayment.php"><?php echo get_string('paymentlist','local_mypage');?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot; ?>/local/my/info.php"><?php echo get_string('myinfoedit','local_mypage');?></a></li>
                    </ul>
                    
                </div>




                <div class="crs-right-block">
                    <?php
                    echo $OUTPUT->course_content_header();
                    echo $OUTPUT->main_content();
                    echo $OUTPUT->course_content_footer();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>   
    <!-- footer end --> 
    <?php echo $OUTPUT->standard_end_of_body_html() ?>

</div>
</body>
</html>
