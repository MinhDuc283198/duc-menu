<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The two column layout.
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);
$type = optional_param('type', 1, PARAM_INT);
// Set default (LTR) layout mark-up for a two column page (side-pre-only).
$regionmain = 'span10 pull-right';
$sidepre = 'span2 desktop-first-column';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmain = 'span10';
    $sidepre = 'span2 pull-right';
}

echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet"/>
    </head>

    <body <?php echo $OUTPUT->body_attributes('custom'); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html() ?>
        <?php require_once(dirname(__FILE__) . '/includes/header.php'); 
        $today = time();
        if( $type > 3){
            $where =' WHERE jb.type > 3 and jb.site = 0  ';
            $url = '/local/jinoboard/board.php?type=';
        } else { 
            $where =' WHERE jb.type <= 3 ';
            $url = '/local/jinoboard/index.php?type=';
        }
        $boards = $DB->get_records_sql('SELECT jb.type, jb.name, jb.engname,jb.vnname, jb.newday,(SELECT timemodified FROM {jinoboard_contents} WHERE jb.type = board ORDER BY timemodified DESC LIMIT 1) as timemodified
                                            FROM {jinoboard} jb'.$where);
        ?>   
        <div class="cont">
            <div class="group">

                <div class="crs-left-block">
                    <ul>
                        <?php foreach($boards as $board){
                             switch (current_language()){
                                case 'ko' :
                                    $boardname = $board->name;
                                    break;
                                case 'en' :
                                    $boardname = $board->engname;
                                    break;
                                case 'vi' :
                                    $boardname = $board->vnname;
                                    break;
                            }
                            ?>
                                <li>
                                    <a href="<?php echo $board->type==2 ? '/local/jinoboard/write.php?type=2' : $url.$board->type;?>&b_t=custom">
                                    <?php echo $boardname; ?> 
                                    <?php if($board->newday&&$board->type!=2){ echo ($board->timemodified + (86400*$board->newday))>$today ? '<span class="ic-new">N</span>' : '' ;} ?>
                                    </a>
                                </li>
                            <?php
                        } ?>
                    </ul>
                </div>




                <div class="crs-right-block">
                    <?php
                    echo $OUTPUT->course_content_header();
                    echo $OUTPUT->main_content();
                    echo $OUTPUT->course_content_footer();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>   
    <!-- footer end --> 
    <?php echo $OUTPUT->standard_end_of_body_html() ?>

</div>
</body>
</html>
