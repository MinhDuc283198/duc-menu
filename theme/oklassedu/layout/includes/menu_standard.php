<?php 
global $DB;
if(has_capability('moodle/course:create', context_system::instance()) || $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id)) == 'pr') {
    $menuauth = 1;
}else{
    $menuauth = 0;
}
?>


<ul id="myMenu" class="span2">     
    <li>
        <a href="#">
            <h3 class="mymenu-mypage">
                <i class="fa fa-address-book-o fa-2x" aria-hidden="true"></i>
                <?php echo get_string('menu:mypage','theme_oklassedu');?> <span class="open"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
            </h3>
        </a>
        <ul id="menu01">
            <li class="mymenu-page-my-index"><a href="/my/"><?php echo get_string('menu:dashboard','theme_oklassedu');?></a></li>
            <li class="mymenu-page-user-files"><a href="/user/files.php"><?php echo get_string('menu:filemanage','theme_oklassedu');?></a></li>
            <?php if($menuauth == 1) {?>
            <li class="mymenu-path-local-courselist"><a href="/local/courselist/course_manage.php"><?php echo get_string('menu:coursemanage','theme_oklassedu');?></a></li>
            <li class="mymenu-path-local-repository"><a href="/local/repository/"><?php echo get_string('menu:lcmsmanage','theme_oklassedu');?></a></li>
            <?php }?>
            <li class="mymenu-path-calendar"><a href="/calendar/view.php?view=month"><?php echo get_string('menu:schedule','theme_oklassedu');?></a></li>
            <li class="mymenu-path-message"><a href="/message/"><?php echo get_string('menu:message','theme_oklassedu');?></a></li>
            <li class="mymenu-page-local-courselist-courseoverview"><a href="/local/courselist/courseoverview.php"><?php echo get_string('menu:coursenotification','theme_oklassedu');?></a></li>
            <li class="mymenu-path-local-board"><a href="/local/board/"><?php echo get_string('menu:courseboard','theme_oklassedu');?></a></li>
            <li class="mymenu-path-local-evaluation"><a href="/local/evaluation/evaluation.php"><?php echo get_string('menu:evaluation','theme_oklassedu');?></a></li>
            <li class="mymenu-path-grade-report"><a href="/grade/report/overview/index.php"><?php echo get_string('menu:gradeview','theme_oklassedu');?></a></li>
            <li class="mymenu-page-user-edit"><a href="/user/edit.php?id=<?php echo $USER->id; ?>&course=1&returnto=profile"><?php echo get_string('menu:editprofile','theme_oklassedu');?></a></li>
            <li class="mymenu-page-user-course"><a href="/my/allcourse.php">강좌확인</a></li>
            <li class="mymenu-page-local-lmsdata-grade_tibero"><a href="/local/lmsdata/grade_tibero.php">최종성적확인</a></li>
            <?php if($menuauth == 0){ ?>
            <li class="mymenu-page-local-attendance-index"><a href="/local/attendance/user_attend.php">출석현황</a></li>
            <li class="mymenu-page-local-apply-assistant"><a href="/local/apply/assistant.php">조교/청강생 신청</a></li>
            <?php } ?>
            
        </ul>
    </li>
    <li>
        <a href="#">
            <h3 class="mymenu-okregular">
                <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                <?php echo get_string('menu:regular','theme_oklassedu');?>
                <span class="open"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
            </h3>
        </a>
        <ul>
            <li class="mymenu-page-local-okregular-my"><a href="/local/okregular/my.php"><?php echo get_string('menu:mycourse','theme_oklassedu');?></a></li>
            <!--<li class="mymenu-page-local-okregular-apply"><a href="/local/okregular/apply.php"><?php //echo get_string('menu:auditenrol','theme_oklassedu');?></a></li>-->
        </ul>
    </li>
    <li>
        <a href="#">
            <h3 class="mymenu-okirregular">
                <i class="fa fa-paste fa-2x" aria-hidden="true"></i>
                <?php echo get_string('menu:irregular','theme_oklassedu');?> 
                <span class="open"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
            </h3>
        </a>
        <ul>
            <li class="mymenu-page-local-okirregular-my"><a href="/local/okirregular/my.php"><?php echo get_string('menu:mycourse','theme_oklassedu');?></a></li>
            <li class="mymenu-page-local-okirregular-apply"><a href="/local/okirregular/apply.php"><?php echo get_string('menu:courseenrol','theme_oklassedu');?></a></li>
            <li class="mymenu-page-local-okirregular-complete"><a href="/local/okirregular/complete.php"><?php echo get_string('menu:completionview','theme_oklassedu');?></a></li>
        </ul>
    </li>
    <li><a href="#">
            <h3 class="mymenu-oklearning"> 
                <i class="fa fa-television fa-2x" aria-hidden="true"></i>
                <?php echo get_string('menu:elearning','theme_oklassedu');?>
                <span class="open"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
            </h3>
        </a>
        <ul>
            <li class="mymenu-page-local-oklearning-my"><a href="/local/oklearning/my.php"><?php echo get_string('menu:mycourse','theme_oklassedu');?></a></li>
            <li class="mymenu-page-local-oklearning-apply"><a href="/local/oklearning/apply.php"><?php echo get_string('menu:allcourse','theme_oklassedu');?></a></li>
            <li class="mymenu-page-local-oklearning-course_add"><a href="/local/oklearning/course_add.php"><?php echo get_string('menu:addcourse','theme_oklassedu');?></a></li>
        </ul>
    </li>
    <li class="link" onclick="window.open('<?php echo $CFG->wwwroot ?>/auth/mnet/jump.php?hostid=3','portfolio','')">
        <a href="#wrap">
            <h3 class="mymenu-course">
                <i class="fa fa-briefcase fa-2x" aria-hidden="true"></i>
                이포트폴리오
            </h3>
        </a>
    </li>
    <li>
        <a href="#">
            <h3 class="mymenu-jinoboard">
                <i class="fa fa-cog fa-2x" aria-hidden="true"></i>
                <?php echo get_string('menu:guide','theme_oklassedu');?> 
                <span class="open"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
            </h3>
        </a>
        <ul>
            <li class="mymenu-path-local-jinoboard-1"><a href="/local/jinoboard/list.php?id=1"><?php echo get_string('menu:notice','theme_oklassedu');?></a></li>
            <li class="mymenu-path-local-jinoboard-2"><a href="/local/jinoboard/list.php?id=2"><?php echo get_string('menu:qna','theme_oklassedu');?></a></li>
            <li class="mymenu-path-local-jinoboard-3"><a href="/local/jinoboard/list.php?id=3"><?php echo get_string('menu:faq','theme_oklassedu');?></a></li>
            <li class="mymenu-path-local-jinoboard-5"><a href="/local/jinoboard/list.php?id=5"><?php echo get_string('menu:reference','theme_oklassedu');?></a></li>
            <li><a href="<?php echo $CFG->wwwroot?>/snut_manual/index.htm"><?php echo get_string('menu:manual','theme_oklassedu');?></a></li>
        </ul>
    </li>
</ul>
