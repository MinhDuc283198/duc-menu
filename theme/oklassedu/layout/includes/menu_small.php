<?php 
global $DB;
if(has_capability('moodle/course:create', context_system::instance()) || $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id)) == 'pr') {
    $menuauth = 1;
}else{
    $menuauth = 0;
}
?>


<ul id="" class="smallMenu">
    <li  title="My Page"><a href="/" onclick="return false"><h3><p class="hidden-title">My Page</p><i class="fa fa-address-book-o fa-2x" aria-hidden="true"></i><span class="open"></span></h3></a>
        <ul> <span><?php echo get_string('menu:mypage','theme_oklassedu');?></span>
            <li><a href="/my/"><?php echo get_string('menu:dashboard','theme_oklassedu');?></a></li>
            <li><a href="/user/files.php"><?php echo get_string('menu:filemanage','theme_oklassedu');?></a></li>
            <?php if($menuauth == 1) {?>
            <li><a href="/local/courselist/course_manage.php"><?php echo get_string('menu:coursemanage','theme_oklassedu');?></a></li>
            <li><a href="/local/repository/"><?php echo get_string('menu:lcmsmanage','theme_oklassedu');?></a></li>
            <?php }?>
            <li><a href="/calendar/view.php?view=month"><?php echo get_string('menu:schedule','theme_oklassedu');?></a></li>
            <li><a href="/message/"><?php echo get_string('menu:message','theme_oklassedu');?></a></li>
            <li><a href="/local/courselist/courseoverview.php"><?php echo get_string('menu:coursenotification','theme_oklassedu');?></a></li>
            <li><a href="/local/board/"><?php echo get_string('menu:courseboard','theme_oklassedu');?></a></li>
            <li><a href="/local/evaluation/evaluation.php"><?php echo get_string('menu:evaluation','theme_oklassedu');?></a></li>
            <li><a href="/course/user.php?mode=grade&id=1&user=<?php echo $USER->id ?>"><?php echo get_string('menu:gradeview','theme_oklassedu');?></a></li>
            <li><a href="/user/profile.php?id=<?php echo $USER->id; ?>"><?php echo get_string('menu:editprofile','theme_oklassedu');?></a></li>
            <li><a href="/my/allcourse.php"><?php echo get_string('menu:courseenrol','theme_oklassedu');?></a></li>
            <li><a href="/my/assistant.php"><?php echo get_string('menu:assistant','theme_oklassedu');?></a></li>
            <li><a href="/local/attendance/user_attend.php">출석현황</a></li>
        </ul>
    </li>
    <li title="<?php echo get_string('menu:regular','theme_oklassedu');?>"><a href="/" onclick="return false"><h3><p class="hidden-title"><?php echo get_string('menu:regular','theme_oklassedu');?></p><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i><span class="open"></span></h3></a>
        <ul> <span><?php echo get_string('menu:regular','theme_oklassedu');?></span>
            <li><a href="/local/okregular/my.php"><?php echo get_string('menu:mycourse','theme_oklassedu');?></a></li>
            <!--<li><a href="/local/okregular/apply.php"><?php echo get_string('menu:assistant','theme_oklassedu');?></a></li>-->
        </ul>
    </li>
    <li title="<?php echo get_string('menu:irregular','theme_oklassedu');?>"><a href="/" onclick="return false"><h3><p class="hidden-title"><?php echo get_string('menu:irregular','theme_oklassedu');?></p><i class="fa fa-paste fa-2x" aria-hidden="true"></i><span class="open"></span></h3></a>
        <ul><span><?php echo get_string('menu:irregular','theme_oklassedu');?></span>
            <li><a href="/local/okirregular/my.php"><?php echo get_string('menu:mycourse','theme_oklassedu');?></a></li>
            <li><a href="/local/okirregular/apply.php"><?php echo get_string('menu:allcourse','theme_oklassedu');?></a></li>
            <!--<li><a href="/local/okirregular/apply.php"><?php echo get_string('menu:courseenrol','theme_oklassedu');?></a></li>-->
            <li><a href="/local/okirregular/complete.php"><?php echo get_string('menu:completionview','theme_oklassedu');?></a></li>
        </ul>
    </li>
    <li title="<?php echo get_string('menu:elearning','theme_oklassedu');?>"><a href="/" onclick="return false"><h3><p class="hidden-title"><?php echo get_string('menu:elearning','theme_oklassedu');?></p><i class="fa fa-television fa-2x" aria-hidden="true"></i><span class="open"></span></h3></a>
        <ul><span><?php echo get_string('menu:elearning','theme_oklassedu');?></span>
            <li><a href="/local/oklearning/my.php"><?php echo get_string('menu:mycourse','theme_oklassedu');?></a></li>
            <li><a href="/local/oklearning/apply.php"><?php echo get_string('menu:allcourse','theme_oklassedu');?></a></li>
            <li><a href="/local/oklearning/course_add.php"><?php echo get_string('menu:addcourse','theme_oklassedu');?></a></li>
        </ul>
    </li>
    <li class="link" onclick="location.href='<?php echo $CFG->wwwroot ?>/auth/mnet/jump.php?hostid=3'">
        <a href="#wrap">
            <h3 class="mymenu-course">
                <i class="fa fa-briefcase fa-2x" aria-hidden="true"></i>
            </h3>
        </a>
    </li>
    <li title="<?php echo get_string('menu:guide','theme_oklassedu');?>"><a href="/" onclick="return false"><h3><p class="hidden-title"><?php echo get_string('menu:guide','theme_oklassedu');?></p><i class="fa fa-cog fa-2x" aria-hidden="true"></i><span class="open"></h3></a>
        <ul><span><?php echo get_string('menu:guide','theme_oklassedu');?></span>
            <li><a href="/local/jinoboard/list.php?id=1"><?php echo get_string('menu:notice','theme_oklassedu');?></a></li>
            <li><a href="/local/jinoboard/list.php?id=2"><?php echo get_string('menu:qna','theme_oklassedu');?></a></li>
            <li><a href="/local/jinoboard/list.php?id=3"><?php echo get_string('menu:faq','theme_oklassedu');?></a></li>
            <li><a href="/local/jinoboard/list.php?id=5"><?php echo get_string('menu:reference','theme_oklassedu');?></a></li>
            <li><a  class="last_menu"  href="javascript:alert('<?php echo get_string('gettingready', 'local_jinoboard') ?>');"><?php echo get_string('menu:manual','theme_oklassedu');?></a></li>
        </ul>
</ul>

