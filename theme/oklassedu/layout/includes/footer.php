<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The maintenance layout.
 *
 * @package   theme_oklassedu
 * @copyright 2015 Nephzat Dev Team,nephzat.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
global $CFG, $VISANG;
$addr = theme_oklassedu_get_setting('address', 'format_text');
$number = theme_oklassedu_get_setting('phoneno', 'format_text');
$count = theme_oklassedu_get_setting('count', 'format_text');
$javascript_footer = get_visang_config('javascript_footer');
?>

<?php echo $javascript_footer; ?>

<!-- footer start -->

<div class="vis-footer">
    <div class="group">
        <div class="tp">
            <span class="ft-logo">
                <img src="/theme/oklassedu/pix/images/ft_logo.png" alt="master korean" />
            </span>
            <ul class="lnk">
<!--                <li><a href="#"><?php echo get_string('service', 'theme_oklassedu'); ?></a></li>-->
<!--                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1', '<?php echo get_string('terms', 'theme_oklassedu'); ?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms', 'theme_oklassedu'); ?></a></li>-->
                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=3')" ><?php echo get_string('c_TermsofUse', 'local_job'); ?></li>
                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2&category=3')" ><?php echo get_string('c_regulations', 'local_job'); ?></li>
                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=3')" ><?php echo get_string('c_Privacypolicy', 'local_job'); ?></li>
                <li><a href="/local/jinoboard/index.php?type=1"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></li>
                <!--<li><a href="<?php echo $CFG->job_url; ?>/local/job" target="_blank">mk jobs</a></li>-->
            </ul>

        </div>

        <div class="bt">
            <?php echo get_string('footer', 'theme_oklassedu'); ?>
            <!-- <br>Đây là trang web chạy thử nghiệm.  -->
        </div>

        <div class="f-r">
           
            <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank" class="ic-facebook"><img src="/theme/oklassedu/pix/images/icon_facebook_white.png" alt="sns"></a>
            <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank" class="ic-youtube"><img src="/theme/oklassedu/pix/images/icon_youtube_white.png" alt="youtube"></a>
             <a href="<?php echo $CFG->job_url; ?>/local/job" target="_blank" class="ic-jobs">mk jobs</a>
            <br/><a id="logoCCDV" href='http://online.gov.vn/Home/WebDetails/66177' target="_blank"><img alt='vcbfg' title='' src='/theme/oklassedu/pix/images/logoSaleNoti.png'/></a>
             
        </div>
    </div>
</div>
<!-- footer end -->

<!-- mobile footer start -->
<div class="mobi-footer">
    <div class="group">
        <div class="row">
            <span class="ft-logo">
                <img src="/theme/oklassedu/pix/images/ft_logo.png" alt="master korean" />
            </span>
        </div>
        <div class="row tp">
        	<ul class="lnk">
        <!--                <li><a href="#"><?php echo get_string('service', 'theme_oklassedu'); ?></a></li>-->
        <!--                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1', '<?php echo get_string('terms', 'theme_oklassedu'); ?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms', 'theme_oklassedu'); ?></a></li>-->
                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=3')" ><?php echo get_string('c_TermsofUse', 'local_job'); ?></li>
                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2&category=3')" ><?php echo get_string('c_regulations', 'local_job'); ?></li>
                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=3')" ><?php echo get_string('c_Privacypolicy', 'local_job'); ?></li>
                <li><a href="/local/jinoboard/index.php?type=1"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></li>
                <!--<li><a href="<?php echo $CFG->job_url; ?>/local/job" target="_blank">mk jobs</a></li>-->
            </ul>
        </div>
        <div class="row bt">
        	<?php echo get_string('footer', 'theme_oklassedu'); ?>
        </div>
        <div class="row">
        	<a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank" class="ic-facebook"><img src="/theme/oklassedu/pix/images/icon_facebook_white.png" alt="sns"></a>
            <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank" class="ic-youtube"><img src="/theme/oklassedu/pix/images/icon_youtube_white.png" alt="youtube"></a>
            <a href="<?php echo $CFG->job_url; ?>/local/job" target="_blank" class="ic-jobs">mk jobs</a>
            <br/><a id="logoCCDV" href='http://online.gov.vn/Home/WebDetails/66177' target="_blank"><img alt='fdgdf' title='' src='/theme/oklassedu/pix/images/logoSaleNoti.png'/></a>
            
        </div>
    </div>
</div>

<style>
@media screen and (min-width: 1025px){
    .mobi-footer {
        display: none;
    }
}
.mobi-footer{clear: both;background: #485cc7;padding: 20px 0 30px;}
.mobi-footer .tp{margin-bottom: 5px;overflow: hidden;}
.mobi-footer .tp .ft-logo{float: left;margin-right: 50px;}
.mobi-footer *{color: #fff;}
.mobi-footer .tp>.f-r>.ic-facebook{display: inline-block;font-size: 0;background: url("/theme/oklassedu/pix/images/icon_facebook.png") no-repeat center;width: 27px;height: 27px;margin-right: 7px;vertical-align: middle;}
.mobi-footer .tp>.f-r>.ic-youtube{display: inline-block;font-size: 0;background: url("/theme/oklassedu/pix/images/icon_youtube.png") no-repeat center;width: 27px;height: 27px;vertical-align: middle;}

.mobi-footer .group{position: relative; padding-left: 20px;}
.mobi-footer .group .f-r{position: absolute;right: 20px;top: 0;font-size: 0;}
.mobi-footer .group .f-r a.ic-facebook{margin-right: 5px;}
.mobi-footer a.ic-jobs{font-weight: bold;font-size: 0;background: url(/theme/oklassedu/pix/images/logo_btn_footer.png) no-repeat center;background-size: auto 15px;width: 110px;height: 28px;display: inline-block;border-radius: 5px;padding: 0px 13px;vertical-align: middle;margin-left: 10px;border: 1px solid #c8ceee;box-sizing: border-box;}
.mobi-footer a.ic-learn{font-weight: bold;font-size: 0;background: url(/theme/oklassedu/pix/images/logo_btn_footer02.png) no-repeat center;background-size: auto 13px;width: 85px;height: 28px;display: inline-block;border-radius: 5px;padding: 0px 13px;vertical-align: middle;margin-left: 10px;border: 1px solid #c8ceee;box-sizing: border-box;}

.mobi-footer .tp>.f-r>select{float: right;font-size: 14px;height: 28px;border: 0;width: 165px;box-sizing: border-box;padding: 4px;color: #666;line-height: 20px;background: #dadef4;color: #485cc7;margin-left: 15px;}

.mobi-footer .tp .lnk{font-size: 0;float: left;width: 100%;}
.mobi-footer .tp .lnk>li{font-size: 16px;margin: 0 20px 3px 0;display: inline-block;line-height: 35px;}
.mobi-footer .tp .lnk>li:last-child{margin-right: 0;}
.mobi-footer .bt{margin: 20px 0;}
.mobi-footer .bt p{font-size: 12px;line-height: 1.5;font-weight: 300;margin-bottom: 0;}
.mobi-footer .bt p.copy{font-size: 11px;margin-top: 13px;font-weight: 300;}

#logoCCDV {width: 100% !important;}
#logoCCDV img{width: 200px !important; margin-top: 20px;}
</style>
<!-- mobile footer end -->
<!-- mobile menu -->
<ul class="m-ft-link">
    <li><a href="/">비상 <?php echo get_string('korean', 'theme_oklassedu'); ?></a></li>
    <li><a href="<?php echo $CFG->job_url; ?>/local/job">비상 <?php echo get_string('job', 'theme_oklassedu'); ?></a></li>
</ul>