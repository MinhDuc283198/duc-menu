<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The maintenance layout.
 *
 * @package   theme_oklassedu
 * @copyright 2015 Nephzat Dev Team,nephzat.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$guesttxt = (isguestuser()) ? ' (' . get_string('guest') . ') ' : '';
$sitename = format_string(strpos($_SERVER['SERVER_NAME'], "job") === false? "Master Korean" :"Master Korean jobs", true, array('context' => context_course::instance(SITEID)));

global $DB;
$sql = 'SELECT * 
        FROM {user} mu
        JOIN {lmsdata_user} lu ON lu.userid = mu.id
        WHERE mu.id = :userid';
$userinfo = $DB->get_record_sql($sql, array('userid'=>$USER->id));

$sql = 'SELECT lca.*
        FROM {lmsdata_class} lc
        JOIN {lmsdata_course_applications} lca ON lca.courseid = lc.id AND lca.userid = :userid
        WHERE lc.courseid = :courseid and paymentstatus = :paymentstatus ';
$courseinfo = $DB->get_record_sql($sql, array('userid'=>$USER->id, 'courseid'=>$COURSE->id, 'paymentstatus'=>'complete'));

$sql = 'select u.*, u.id as userid from {role_assignments} ra 
        join {context} ctx on ctx.id = ra.contextid
        join {course} co on co.id = ctx.instanceid
        join {role} ro on ro.id = ra.roleid and ro.shortname = "editingteacher"
        join {user} u on u.id = ra.userid 
        where co.id = :courseid';
$profinfo = $DB->get_record_sql($sql, array('courseid'=>$COURSE->id));


if($bg = $DB->get_field('lmsdata_class', 'background', array('courseid' => $COURSE->id))){    
}else if($bg = $DB->get_field('lmsdata_course', 'background', array('courseid' => $COURSE->id))){    
} else {
    $bg = 1;
}
$textcolor = "blue";
if($bg == 2 || $bg == 3 || $bg == 4){
    $textcolor = "white";
}

//소속학교
$school = '비어있음';
if(!empty($courseinfo->school)) {
    $school = $courseinfo->school;
}
// 학년/반
$classgroup = '비어있음';
if(!empty($courseinfo->classlevel) && !empty($courseinfo->classgroup)) {
    $classgroup = $courseinfo->classlevel.'/'.$courseinfo->classgroup;
}
// 생년월일
$birthday = '비어있음';
if(!empty($userinfo->birthday)) {
    $birthday = substr($userinfo->birthday, 0, 4).'.'.substr($userinfo->birthday, 4, 2).'.'.substr($userinfo->birthday, 6, 2);
}
// 핸드폰번호
$phonenumber = '비어있음';
if(!empty($courseinfo)){
    $phone_first_number = substr($courseinfo->phone, 0, 3);
    $phone_middle_number = substr($courseinfo->phone, 3, 4);
    $phone_last_number = substr($courseinfo->phone, 7, 4);

    $phonenumber = $phone_first_number.'-'.$phone_middle_number.'-'.$phone_last_number;
}

?>
<style type="text/css">
    #course_bg{
        background: url('/local/okmanage/backgrounds/<?php echo $bg; ?>.jpg') no-repeat;
        background-size: cover;
    }
</style>
<div id="course_bg">
    <div class="user_img">
        <?php
        $usergroups = array('pr' => '교수', 'it' => '강사');
        if($COURSE->id != SITEID) {
            $user = $DB->get_record('user', array('id'=>$profinfo->userid));
        }else{
            $user = $DB->get_record('user', array('id'=>$USER->id));
        }
        
        //임시로 수정해놓음
        if(!empty($profinfo)){
            $userpicture = new user_picture($user);
            $userpicture->size = 1; // Size f1.
            $url = $userpicture->get_url($PAGE)->out(false);
        } else {
            $url = $CFG->wwwroot.'/theme/oklassedu/pix/images/nouser.jpg';
        }
        
        ?>
        <img src="<?php echo $url;?>" alt="user" title="<?php echo $user->lastname;?> 사진" class="pointer" onclick="window.open('/user/view.php?id=<?php echo $profinfo->userid;?>&course=<?php echo $COURSE->id;?>', '_blank')"/>
    </div>
    <div class="user_info l_content">
        <div class="l_content">
            <p class="name">
                과정 담당자 : <?php echo $user->lastname . ' '. $usergroups[$profinfo->usergroup];?>
            <p class="user_edit">
                <!--<input type="button" value="개인정보수정" name="btn01" class="btn gray" />--> 
                <img src="/theme/oklassedu/pix/images/message.png">&nbsp;<a onclick="window.open('/message/index.php?user1=<?php echo $USER->id;?>&user2=<?php echo $profinfo->userid;?>&viewing=course_<?php echo $COURSE->id;?>', '', 'width=900px, height=700px')">메시지</a>
            </p>
        </div>
        <div class="r_content">
            <div class="user_text">
                <p>
                    <span class="title">소속학교</span>
                    <strong class="<?php echo $textcolor; ?>"><?php echo$school;?></strong><br />
                    <span class="title">학년/반</span>
                    <strong class="<?php echo $textcolor; ?>"><?php echo $classgroup;?></strong>
                </p>
                <p>
                    <span class="title">생년월일</span>
                    <strong class="<?php echo $textcolor; ?>"><?php echo $birthday; ?></strong><br />
                    <span class="title">핸드폰번호</span>
                    <strong class="<?php echo $textcolor; ?>"><?php echo $phonenumber;?></strong>
                </p>
            </div>
            <p class="confirm"><span class="none">*소속학교 및 생년월일이 정확한지 확인 해주세요.</span><a href="<?php echo $CFG->wwwroot.'/local/mypage/application_edit.php?id='.$courseinfo->id;?>">수정하기</a></p>
        </div>
    </div>
    <div class="header_notice r_content">
        <?php
        $sql = 'SELECT jbc.* 
                FROM m_jinotechboard jb
                JOIN m_jinotechboard_contents jbc ON jbc.board = jb.id
                WHERE jb.course = :courseid AND jb.type = :type order by jbc.timemodified desc';
        $notices = $DB->get_records_sql($sql, array('courseid'=>$COURSE->id, 'type'=>1), 0, 3);
        ?>
        <h3>
            공지사항
            <?php
            $sections_sql = "select DISTINCT cs.id,cs.name,cs.section 
                            from {course_sections} cs
                            where cs.visible = 1 and cs.course = :id and cs.section = 0";
            $course_sections = $DB->get_record_sql($sections_sql, array('id' => $COURSE->id));
            $headsql = "select cm.id, cm.instance, mo.name, ctx.id as contextid from {course_modules} cm "
                    . "join {modules} mo on mo.id = cm.module and mo.name = 'jinotechboard' and mo.visible = 1 "
                    . "join {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel "
                    . "join {jinotechboard} jb on cm.instance = jb.id "
                    . "where cm.course = :course and cm.section = :section and jb.type = 1 ";
            $heads = $DB->get_record_sql($headsql, array('course' => $COURSE->id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_sections->id));
            if($heads->id){
                $boardurl = $CFG->wwwroot . '/mod/' . $heads->name . '/view.php?id=' . $heads->id ;
                if($COURSE->format == 'oklass_mooc'){
                    $boardurl = 'javascript:window.open(\''.$boardurl.'\',\'menuopen\',\'width=1200, height=800,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=no\');void(0);';
                }
            }
            if($boardurl){
            ?>
            <a href="<?php echo $boardurl;?>" class="more">더보기</a>
            <?php }?>
        </h3>
        <ul>
            <?php 
            if($notices){
                foreach($notices as $notice){
                        //콘텐츠 변수처리
                        require_once($CFG->dirroot . '/local/lmsdata/lib.php');
                        $vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
                        $vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');
                        $data['courseid'] = $COURSE->id;
                        $data['userid'] = $USER->id;
                        $data['title'] =$notice->title;
                        $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
                        $notice->title = $data['title'];
                        $contents_url = $CFG->wwwroot . '/mod/jinotechboard/content.php?boardform=2&contentId=' . $notice->id;
                        if($COURSE->format == 'oklass_mooc'){
                            $contents_url = 'javascript:window.open(\''.$contents_url.'\',\'menuopen\',\'width=1200, height=800,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=no\');void(0);';
                        }
                        
            ?>
            <li>
                <a href="<?php echo $contents_url; ?>"><?php echo $notice->title;?></a>
                <span class="date"><?php echo date('Y-m-d', $notice->timemodified);?></span>
            </li>
            <?php 
                }
            } else {
                echo '<li><a href="#">공지사항이 없습니다.</a></li>';
            }
            ?>
        </ul>
    </div>
</div>
