<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The maintenance layout.
 *
 * @package   theme_oklassedu
 * @copyright 2015 Nephzat Dev Team,nephzat.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$guesttxt = (isguestuser()) ? ' (' . get_string('guest') . ') ' : '';
$sitename = format_string(strpos($_SERVER['SERVER_NAME'], "job") === false? "Master Korean" :"Master Korean jobs", true, array('context' => context_course::instance(SITEID)));
global $DB;
?>
<div id="lnb">
    <?php
    $url = str_replace($CFG->wwwroot, '', $_SERVER["REQUEST_URI"]);
    $url = explode('?', $url)[0];
    $dir_u = explode('/', $url);

    $path_depth = count($dir_u);
    $d_name = null;
    if ($path_depth > 2) {
        $d_name = $dir_u[$path_depth - 2];
    }
    $f_name = $dir_u[$path_depth - 1];

    if (empty($d_name)) {
        $serchwhere = "/" . $f_name;
    } else {
        if ($d_name == 'jinoboard') {
            if ($f_name == 'list.php' || $f_name == 'mylist.php') {
                $serchwhere = "%" . $d_name . "/%id=" . $_REQUEST['id'];
            } else {
                $serchwhere = "%" . $d_name . "/%id=" . $_REQUEST['board'];
            }
        } else if ($f_name == 'course_list.php' || $f_name == 'course_list_detail.php' || $f_name == 'application.php') {
            $serchwhere = "%" . $d_name . "/%menu_gubun=" . $_REQUEST['menu_gubun'];
        } else if ($f_name == 'application_edit.php') {
            $serchwhere = "%" . $d_name . "/mycourse.php";
        } else if ($d_name == 'evaluation') {
            if ($f_name == 'survey.php') {
                $serchwhere = "%" . $d_name . "/index.php?type=2%";
            } else {
                $serchwhere = "%" . $d_name . "/" . $f_name . "%";
            }
        } else if ($f_name == 'searchresult.php') {
            $serchwhere = "%" . $d_name . "/reference.php";
        } else {
            $serchwhere = "%" . $d_name . "/" . $f_name . '%';
        }
    }
    if (!empty($serchwhere)) {
        $depth .= ' and depth = (select max(depth) from {main_menu} WHERE url LIKE :url2)';
    }

    $current_sql = 'SELECT * FROM {main_menu} WHERE url LIKE :url ' . $depth;
    $current_menu = $DB->get_record_sql($current_sql, array('url' => $serchwhere, 'url2' => $serchwhere));

  
    $user = $USER->id;
    $getusertype = $DB->get_record('lmsdata_user', array('userid' => $USER->id));
 
    $menulog = new stdClass(); 
    
    $menulog->userid = $user;
    $menulog->usertype = $getusertype->usertypecode;
    $menulog->timecreated = time();
    $menulog->menu = $current_menu->id;
    $menulog->menu_url = $current_menu->url;
//    $DB->insert_record('main_menu_log',$menulog);
    
    $myusergroup = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id));
    if (empty($myusergroup)) {
        $myusergroup = 'rs';
    }

    $main_menu = $DB->get_field('main_menu', 'id', array('id' => $current_menu->parent));
    $main_lang = $DB->get_field('main_menu_name', 'name', array('menuid' => $main_menu, 'lang' => current_language()));
    $parent_menu_on = 0;
    if ($current_menu->sub_parent != 0) {
        $current_sub_menu = $DB->get_field('main_menu', 'id', array('id' => $current_menu->sub_parent));
        if ($current_menu->isused != 1) {
            $parent_menu_on = 1;
        }
    } else {
        $current_sub_menu = $current_menu->id;
    }

    $sub_menus = $DB->get_records('main_menu', array('parent' => $main_menu, 'depth' => 2, 'isused' => 1), 'step ASC');
    ?>
    <h5><?php echo $main_lang; ?></h5>
    <ul>
        <?php
        foreach ($sub_menus as $sub_menu) {
            if (!is_siteadmin()) {
                $menu_apply = $DB->get_record('main_menu_apply', array('menuid' => $sub_menu->id, 'usergroup' => $myusergroup));
                if (empty($menu_apply)) {
                    continue;
                }
            }
            $on = ' ';
            if (($sub_menu->id == $current_menu->id || ($sub_menu->id == $current_sub_menu && $parent_menu_on == 1)) && $f_name != 'searchresult.php') {
                $on = 'class="on"';
            } else {
                $on = ' ';
            }
            $link = (preg_match('/http/i', $sub_menu->url)) ? $sub_menu->url : $CFG->wwwroot . $sub_menu->url;
            $sub_lang = $DB->get_field('main_menu_name', 'name', array('menuid' => $sub_menu->id, 'lang' => current_language()));
            if ($sub_menu->ispopup == 2) {
                $target = 'target="_blank"';
            }
            echo '<li ' . $on . '><a href="' . $link . '" ' . $target . '>' . $sub_lang . '</a></li>';
            if ($sub_menu->id == $current_sub_menu) {
                $third_menus = $DB->get_records('main_menu', array('parent' => $main_menu, 'depth' => 3, 'sub_parent' => $sub_menu->id, 'isused' => 1), 'step ASC');
                foreach ($third_menus as $third_menu) {
                    if (!is_siteadmin()) {
                        $menu_apply = $DB->get_record('main_menu_apply', array('menuid' => $third_menu->id, 'usergroup' => $myusergroup));
                        if (empty($menu_apply)) {
                            continue;
                        }
                    }
                    if ($current_menu->id == $third_menu->id) {
                        $class_on = 'on';
                    } else {
                        $class_on = ' ';
                    }
                    $link = (preg_match('/http/i', $third_menu->url)) ? $third_menu->url : $CFG->wwwroot . $third_menu->url;
                    $third_lang = $DB->get_field('main_menu_name', 'name', array('menuid' => $third_menu->id, 'lang' => current_language()));
                    $target = '';
                    if ($third_menu->ispopup == 2) {
                        $target = 'target="_blank"';
                    }

//                    echo '<li class="depth3 ' . $class_on . '"><a href="' . $link . '" ' . $target . '>' . $third_lang . '</a></li>';
                }
            }
        }
        ?>
    </ul>
    <div class="m_menu">&gt;</div>
</div>
