<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's oklassedu theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);

// Set default (LTR) layout mark-up for a three column page.
$regionmainbox = 'span10';
$regionmain = 'span10 pull-right';
$sidepre = 'span2 desktop-first-column';
$sidepost = 'span2 pull-right';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmainbox = 'span10 pull-right';
    $regionmain = 'span8';
    $sidepre = 'span2 pull-right';
    $sidepost = 'span3 desktop-first-column';
}


echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function(){
           /*메뉴*/
            $("#block-region-side-pre .block").click(function(){
                if(!$(this).hasClass("on")){
                    $(this).addClass("on");
                }else{
                    $(this).removeClass("on");
                }
            });
            
            /*모바일메뉴*/
            $("#mMenu").click(function(){
                $("body").append("<div id='opacity'></div>");
                $("#opacity").show();
                $("#block-region-side-pre").css("left","0px");
                
                $("#opacity").click(function(){
                    $(this).fadeOut();
                    $("#block-region-side-pre").css("left","-500px");
                });
            });
            
            
        });
    </script>
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<header role="banner" class="navbar navbar-fixed-top<?php echo $html->navbarclass ?> moodle-has-zindex">
    <nav role="navigation" class="navbar-inner">
        <div class="container-fluid">
            <div id="mMenu"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></div>
            <a class="brand" href="<?php echo $CFG->wwwroot; ?>">
                <?php echo theme_oklassedu_get_logo_url(); ?>
            </a>
            <?php echo $OUTPUT->navbar_button(); ?>
            <?php echo $OUTPUT->user_menu(); ?>
            <?php echo $OUTPUT->search_box(); ?>
            <div class="nav-collapse collapse">
                <?php echo $OUTPUT->custom_menu(); ?>
                <ul class="nav pull-right">
                    <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<div id="page" class="container-fluid">
    <?php // echo $OUTPUT->full_header(); ?>
    <div id="page-content" class="row-fluid">
        <div id="region-main-box" class="<?php echo $regionmainbox; ?>">
            <div class="row-fluid">
                <section id="region-main" class="<?php echo $regionmain; ?>">
                    <?php
                    echo $OUTPUT->course_content_header();
                    echo $OUTPUT->main_content();
                    echo $OUTPUT->course_content_footer();
                    ?>
                </section>
                <?php echo $OUTPUT->blocks('side-pre', $sidepre); ?>
            </div>
        </div>
        <?php echo $OUTPUT->blocks('side-post', $sidepost); ?>
    </div>

     <footer id="page-footer">
                <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
                <div class="span1">
                    <?php echo theme_oklassedu_get_logo_url('footer'); ?>
                </div>
                <div class="span7">
                    <?php
                    echo $html->footnote;
                    echo $OUTPUT->standard_footer_html();
                    ?>
                </div>
            </footer>

    <?php echo $OUTPUT->standard_end_of_body_html() ?>

</div>
</body>
</html>
 