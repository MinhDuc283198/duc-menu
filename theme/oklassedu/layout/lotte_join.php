<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * The one column layout.
 *
 * @package   theme_enlight
 * @copyright 2015 Nephzat Dev Team,nephzat.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);
if(isloggedin()){
    redirect($CFG->wwwroot);
}
if (strrchr($PAGE->url, 'errormag') != 'errormag') {
    $errormsg = urldecode(substr(strrchr($PAGE->url, 'errormag'), 9));
}
$rememberusername = get_moodle_cookie();
echo $OUTPUT->doctype();
$firstname = optional_param('firstname', '', PARAM_RAW); // 20.02.12 user firstname by googleouath2
$email = optional_param('id', '', PARAM_RAW); // 20.02.12 user email by googleouath2
$uid = optional_param('uid', '', PARAM_RAW);
$username = optional_param('username', '', PARAM_RAW);
$auth = optional_param('auth', '', PARAM_RAW);
$code = optional_param('code', '', PARAM_RAW);
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"  />
        <?php require_once(dirname(__FILE__) . '/homepage/css.php'); ?>
        <style>
            .lotte-header {
                border-bottom: 3px solid rgba(0, 181, 226, 0.95);
            }
            .login-title {
                margin-top: 50px;
                margin-bottom: 40px;
            }
            .login-title span {
                color: #ed1c24;
                font-weight: bold;
            }
            .login-title h2 {
                font-size: 36px;
                font-weight: normal;
            }
            .bg-form {
                background-image: url("/theme/oklassedu/pix/lotte/back-login.png");
                width: 100%;
                background-size: cover;
                background-repeat: no-repeat;
                border-radius: 6px;
                border: 1px solid  rgba(0, 181, 226, 0.95);
            }
            .lotte-header {
                padding-top: 16px;
                padding-bottom: 16px;
            }
            .lotte-header img:first-child {
                border-right: 2px solid #55555594;
                padding-right: 14px;
                margin-right: 14px
            }
            .login-form form {
                padding: 30px;
                padding-top: 50px;
                padding-bottom: 5px;
            }
            .btn-danger {
                color: #fff;
                background-color: #ed1c24;
                border-color: #ed1c24;
                width: 100%;
            }
            .visang-flex {
                display: flex;
                justify-content: space-between;
            }
            .text-lotte {
                color: #ed1c24;
                margin-left: 10px;
            }
            .register {
                margin-bottom: 0px;
                margin-top: 60px;
            }
            .register label {
                padding-bottom: 0px;
            }
            footer {
                border-top: 2px solid #55555594;
                margin-top: 60px;
                background: unset;
                color: #000;
                padding-top: 40px;
            }
            footer a {
                color: #000 !important;
            }
            .foot-icon {
                background: #485cc7;
                color: #fff;
            }
            .foot-icon i {
                color: #fff;
            }
            #logoCCDV img {
                background: unset;
            }
            #register, #login {
                cursor: pointer;
            }
            .dash {
                padding: 4px;
            }
        </style>

    </head>

    <body <?php echo $OUTPUT->body_attributes("bgblue custom"); ?>>

        <!-- header start -->
        <div class="container">
            <div class="lotte-header">
                <img src="/theme/oklassedu/pix/lotte/logo-visang.png" alt="">
                <img src="/theme/oklassedu/pix/lotte/logo-lotte.png" alt="">
            </div>
            <div class="login-title">
                <h2 class="text-center"><b>Master Korean Academy</b> for <span>Lotte Vietnam</span></h2>
            </div>
            <div class="login-form">
                <div class="row justify-content-md-center">
                    <div class="col-md-10 bg-form" id="login-form">
                        <div class="row">
                            <form class="col-md-7" action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post">
                                <div class="form-group row">
                                    <label for="inputEmail" class="col-sm-3 col-form-label">ID</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="username" class="form-control" id="inputEmail" placeholder="<?php echo get_string('username').'('.get_string('email').')'?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label"><?php echo get_string("joinpassword","local_signup")?></label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password" class="form-control" id="inputPassword" placeholder="<?php echo get_string("joinpassword","local_signup")?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="saveID" class="col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-sm-9 visang-flex">
                                        <div class="form-check">
                                            <input type="checkbox" name="rememberusername" value="1" <?php echo empty($rememberusername) ? '' : 'checked'; ?> class="form-check-input" id="saveID">
                                            <label class="form-check-label" for="saveID"><?php echo get_string('saveid', 'theme_oklassedu') ?></label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label"><a href="/local/signup/findaccount.php"><?php echo get_string("findmyaccount","local_signup")?></a></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-user"></i> <?php echo get_string('login', 'theme_oklassedu') ?></button>
                                    </div>
                                </div>
                                <div class="form-group row register">
                                    <label for="" class="col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-sm-9 text-center">
                                        <?php echo get_string('account_exist', 'theme_oklassedu') ?><a id="register" class="text-lotte"><?php echo get_string('signup', 'theme_oklassedu') ?></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-10 bg-form" id="register-form" style="display: none">
                        <div class="row justify-content-md-center">
                            <form class="col-md-9" action="<?php echo $CFG->httpswwwroot; ?>/local/signup/signup_summit.php" method="post">
                                <input type="hidden" name="username" value="" readonly>
                                <input type="hidden" name="code" value="" readonly>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="email" class="form-control" id="email" placeholder="<?php echo get_string('username').'('.get_string('email').')'?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="firstname" class="col-sm-3 col-form-label"><?php echo get_string("writename", "local_signup"); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="firstname" class="form-control" id="firstname" placeholder="<?php echo get_string("writename", "local_signup"); ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone1" class="col-sm-3 col-form-label"><?php echo get_string("phone", "local_signup"); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="phone1" class="form-control" id="phone1" placeholder="<?php echo get_string("phone_txt", "local_my"); ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="birth-of-date" class="col-sm-3 col-form-label"><?php echo get_string("birth", "local_signup"); ?></label>
                                    <div class="col-sm-9" style="display: flex">
                                        <select name="year" id="year" class="form-control">
                                            <?php
                                            foreach (range(date('Y'), 1960) as $year) {
                                                echo '<option value="' . $year . '">' . $year . '</option>';
                                            }
                                            ?>
                                        </select>
                                        <span class="dash"> - </span>
                                        <select name="month" id="month" class="form-control">
                                            <?php
                                            for ($m = 1; $m <= 12; $m++) {
                                                if (strlen($m) == 1)
                                                    $m = "0" . $m;
                                                if ($m == $mo) {
                                                    $date_month .= "<option value='$m' selected>$m</option>\n";
                                                } else {
                                                    $date_month .= "<option value='$m'>$m</option>\n";
                                                }
                                            }
                                            echo $date_month;
                                            ?>
                                        </select>
                                        <span class="dash">-</span>
                                        <select name="days" id="days" class="form-control">
                                            <?php
                                            for ($d = 1; $d <= 31; $d++) {
                                                if (strlen($d) == 1)
                                                    $d = "0" . $d;
                                                if ($d == $da) {
                                                    $date_day .= "<option value='$d' selected>$d</option>\n";
                                                } else {
                                                    $date_day .= "<option value='$d'>$d</option>\n";
                                                }
                                            }
                                            echo $date_day;
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="joinpassword" class="col-sm-3 col-form-label"><?php echo get_string("joinpassword","local_signup")?></label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password" class="form-control" id="password" placeholder="<?php echo get_string("joinpassword","local_signup")?>">
                                        <p class="text-info" style="font-size: 12px; margin-bottom: 0px"><?php echo get_string('passvalcheck','local_signup'); ?></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="saveID" class="col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-sm-9 visang-flex">
                                        <div class="form-check" style="padding-left: 0px">
                                            <a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=3', '서비스이용방침', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><span><?php echo get_string("c_agreeAllAll","local_job")?></span></a>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label"><a href="/local/signup/findaccount.php"><?php echo get_string("findmyaccount","local_signup")?></a></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-user"></i> <?php echo get_string("startingemail","local_signup")?></button>
                                    </div>
                                </div>
                                <div class="form-group row register">
                                    <label for="" class="col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-sm-9 text-center">
                                        <?php echo get_string('account_exist', 'theme_oklassedu') ?><a id="login" class="text-lotte"><?php echo get_string('login', 'theme_oklassedu') ?></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-12">
                        <a href="/">
                            <img class="lazyload img-fluid" loading="lazy" data-src="/theme/oklassedu/pix/lotte/mk-logo-footer-lotte-login.png" alt="master korean" />
                        </a>
                    </div>
                    <div class="col-lg-7 col-sm-12 mb-4">
                        <ul class="term">
                            <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=3')" ><?php echo get_string('c_TermsofUse', 'local_job'); ?></li>
                            <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2&category=3')" ><?php echo get_string('c_regulations', 'local_job'); ?></li>
                            <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=3')" ><?php echo get_string('c_Privacypolicy', 'local_job'); ?></li>
                            <li><a href="/local/jinoboard/index.php?type=1"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></li>
                        </ul>
                        <div class="bt">
                            <?php echo get_string('footer', 'theme_oklassedu'); ?>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-12">
                        <div class="f-r">
                            <a class="foot-icon" href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a class="foot-icon" href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                            <a class="foot-icon" href="<?php echo $CFG->job_url; ?>/local/job" target="_blank">
                                <img class="lazyload" loading="lazy" data-src="/theme/oklassedu/pix/images/logo_btn_footer.png" alt="">
                            </a>
                            <a id="logoCCDV" href='http://online.gov.vn/Home/WebDetails/66177' target="_blank">
                                <img alt='vcbfg' title='' class="lazyload" loading="lazy" data-src='/theme/oklassedu/pix/images/logoSaleNoti.png'/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer end -->
        </footer>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="/local/search/index.php" class="search-form container">
                        <div class="p-1 bg-light rounded rounded-pill shadow-sm mb-4 search-main">
                            <div class="input-group">
                                <input type="text" name="searchtext" placeholder="Nhập từ bạn cần tìm?"  class="form-control border-0 bg-light rounded-pill search-input">
                                <div class="input-group-append">
                                    <button id="button-addon1" type="submit" class="btn btn-link text-main search-button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <ul class="search-key">
                            <?php
                            foreach ($searchtext as $text) {
                                switch (current_language()) {
                                    case 'ko':
                                        $search_text_lang = $text->title;
                                        break;
                                    case 'en':
                                        $search_text_lang = $text->titleen;
                                        break;
                                    case 'vi':
                                        $search_text_lang = $text->titlevn;
                                        break;
                                }
                                ?>
                                <li><a href="/local/search/index.php?searchtext=<?php echo $search_text_lang; ?>">#<?php echo $search_text_lang; ?></a></li>
                            <?php } ?>
                        </ul>

                    </form>
                </div>
            </div>
        </div>

        <style>
            .search-key {
                padding: 0px;
                margin-bottom: 40px;
            }
            .search-key li {
                list-style: none;
                display: inline-block;
            }
            .search-key li a {
                color: #555;
                font-size: 17px;
            }
            #exampleModal .modal-dialog {
                width: 100vw;
                margin-top: 100px;
            }
            #exampleModal .modal-dialog form {
                margin-top: 130px;
                margin-left: auto;
                margin-right: auto;
            }
            #exampleModal .modal-content {
                border: 0px;
            }
            .fixed-top {
                z-index: 1176;
            }
            @media (min-width: 576px) {
                #exampleModal .modal-dialog {
                    max-width: 100vw !important;
                    margin: 1.75rem auto;
                }
            }
            @media (max-width: 400px) {
                .modal-dialog {
                    margin: 0px !important;
                }

                #exampleModal .modal-dialog form {
                    margin-top: 90px;
                }
                .form-control-lg {
                    padding: ;
                }
            }
            .search-main {
                border: 2px solid #485cc7;
            }
            .form-control:focus {
                box-shadow: none;
            }

            .search-button {
                font-size: 24px;
                padding: 0px;
                padding-right: 10px;
            }
            .form-control::placeholder {
                font-size: 0.95rem;
                color: #aaa;
                font-style: italic;
            }

        </style>
        <script src="/theme/oklassedu/javascript/jquery-3.6.0.min.js"></script>

        <script type="text/javascript">
            if ('loading' in HTMLImageElement.prototype) {const images = document.querySelectorAll('img[loading="lazy"]'); images.forEach(img => { img.src = img.dataset.src; }); } else { const script = document.createElement('script');script.src = '/theme/oklassedu/javascript/new-js/lazysizes.min.js';document.body.appendChild(script);}
        </script>
        <?php echo $OUTPUT->main_content() ?>

        <script>
            $('#register').click(function (){
                $('#login-form').hide()
                $('#register-form').show()
            })
            $('#login').click(function (){
                $('#register-form').hide()
                $('#login-form').show()
            })
        </script>
    </body>
</html>