<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The maintenance layout.
 *
 * @package   theme_oklassedu
 * @copyright 2015 Nephzat Dev Team,nephzat.com
 * 
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$surl = new moodle_url('/course/search.php');
$guesttxt = (isguestuser()) ? ' (' . get_string('guest') . ') ' : '';
$sitename = format_string(strpos($_SERVER['SERVER_NAME'], "job") === false? "Master Korean" :"Master Korean jobs", true, array('context' => context_course::instance(SITEID)));
require_once($CFG->dirroot . '/local/course/lib.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/theme/oklassedu/lib.php');
require_once($CFG->dirroot . '/local/course/course.php');

$langurl = oklassedu_change_lang($url, 'lang');
$langs = get_string_manager()->get_list_of_translations();

global $DB;
//$separator_self = explode(DIRECTORY_SEPARATOR, $_SERVER['REQUEST_URI']);
//$topmenu_self = array_pop($separator_self);
//$url_org = $_SERVER["REQUEST_URI"];
//$url = str_replace($CFG->wwwroot, '', $_SERVER["REQUEST_URI"]);
//$url_exp = explode('?', $url)[0];
//$url_exp1 = explode('?', $url)[1];
//$dir_u = explode('/', $url_exp);
//$path_depth = count($dir_u);
//$d_name = null;
//if ($path_depth > 2) {
//    $d_name = $dir_u[$path_depth - 2];
//}
//$f_name = $dir_u[$path_depth - 1];
//$getusertypes = $DB->get_record('lmsdata_user', array('userid' => $USER->id));

$res = optional_param('re', 0, PARAM_INT);
if ($res == 6) {
    echo '<script type="text/javascript">
                        alert("교육생은 접근할 수 없습니다");
                        location.href="' . $CFG->wwwroot . '";
                    </script>';
}
?>
<!-- header start -->
<script src="<?php echo $CFG->wwwroot ?>/local/mypage/notification.js"></script>
<div class="vis-hd clearfix">
    <?php if (!isloggedin()) { ?>
        <a href="<?php echo $CFG->wwwroot; ?>/login/index.php" class="ic-login"><?php echo get_string('login', 'theme_oklassedu'); ?></a>
    <?php } ?>

    <!-- 강의실 모바일 나가기 버튼 -->
    <a href="#" onclick="history.back()" class="m-crs-out">강의실 나가기</a>
    <div class="tp">
        <ul>
            <li>
                <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank" class="ic-facebook"><?php echo get_string('facebook', 'theme_oklassedu'); ?></a>
                <a href="https://www.youtube.com/channel/UCcU6RCjsoPqJ7HtiIpLmyyw" target="_blank" class="ic-youtube"><?php echo get_string('youtube', 'theme_oklassedu'); ?></a>
            </li>
            <li><a href="/local/jinoboard/index.php?type=1"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></li>
            <li><a href="<?php echo $CFG->job_url; ?>/local/job" class="ic-job" target="_blank"><?php echo get_string('job', 'theme_oklassedu'); ?></a></li>
        </ul>
        <p>
            <?php
            $langs = array('vn'=>'Vietnamese ‎(vn)', 'en'=>'English ‎(en)‎', 'ko'=>'한국어 ‎(ko)');
            foreach ($langs as $lang => $val) {
                $currlang = current_language();
                $on = '';
                if($lang == 'vn'){
                    $lang_txt = 'vi';
                }else{
                    $lang_txt = $lang;
                }

                if ($currlang == $lang_txt) {
                    $on = 'class="on"';
                }
                
            ?>
                <a href="<?php echo $langurl . "lang=$lang_txt"; ?>" <?php echo $on ?>><?php echo strtoupper($lang) ?></a>
                <!--<a href="<?php echo $langurl; ?>lang=en">EN</a>-->
            <?php }
            ?>
        </p>
    </div>
    <div class="wp">
        <div class="mx">
            <div class="logo">
                <a href="/">
                    <img src="/theme/oklassedu/pix/images/logo_beta.png" alt="visang 로고" />
                </a>
            </div>
            <div class="m-menu-area">
                <a href="#" class="m-close"> <?php echo get_string('close', 'local_mypage'); ?></a>
                <div class="m-tp">
                    <?php if (isloggedin()) { ?>
                        <div><?php echo $USER->firstname . $USER->lastname; ?><?php echo get_string('sir', 'theme_oklassedu'); ?></div>
                        <a href="/local/mypage/mycourse.php" class="btns br"><?php echo get_string('viewmycourse', 'theme_oklassedu'); ?></a>
                    <?php } else { ?>
                        <div><?php echo get_string('getlogin', 'theme_oklassedu'); ?></div>
                        <a href="<?php echo $CFG->wwwroot; ?>/login/index.php" class="btns br"><?php echo get_string('login', 'theme_oklassedu'); ?>/<?php echo get_string('signup', 'theme_oklassedu'); ?></a>
                    <?php } ?>
                </div>
                <div class="m-scrl">
                    <div>
                        <div class="custom-select fr sub-mn">
                            <strong><a href="<?php echo $CFG->wwwroot; ?>/local/course/main.php"><?php echo get_string('course', 'theme_oklassedu'); ?></a></strong>
                            <ul>
                                <?php
                                $ci = 0;
                                $cata = local_course_category_depth(1);
                                foreach ($cata as $ct) {
                                    $selected = '';
                                    if ($ci == 0) {
                                        $selected = 'class="selected"';
                                    }
                                    $cata1 = local_course_category_parent($ct->id);
                                    $url = '#';
                                    if (!$cata1) {
                                        $url = $CFG->wwwroot . '/local/course/list.php?category=' . $ct->id;
                                    }
                                ?>
                                    <li data-id="<?php echo $ct->id ?>" <?php echo $selected ?>>
                                        <a href="<?php echo $url ?>"><?php echo $ct->cname ?></a>
                                        <ul>
                                            <?php
                                            if ($cata1) {
                                                foreach ($cata1 as $ct1) {
                                            ?>
                                                    <li><a href="/local/course/list.php?id=<?php echo $ct1->id ?>"><?php echo $ct1->cname ?></a></li>
                                            <?php }
                                            } ?>
                                        </ul>
                                    </li>
                                <?php $ci++;
                                } ?>
                            </ul>
                        </div>
                        <div class="custom-select">
                            <strong><a href='<?php echo $CFG->wwwroot; ?>/local/book/list.php'><?php echo get_string('book', 'theme_oklassedu'); ?></a></strong>
                        </div>
                        <div class="custom-select no-sub">
                            <strong><a href="<?php echo $CFG->wwwroot; ?>/local/jinoboard/board.php?type=4"><?php echo get_string('board', 'theme_oklassedu'); ?></a></strong>
                        </div>
                        <div class="custom-select no-sub">
                            <strong><a href="<?php echo $CFG->wwwroot; ?>/local/jinoboard/community.php"><?php echo get_string('community', 'theme_oklassedu'); ?></a></strong>
                        </div>
                        <?php if (isloggedin()) { ?>
                            <div class="custom-select no-pc">
                                <strong><?php echo get_string('menu:mypage02', 'theme_oklassedu'); ?></strong>
                                <ul>
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mypage.php"><?php echo get_string('myinfo', 'theme_oklassedu'); ?></a></li>
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mycourse.php"><?php echo get_string('mycourse', 'theme_oklassedu'); ?></a></li>
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/notification.php"><?php echo get_string('notice2', 'theme_oklassedu'); ?></a></li>
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/jinoboard/index.php?type=2"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mypayment.php"><?php echo get_string('paymentlist', 'local_mypage'); ?></a></li>
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/my/info.php"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
                                </ul>
                            </div>
                            <!-- 모바일 footer 영역 -->
                        <?php } ?>
                        <div class="bt">
                            <a href="/local/job/" target="_blank" class="ic-job">구인구직</a>
                            <ul>
                                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1', '<?php echo get_string('terms','theme_oklassedu');?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms','theme_oklassedu');?></a></li>
                                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2', '<?php echo get_string('terms3','theme_oklassedu');?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms3','theme_oklassedu');?></a></li>
                                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0', '<?php echo get_string('terms2','theme_oklassedu');?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms2','theme_oklassedu');?></a></li>
                            </ul>
                            <p class="cp">Copyright © VISANG Education Group Vietnam Company</p>
                        </div><!-- 모바일 footer 영역 -->
                    </div>
                </div>
                <script type="text/javascript">
                    function confirmLogout() {
                        if( confirm("<?php echo get_string('logout:confirm','theme_oklassedu')?>") ) {
                            location.href = "<?php echo $CFG->wwwroot; ?>/login/logout.php?sesskey=<?php echo $USER->sesskey; ?>";
                        }
                    }
                </script>
                <div class="m-bt">
                    <p class="bg">
                        <span><a href="/local/jinoboard/index.php?type=1"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></span>
                        <span>
                            <?php if (isloggedin()) { ?>
                                <a href="javascript:void(0);" onclick="confirmLogout();"><?php echo get_string('logout', 'theme_oklassedu'); ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $CFG->wwwroot; ?>/login/index.php"><?php echo get_string('login', 'theme_oklassedu'); ?>/<?php echo get_string('signup', 'theme_oklassedu'); ?></a>
                            <?php } ?>
                        </span>
                    </p>

                    <p class="ic">
                        <span class="ic01">
                            <strong><a href="/local/jinoboard/write.php?type=2"><?php echo get_string('qna2','theme_oklassedu');?></a></strong>
                        </span>
                        <span class="ic02">
                            <strong><a href="https://www.messenger.com/t/111453797011701" target="_blank"><?php echo get_string('facebook', 'local_mypage'); ?><br /><?php echo get_string('counseling', 'local_mypage'); ?></a></strong>
                        </span>
                    </p>

                    <div>
                        <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank"><img src="/theme/oklassedu/pix/images/icon_sns.png" alt="sns" /></a>
                        <a href="https://www.youtube.com/channel/UCcU6RCjsoPqJ7HtiIpLmyyw" target="_blank"><img src="/theme/oklassedu/pix/images/icon_youtube.png" alt="youtube" /></a>
                        <select onchange="location.href = $('.mobile_langtab').val()" class="mobile_langtab">
                            <?php
                            foreach ($langs as $lang => $val) {
                                $currlang = current_language();
                                $on = '';
                                if ($currlang == $lang) {
                                    $on = 'selected="seleted"';
                                }
                                $lang_param = $lang;
                                if($lang == "vn"){
                                    $lang_param = "vi";
                                }
                            ?>
                                <option value="<?php echo $langurl . "lang=$lang_param"; ?>" <?php echo $on ?>><a href="#"><?php echo strtoupper($lang) ?></a></option>
                            <?php }
                            ?>
                        </select>
                    </div>
                </div>
            </div>



            <div class="f-r">
                <div class="search-evt">
                    <a href="#">
                        <img src="/theme/oklassedu/pix/images/icon_search.png" alt="<?php echo get_string('search', 'local_mypage'); ?>" />
                    </a>
                </div>
                <?php
                if (isloggedin()) {
                    $userpicture = new user_picture($USER);
                    $url = $userpicture->get_url($PAGE);
                    $userpicture = $url->out(false);
                    $userpicture = str_replace("/f2?rev", "/f1?rev", $userpicture);
                    $ma_cnt_sql = "select count(*) from {message} where useridto = :user_id and timeusertodeleted = 0";
                    $ma_cnt = $DB->count_records_sql($ma_cnt_sql, array("user_id" => $USER->id));

                    $notifications = local_mypage_get_message($USER->id);
                ?>
                    <div class="alram">
                        <a href="#">
                            <img src="/theme/oklassedu/pix/images/icon_bell.png" alt="<?php echo get_string('notice', 'theme_oklassedu'); ?>" />
                            <?php if ($ma_cnt > 0) { ?><span><?php echo ($ma_cnt < 100) ? $ma_cnt : '99+'; ?></span> <?php } ?>
                        </a>
                        <div class="alram-list">
                            <ul>
                                <?php
                                if (!empty($notifications)) {
                                    foreach ($notifications as $message) {
                                        //읽은메세지는 li class="read" 추가
                                        $time = notifi_time($message->timecreated);
                                ?>
                                        <li class="noti<?php echo $message->id; ?> <?php echo $message->timeread ? 'read' : ''; ?>" onclick="message_read(<?php echo $message->id; ?>,<?php echo $USER->id ?>)">
                                            <span class="close-al" onclick='message_delete(<?php echo $message->id; ?>,<?php echo $message->timeread; ?>,<?php echo $USER->id ?>, "<?php echo get_string('delnoti', 'local_mypage'); ?>")'><?php echo get_string('close', 'local_mypage'); ?></span>
                                            <div><?php echo $message->smallmessage; ?></div>
                                            <p onclick=""><?php
                                                            echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
                                                            ?></p>
                                            <span><?php echo $time; ?></span>
                                        </li>
                                    <?php
                                    }
                                } else {
                                    ?>
                                    <div class="no-data">
                                        <?php echo get_string('nodata:notifi', 'local_mypage'); ?>
                                    </div>
                                <?php } ?>
                                <?php if (!empty($message)) { ?><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/notification.php" class="all-btn">전체보기</a><?php } ?>
                        </div>
                    </div>
                    <div class="user-info">
                        <p>
                            <!--                        <img src="<?php echo $userpicture; ?>" alt="김비상" />-->
                            <span><?php echo $USER->firstname . $USER->lastname; ?><?php echo get_string('sir', 'theme_oklassedu'); ?></span>
                        </p>
                        <ul class="">
                            <?php
                            if (is_siteadmin()) {
                            ?> <li><a href="<?php echo $CFG->wwwroot; ?>/local/management" target="_blank"><?php echo get_string('gomanagement', 'theme_oklassedu'); ?></a></li> <?php
                                                                                                                                                                                }
                                                                                                                                                                                    ?>
                            <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mypage.php"><?php echo get_string('myinfo', 'theme_oklassedu'); ?></a></li>
                            <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mycourse.php<?php 
                            $userinfos = $DB->get_records_sql('SELECT * FROM {user} u JOIN {lmsdata_user} lu on u.id=lu.userid WHERE lu.userid = :userid',array('userid'=>$USER->id));
                                        foreach($userinfos as $userinfo){
                                            $prof = $userinfo->usergroup == 'pr' ? 'pr' : '';
                                        }
                                        if($prof=='pr'){ echo '?type=1';}
                                        ?>"><?php echo get_string('mycourse', 'theme_oklassedu'); ?></a></li>
                            <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/notification.php"><?php echo get_string('notice2', 'theme_oklassedu'); ?></a></li>
                            <li><a href="<?php echo $CFG->wwwroot; ?>/local/jinoboard/index.php?type=2"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
                            <li><a href="<?php echo $CFG->wwwroot; ?>/local/mypage/mypayment.php"><?php echo get_string('paymentlist', 'local_mypage'); ?></a></li>
                            <li><a href="<?php echo $CFG->wwwroot; ?>/local/my/info.php"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
                            <li><a href="javascript:void(0);" onclick="confirmLogout();"><?php echo get_string('logout', 'theme_oklassedu'); ?></a></li>
                        </ul>
                    </div>
                <?php
                } else {
                ?>
                    <div class="login-area">
                        <a href="<?php echo $CFG->wwwroot; ?>/login/index.php" class="btns br"><?php echo get_string('signup', 'theme_oklassedu'); ?>/<?php echo get_string('login', 'theme_oklassedu'); ?></a>
                    </div>
                <?php
                }
                ?>
                <span class="m-menu"></span>
            </div>
        </div>
    </div>
    <!-- 검색 영역 -->
    <div class="hd-search-area">
        <a href="#" class="close-search"> <?php echo get_string('close', 'local_mypage'); ?></a>
        <form class="hd-fr" action="<?php echo $CFG->wwwroot ?>/local/search/index.php" method="get">
            <input type="text" name="searchtext" placeholder="<?php echo get_string('search_txt', 'theme_oklassedu') ?>" />
            <input type="submit" value="<?php echo get_string('search', 'theme_oklassedu') ?>" />
        </form>
        <ul class="hash-area">
            <?php
            //추천검색어
            $sql = "SELECT ms.id,ms.title,ms.titleen,ms.titlevn, ms.isused, ms.timecreated, ms.divide FROM {lmsdata_manage_search} ms WHERE ms.isused = 1 and divide = 0 ORDER BY id DESC";
            $searchtext = $DB->get_records_sql($sql, array());
            foreach ($searchtext as $text) {
            ?> <li data-title="<?php echo $text->title ?>"><a href="#"><?php echo $text->title; ?></a></li>
            <?php
            }
            ?>
        </ul>
    </div>
    <!-- 검색 영역 -->
</div><!-- header end -->
<div class="m-prev">
    <a href="javascript:history.back();" class="ic-arrow">prev</a>
    <span><?php echo $PAGE->heading; ?></span>
</div>