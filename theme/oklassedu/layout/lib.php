<?php

function theme_oklassv2_get_coursemenu_items($courseid) {
    global $CFG, $PAGE;

    $items = array();

    $modinfo = get_fast_modinfo($courseid);

    // 학습하기 메뉴
    $iteminfo = new stdClass();
    if (!$PAGE->user_is_editing()) {
        $iteminfo->name = '학습하기';
        $iteminfo->url = $CFG->wwwroot . '/course/view.php?id=' . $courseid;
        $iteminfo->on = true;
        $items[] = $iteminfo;
    }

    foreach ($modinfo->sections[0] as $modnumber) {
        $mod = $modinfo->cms[$modnumber];

        if ($mod->deletioninprogress) {
            continue;
        }

        $iteminfo = new stdClass();
        $iteminfo->name = $mod->name;
        $iteminfo->url = $mod->url->out(true);
        $iteminfo->on = false;
        if ($PAGE->user_is_editing()) {
            $type = 'show';
            if ($mod->visible) {
                $type = 'hide';
            }

            $url = new moodle_url('/course/mod.php', array('sesskey' => sesskey(), 'sr' => 0, $type => $mod->id));

            $action = new stdClass();
            $action->class = $type;
            $action->text = get_string($type);
            $action->url = $url->out(true);

            $iteminfo->action = $action;
            $iteminfo->visible = $mod->visible;
        }

        if ($PAGE->cm->id == $mod->id) {
            $iteminfo->on = true;
            if (!$PAGE->user_is_editing()) { // 학습하기 메뉴를 추가한 경우에만...
                $items[0]->on = false;
            }
        }

        if ($mod->visible || $PAGE->user_is_editing()) {
            $items[] = $iteminfo;
        }
    }

    return $items;
}

function get_courselayout_block() {
    global $CFG, $DB, $COURSE;
    $scriptname = $_SERVER['REQUEST_URI'];
    $lmsclassid = $DB->get_field("lmsdata_class", 'id', array('courseid' => $COURSE->id));
    $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);
    $sub_html .= html_writer::start_tag('li', array('class' => strpos($scriptname, "/course/view.php") !== false ? 'on' : ''));
    $sub_html .= html_writer::start_tag('a', array('href' => new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $COURSE->id))));
    $sub_html .= get_string('courselist', 'theme_oklassedu');
    $sub_html .= html_writer::end_tag('a');
    $sub_html .= html_writer::end_tag('li');
    //여기에 section0에 대한 학습활동 가져오기 시작    
    $section = course_get_format($COURSE)->get_section(0);
    $sequences = explode(',', $section->sequence);

    foreach ($sequences as $moduleid) {

        $sql = "SELECT cm.id,cm.instance,mo.name
                  FROM {course_modules} cm 
                  JOIN {modules} mo ON mo.id = cm.module
                  WHERE cm.id=:moduleid and cm.visible=1 and mo.visible=1 and mo.name!=:lcmsprogress";

        if ($module = $DB->get_record_sql($sql, array('moduleid' => $moduleid, 'lcmsprogress' => 'lcmsprogress'))) {
            if ($mod = $DB->get_record($module->name, array('id' => $module->instance))) {
                $url = new moodle_url($CFG->wwwroot . '/mod/' . $module->name . '/view.php', array('id' => $module->id, 'popup' => 1));
                if ($COURSE->format == 'oklass_mooc') {
                    $url = 'javascript:window.open(\'' . $url . '\',\'menuopen\',\'width=1200, height=800,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=no\');void(0);';
                }
                $sub_html .= html_writer::start_tag('li', array('class' => (strpos($scriptname, "/mod/$module->name") !== false && (strpos($scriptname, "id=$module->id") !== false || strpos($scriptname, "b=$module->instance") !== false )) ? 'on' : ''));
                $sub_html .= html_writer::start_tag('a', array('href' => $url));
                $sub_html .= get_string($mod->name, 'theme_oklassedu');
                $sub_html .= html_writer::end_tag('a');
                $sub_html .= html_writer::end_tag('li');
            }
        }
    }

//        $sub_html .= html_writer::start_tag('li');
//        $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/course/detail.php?id=" . $lmsclassid));
//        $sub_html .= get_string('courseintro', 'theme_oklassedu');
//        $sub_html .= html_writer::end_tag('a');
//        $sub_html .= html_writer::end_tag('li');

    $sub_html .= html_writer::start_tag('li');
    $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/course/coursereview.php?id=" . $COURSE->id));
    $sub_html .= get_string('coursereviews', 'local_course');
    $sub_html .= html_writer::end_tag('a');
    $sub_html .= html_writer::end_tag('li');

    //section0 학습활동 가져오기 끝
    /*
      $sub_html .= html_writer::start_tag('li');
      $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/jinoboard/list.php?id=7", 'target'=>'_blank'));
      $sub_html .= get_string('courseqna', 'block_oklass_course_menu');
      $sub_html .= html_writer::end_tag('a');
      $sub_html .= html_writer::end_tag('li');
     */

//        if (has_capability('moodle/course:manageactivities', $context)){
//            $sub_html .= html_writer::start_tag('li');
//            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/course/report/statistics/progress.php?id=" . $COURSE->id));
//            $sub_html .= get_string('progressstats', 'block_oklass_course_menu');
//            $sub_html .= html_writer::end_tag('a');
//            $sub_html .= html_writer::end_tag('li');
//        }
//        
//        if (has_capability('moodle/course:manageactivities', $context) && $COURSE->format != 'oklass_mooc') {
//            $sub_html .= html_writer::start_tag('li');
//            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/course/report/statistics/modules.php?id=" . $COURSE->id));
//            $sub_html .= get_string('learningstats', 'block_oklass_course_menu');
//            $sub_html .= html_writer::end_tag('a');
//            $sub_html .= html_writer::end_tag('li');
//        }/enrol/users.php?id=202
    //참여자 목록은 과정에 관리 권한이 있는 사용자만 볼 수 있게 처리
//        if (has_capability('moodle/course:manageactivities', $context)) {
//            $sub_html .= html_writer::start_tag('li');
//            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/okcourse/userindex.php?id=" . $COURSE->id));
//            $sub_html .= get_string('participantlist', 'block_oklass_course_menu');
//            $sub_html .= html_writer::end_tag('a');
//            $sub_html .= html_writer::end_tag('li');
//        }
    //admin 등록된 사용자 목록
//        if (has_capability('moodle/course:manageactivities', $context)) {
//            $sub_html .= html_writer::start_tag('li');
//            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/enrol/users.php?id=" . $COURSE->id));
//            $sub_html .= get_string('participantlist', 'block_oklass_course_menu');
//            $sub_html .= html_writer::end_tag('a');
//            $sub_html .= html_writer::end_tag('li');
//        }
//        
//        if (has_capability('moodle/course:manageactivities', $context)) {
//            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/grade/report/yieldgrader/index.php?id=" . $COURSE->id));
//            $sub_html .= html_writer::start_tag('li');
//            $sub_html .= get_string('gradebook', 'block_oklass_course_menu');
//            $sub_html .= html_writer::end_tag('a');
//            $sub_html .= html_writer::end_tag('li');
//        }
//        $sub_html .= html_writer::end_tag('ul');
//        $sub_html .= html_writer::end_tag('div');

    return $sub_html;
}

function get_course_adminayout_block() {
    global $CFG, $DB, $COURSE;

    $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);

    if (has_capability('moodle/course:manageactivities', $context)) {
        $sub_html .= html_writer::start_tag('li');
        $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/course/completion.php?id=" . $COURSE->id));
        $sub_html .= get_string('coursecompletesetting', 'theme_oklassedu');
        $sub_html .= html_writer::end_tag('a');
        $sub_html .= html_writer::end_tag('li');

        $sub_html .= html_writer::start_tag('li');
        $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/question/edit.php?courseid=" . $COURSE->id));
        $sub_html .= get_string('quizbank', 'theme_oklassedu');
        $sub_html .= html_writer::end_tag('a');
        $sub_html .= html_writer::end_tag('li');

//        $sub_html .= html_writer::start_tag('li');
//        $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/lmsdata/learningstatus.php?id=" . $COURSE->id));
//        $sub_html .= get_string('learningstatus', 'theme_oklassedu');
//        $sub_html .= html_writer::end_tag('a');
//        $sub_html .= html_writer::end_tag('li');

        $sub_html .= html_writer::start_tag('li');
        $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/lmsdata/learningstatus.php?page=1&perpage=10&id=" . $COURSE->id));
        $sub_html .= get_string('participantlist', 'theme_oklassedu');
        $sub_html .= html_writer::end_tag('a');
        $sub_html .= html_writer::end_tag('li');
    }



    return $sub_html;
}

function get_course_title($useatag=1) {
    global $DB, $COURSE, $PAGE;
    $course_info_sql = " SELECT * "
            . "FROM {course} co "
            . "left JOIN {lmsdata_class} lc on lc.courseid = co.id "
            . "WHERE co.id = :courseid ";
    $courseinfo = $DB->get_record_sql($course_info_sql, array('courseid' => $COURSE->id));

    $category = $DB->get_record('course_categories', array('visible' => 1, 'id' => $courseinfo->category), '*');
    $category_path = array_filter(explode('/', $category->path));
    $i = 0;
    $category_text = '';
    $scriptname = $_SERVER['REQUEST_URI'];
    if (!strpos($scriptname, "course/view.php")  || $PAGE->user_is_editing()) {
        if ($category_path) {
            foreach ($category_path as $cp) {
                $category_name = $DB->get_record('course_categories', array('visible' => 1, 'id' => $cp), 'name');
                switch (current_language()) {
                    case 'ko' :
                        $category_text = $category_name->name;
                        break;
                    case 'en' :
                        $category_text = $category_name->en_name;
                        break;
                    case 'vi' :
                        $category_text = $category_name->vi_name;
                        break;
                }
                //$category_text .= $category_name->name;
                $i++;
                if ($i != count($category_path)) {
                    $category_text .= ' > ';
                }
            }
            if($useatag){
                $targethref['href'] = "/course/view.php?id=" . $COURSE->id;
            }else{
                $targethref = null;
            }
            $o .= html_writer::start_div("editing-tp");
            switch (current_language()) {
                case 'ko' :
                    $coruse_name_lang = $courseinfo->title;
                    break;
                case 'en' :
                    $coruse_name_lang = $courseinfo->en_title;
                    break;
                case 'vi' :
                    $coruse_name_lang = $courseinfo->vi_title;
                    break;
            }
            $o .= html_writer::tag('a', html_writer::tag('span', $category_text) . ($courseinfo->title == null ? " Master Course -" . $courseinfo->fullname : $coruse_name_lang), $targethref);
            $o .= html_writer::end_div();//end editing-tp
        }
    }
    return $o;
}
