<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once(dirname(__FILE__) . '/homepage/repo.php'); ?>

    <?php echo get_visang_config('javascript_header'); ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="preload"  href="/theme/oklassedu/style/icon/css/all.min.css" as="style" onload="this.rel='stylesheet'"/>

    <title>Master Korean</title>
    <meta name="description" content="Master Korean! Master Your Job! Cung cấp các khóa học tiếng Hàn online đa dạng: Tiếng Hàn cho người mới bắt đầu, sơ cấp, trung cấp, luyện thi Topik và giao tiếp doanh nghiệp">
    <meta property="og:image" content="<?php echo $CFG->wwwroot ?>/theme/oklassedu/pix/banner/banner-2.webp" />
    <?php require_once(dirname(__FILE__) . '/homepage/css.php'); ?>
    <style>
        @media only screen and (max-width: 480px){
            .carousel-item img {
                height: 25vh;
            }
            .carousel-item {
                height: 25vh;
            }
        }


        .bg-course-recommned {
            background: url("/theme/oklassedu/pix/banner/m-2.webp");
        }
        .bg-course-employer {
            background: url("/theme/oklassedu/pix/banner/m-3.webp");
        }

        .bg-course-popular {
            background: url("/theme/oklassedu/pix/banner/m-1.webp");
        }

        .job-logo {
            background:url("/theme/oklassedu/pix/images/hd_m_logo_job.png") no-repeat center;
        }
        /* use display:inline-flex to prevent whitespace issues. alternatively, you can put all the children of .rating-group on a single line */
        #full-stars-example-two  .rating-group {
            display: inline-flex;
        }

        /* make hover effect work properly in IE */
        #full-stars-example-two .rating__icon {
            pointer-events: none;
        }

        /* hide radio inputs */
        #full-stars-example-two .rating__input {
            position: absolute !important;
            left: -9999px !important;
        }

        /* hide 'none' input from screenreaders */
        #full-stars-example-two .rating__input--none {
            display: none
        }

        /* set icon padding and size */
        #full-stars-example-two .rating__label {
            cursor: pointer;
            padding: 0 0.1em;
            font-size: 2rem;
        }

        /* set default star color */
        #full-stars-example-two .rating__icon--star {
            color: orange;
        }

        /* if any input is checked, make its following siblings grey */
        #full-stars-example-two .rating__input:checked ~ .rating__label .rating__icon--star {
            color: #ddd;
        }

        /* make all stars orange on rating group hover */
        #full-stars-example-two .rating-group:hover .rating__label .rating__icon--star {
            color: orange;
        }

        /* make hovered input's following siblings grey on hover */
        #full-stars-example-two #full-stars-example-two .rating__input:hover ~ .rating__label .rating__icon--star {
            color: #ddd;
        }

        .discount {
            background: #ed1c24;
            padding-left: 6px;
            padding-right: 6px;
            color: #fff;
        }

    </style>
</head>
<body>
    <?php echo get_visang_config('javascript_body'); ?>

    <!--  menu  -->
    <?php require_once(dirname(__FILE__) . '/homepage/header.php'); ?>
    <!--    end menu-->

    <!--  banner  -->
    <?php require_once(dirname(__FILE__) . '/homepage/banner.php'); ?>
    <!--    end banner-->

    <!--  popular  -->
    <?php require_once(dirname(__FILE__) . '/homepage/course-popular.php'); ?>
    <!--    end popular-->

    <!--  recommend  -->
    <?php require_once(dirname(__FILE__) . '/homepage/course-recommend.php'); ?>
    <!--    end recommend-->

    <!--  employer  -->
    <?php require_once(dirname(__FILE__) . '/homepage/employer.php'); ?>
    <!--    end employer-->

    <!--  feedback  -->
    <?php require_once(dirname(__FILE__) . '/homepage/feedback.php'); ?>
    <!--    end feedback-->

    <!--  feedback  -->
    <?php require_once(dirname(__FILE__) . '/homepage/footer.php'); ?>
    <!--    end feedback-->

<!--    <script src="/theme/oklassedu/javascript/jquery-3.6.0.min.js" ></script>-->
    <?php require_once(dirname(__FILE__) . '/homepage/js.php'); ?>

    <a href="https://zalo.me/2282433133049043336" style="position: fixed; right: 15px; bottom: 15px">
        <img style="width: 70px" class="lazyload"  loading="lazy" data-src="/theme/oklassedu/pix/home/mobile/icon/zalo-chat-icon.png" alt="">
    </a>

    <script type="text/javascript">
        if ('loading' in HTMLImageElement.prototype) {const images = document.querySelectorAll('img[loading="lazy"]'); images.forEach(img => { img.src = img.dataset.src; }); } else { const script = document.createElement('script');script.src = '/theme/oklassedu/javascript/new-js/lazysizes.min.js';document.body.appendChild(script);}
    </script>
    <div class="main-content-none">
        <?php echo $OUTPUT->main_content(); ?>
    </div>



    <?php echo get_visang_config('javascript_footer_mobile'); ?>
    <script type="text/javascript">
        $(window).on("load", function() {
            $("#myModal").modal("show");
        });

        $('.star-event span').click(function() {
            let scope = $(this).attr('data-scope');
            $('#scope').val(scope);
        })

    </script>
</body>
</html>
