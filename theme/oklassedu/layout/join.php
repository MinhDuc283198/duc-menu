<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The one column layout.
 *
 * @package   theme_enlight
 * @copyright 2015 Nephzat Dev Team,nephzat.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);
if(isloggedin()){
    redirect($CFG->wwwroot);
}
if (strrchr($PAGE->url, 'errormag') != 'errormag') {
    $errormsg = urldecode(substr(strrchr($PAGE->url, 'errormag'), 9));
}
$rememberusername = get_moodle_cookie();
echo $OUTPUT->doctype();
$firstname = optional_param('firstname', '', PARAM_RAW); // 20.02.12 user firstname by googleouath2
$email = optional_param('id', '', PARAM_RAW); // 20.02.12 user email by googleouath2
$uid = optional_param('uid', '', PARAM_RAW);
$username = optional_param('username', '', PARAM_RAW);
$auth = optional_param('auth', '', PARAM_RAW);
$code = optional_param('code', '', PARAM_RAW);
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet"/>
    </head>

    <body <?php echo $OUTPUT->body_attributes("bgblue custom"); ?>>

        <?php echo $OUTPUT->standard_top_of_body_html() ?>
        <!-- header start -->
        <?php require_once(dirname(__FILE__) . '/includes/header.php'); ?>

        <div class="login-bx">
            <ul class="login-tab tab-event">
                <li data-target=".box01" id="logintab" <?php if($auth==''){ echo 'class="on"';} ?>><a href="#"><?php echo get_string('login', 'theme_oklassedu') ?></a></li>
                <li data-target=".box02" id="signuptab" <?php if($auth!=''){ echo 'class="on"';} ?>><a href="#"><?php echo get_string('signup', 'theme_oklassedu') ?></a></li>
            </ul>
            <div class="target-area">
                <div class="box01 <?php if($auth==''){ echo 'on';} ?>">
                    <form class="email-bx login"  action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post" id="login1" >
                        <p>
                            <input type="text" class="w100" id="username" name="username" value="<?php echo $rememberusername; ?>" title="<?php echo get_string("username").'('.get_string('email').')'?> " placeholder="<?php echo get_string('username').'('.get_string('email').')'?>" />
                        </p>
                        <p>
                            <input type="password" id="password" name="password" title="<?php echo get_string("joinpassword","local_signup")?>" placeholder="<?php echo get_string("joinpassword","local_signup")?>" />
                        </p>
                        <!-- 아이디저장 영역 -->
                        <?php if (isset($CFG->rememberusername) and $CFG->rememberusername == 2) { ?>
                            <label class="custom-ck"><input type="checkbox" name="rememberusername" value="1" <?php echo empty($rememberusername) ? '' : 'checked'; ?>/><span></span><strong><?php echo get_string('saveid', 'theme_oklassedu') ?></strong></label>
                            <div class="btns-area text-center">
                                <input type="submit" id="loginbtn1" class="btns point big" value="<?php echo get_string('login', 'theme_oklassedu') ?>" />
                            </div>
                        <?php } ?>

                        <p class="find-id"><a href="/local/signup/findaccount.php" ><?php echo get_string("findmyaccount","local_signup")?></a></p>
                    </form>
                    <div class="sns-area">
                            <a href="/local/signup/oauth2login.php?providername=facebook" class="ic-fb">
                                <span class="ic"><img src="/theme/oklassedu/pix/images//icon_facebook_big.png"  alt="facebook" /></span>
                                <span><?php echo get_string('facebooklogin', 'theme_oklassedu') ?></span>
                            </a>
                            <a href="/local/signup/oauth2login.php?providername=google" class="ic-gl">
                                <span class="ic"><img src="/theme/oklassedu/pix/images//icon_google.png" alt="google" /></span>
                                <span><?php echo get_string('googlelogin', 'theme_oklassedu') ?></span>
                            </a>
<!--                        <a href="#" class="ic-zl">
                            <span class="ic"><img src="/theme/oklassedu/pix/images//icon_zalo.png" alt="zalo"/></span>
                            <span><?php echo get_string('zalologin', 'theme_oklassedu') ?></span>
                        </a>-->
                    </div>
                </div>

                <div class="box02 <?php if($auth!=''){ echo 'on';} ?>">
                </div>
                <?php echo $OUTPUT->main_content() ?>
            </div>
            <?php echo $OUTPUT->standard_end_of_body_html(); ?>
    </body>
</html>
<script>
    $(document).ready(function () {
        var passp = "";
        var passi = "";
        var errormsg = "<?php echo $errormsg ?>";
        if (errormsg != "") {
            alert(errormsg);
        }
        $(".login-tab li").click(function () {
            if (!$(this).hasClass("on")) {
                change_tab(1);
            }
        });
        $(document).on("click", ".termcheck", function () {
                change_tab(2);
        });
        $(document).on("keyup", "#joinpassword", function () {
            check_val('joinpassword');
            match_password();
        });
        $(document).on("keyup", "#joinpassword2", function () {
            match_password();
        });
        $(document).on("click", ".joinemail", function () {
            if ($("#emailconfirm").val() != 1) {
                alert("<?php echo get_string("checkemail", "local_signup") ?>");
                return false;
            }
            if ($("#joinpasswordconfirm").val() != 1) {
                alert("<?php echo get_string("checkjoinpassword", "local_signup") ?>");
                return false;
            }
            if ($("#joinpassword2").val() == $("#joinpassword").val()) {
                change_tab(3);
            } else {
                alert("<?php echo get_string("notmatchedpass", "local_signup") ?>");
                return false;
            }
        });
        $(document).on("change", "#year, #month", function () {
            get_day();
        });
        var auth = '<?php echo $auth;?>';
        if(auth!=''){
             var data = {"type": 1, "check": 3};
               data["postn"] = '<?php echo $firstname; ?>';
               data["poste"] = '<?php echo $email; ?>';
               data["username"] = '<?php echo $username; ?>';
               data["uid"] = '<?php echo $uid; ?>';
               data["code"] = '<?php echo $code; ?>';
                data["auth"] = auth;
                $.ajax({
                    url: "/local/signup/logintab_form.php",
                    type: 'post',
                    data: data,
                    success: function (result) {
                        $(".box02").empty().append(result);
                    }
                });
        }
    });

    function change_tab(check) {
        $(".login-tab li").each(function (i, v) {
            if ($(v).hasClass("on")) {
                get_loginform($(v).attr("id"), check);
            }
            ;
        });
    }
    function match_password() {
        if ($("#joinpassword2").val() == $("#joinpassword").val()) {
            $(".joinpassword2warning").text("<?php echo get_string("matchedpass", "local_signup") ?>");
        } else {
            $(".joinpassword2warning").text("<?php echo get_string("notmatchedpass", "local_signup") ?>");
        }
    }
    function get_loginform(type, check) {
        var data = {"type": type, "check": check};
        if (check == 3) {
            data["postp"] = $("#joinpassword").val();
            data["poste"] = $("#email").val();
        }
        console.log(data);
        $.ajax({
            url: "/local/signup/logintab_form.php",
            type: 'post',
            data: data,
            success: function (result) {
                $(".box02").empty().append(result);
            }
        });
    }

    function get_day() {
        var Year = document.getElementById('year').value;
        var Month = document.getElementById('month').value;
        var day = new Date(new Date(Year, Month, 1) - 86400000).getDate();
        var text = '';
        for (var i = 1; i <= day; i++) {
            var i_length = i.toString().length;
            if(i_length == 1){
                i = '0'+i.toString();
            }
            text += "<option value='" + i + "' "+selected+">" + i + "</option>\n"
        }
        $("#days").empty().append(text);

    }

    function check_val(type) {
        var target = $("#" + type);
        var regExpemail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (type == 'email' && !regExpemail.test($("#email").val())) {
            var errortext = "<?php echo get_string("emailrule", "local_signup", array('rule' => "visang@visang.com")) ?>";
            alert(errortext);
            $("." + type + "warning").text(errortext);
            $("#" + type + "confirm").val(0);
        } else {
            $.ajax({
                url: "/local/signup/valueconfirm.php",
                type: 'post',
                dataType: "json",
                data: {type: type, value: target.val()},
                success: function (result) {
                    if (result.status) {
                        $("#" + type + "confirm").val(result.status);
                        $("." + type + "warning").text(result.text);
                    } else {
                        $("#" + type + "confirm").val(result.status);
                        $("." + type + "warning").text(result.text);
                    }
                }
            });
        }
    }
</script>