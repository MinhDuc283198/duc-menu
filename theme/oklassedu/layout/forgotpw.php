<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The one column layout.
 *
 * @package   theme_enlight
 * @copyright 2015 Nephzat Dev Team,nephzat.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);

if (isloggedin() && !isguestuser()) {
    redirect($CFG->wwwroot);
}
if (empty($CFG->authloginviaemail)) {
    $strusername = get_string('username');
} else {
    $strusername = get_string('usernameemail');
}
echo $OUTPUT->doctype();

global $SESSION;
$googleloginerror = $SESSION->googleloginerrormsg;
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body <?php echo $OUTPUT->body_attributes(); ?>>

        <?php echo $OUTPUT->standard_top_of_body_html() ?>

        
        <?php
        /* google oauth2 로그인 코드 - 2018.01.25 */
        $providername = 'google';
        require_once($CFG->dirroot . '/auth/googleoauth2/vendor/autoload.php');
        require_once $CFG->dirroot . '/auth/googleoauth2/lib.php';
        require_once($CFG->dirroot . '/auth/googleoauth2/classes/provider/' . $providername . '.php');
        $providerclassname = 'provideroauth2' . $providername;
        $provider = new $providerclassname();
        $authurl = $provider->getAuthorizationUrl();
        set_state_token($providername, $provider->state);
        ?>
        <!-- wrap start -->
        <div id="wrap" class="login white">
            <div class="center_cont">
                <img src="/theme/oklassedu/pix/images/logo.png" alt="JEL재능그룹" class="logo" />
               <?php echo $OUTPUT->main_content() ?>
            </div>

            <footer class="login_footer">
                <ul>
                    <li>
                        <a href="#"><?php get_string('privacypolicy','theme_layout'); ?></a>
                    </li> 
                </ul>
                <p>Copyright © JEI. All rights reserved.</p>
            </footer>

        </div>
        <!-- wrap end -->
        <?php echo $OUTPUT->standard_end_of_body_html() ?>
    </body>
</html>
