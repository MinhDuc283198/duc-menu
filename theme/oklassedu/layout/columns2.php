<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The two column layout.
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);

// Set default (LTR) layout mark-up for a two column page (side-pre-only).
$regionmain = 'span10 pull-right';
$sidepre = 'span2 desktop-first-column';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmain = 'span10';
    $sidepre = 'span2 pull-right';
}

echo $OUTPUT->doctype();
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body id="<?php p($PAGE->bodyid); ?>" class="<?php p($PAGE->bodyclasses); ?>">

        <?php echo $OUTPUT->standard_top_of_body_html() ?>

        <header role="banner" class="navbar navbar-fixed-top<?php echo $html->navbarclass ?> moodle-has-zindex">
            <nav role="navigation" class="navbar-inner">
                <div class="container-fluid">
                    <a class="brand" href="<?php echo $CFG->wwwroot; ?>">
                        <?php echo theme_oklassedu_get_logo_url(); ?>
                    </a>
                    <?php echo $OUTPUT->navbar_button(); ?>
                    <?php echo $OUTPUT->user_menu(); ?>
                    <?php require_once(dirname(__FILE__) . '/includes/custom_user_menu.php'); ?>
                    <?php echo $OUTPUT->search_box(); ?>
                    <div class="nav-collapse collapse">
                        <?php echo $OUTPUT->custom_menu(); ?> 
                        <ul class="nav pull-right">
                            <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div id="page" class="container-fluid">
            <div id="page-content" class="row-fluid">
                <section id="region-main" class="<?php echo $regionmain; ?>">
                    <!-- 상단 네비-->
                    <?php if ($PAGE->heading || (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar())) { ?>
                        <div id="page-header">
                            <?php if (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar()) { ?>
                                <div class="navbar clearfix">
                                    <div class="breadcrumb"><?php echo $OUTPUT->navbar(); ?></div>
                                    <div class="navbutton"> <?php echo $PAGE->button; ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($PAGE->heading) { ?>
                                <h2 class="headermain"><?php echo $PAGE->heading ?></h2>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <!-- 상단네비 끝-->
                    <div class="region-main-content-area">
                        <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                        ?>
                    </div>
                </section>
                <?php
                if (!$_GET['test2']) {
                    require_once(dirname(__FILE__) . '/includes/menu_standard.php');
                } else {
                    require_once(dirname(__FILE__) . '/includes/menu_standard2.php');
                }
                ?>
                <?php require_once(dirname(__FILE__) . '/includes/menu_small.php'); ?>
            </div>

            <div class="footer100">
                <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>
            </div>

            <?php echo $OUTPUT->standard_end_of_body_html() ?>

        </div>
    </body>
</html>
