<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   theme_oklassedu
 * @copyright 2016 Jinotech
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
global $PAGE;
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);

echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <link href="/theme/oklassedu/style/font-awesome.min.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet"/>
        <style>
            body{padding-top:0 !important;}
            @page {size: A4;margin: 0;}
            @media print {html, body {width: 210mm;height: 297mm;}
        </style>
    </head>

    <body id="<?php p($PAGE->bodyid); ?>" class="<?php p($PAGE->bodyclasses); ?>">
        <?php echo $OUTPUT->standard_top_of_body_html() ?>

        <?php
        echo $OUTPUT->course_content_header();
        echo $OUTPUT->main_content();
        echo $OUTPUT->course_content_footer();
        ?>

        <?php echo $OUTPUT->standard_end_of_body_html(); ?> 
    </body>
</html> 
