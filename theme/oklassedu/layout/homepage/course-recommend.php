<div class="bg-course-recommned bg-course">
    <h2 class="text-center text-white text-uppercase"><?php echo get_string('package', 'theme_oklassedu') ?></h2>
    <div class="course-popular container">
        <button class="course-popular-slider-button  course-recommend-slider-pre">
            <i class="fa fa-caret-left"></i>
        </button>
        <button class="course-popular-slider-button course-recommend-slider-next">
            <i class="fa fa-caret-right"></i>
        </button>
        <div class="slider slider-for course-recommned-slider">
            <?php
                foreach ($upPackage as $course) {
                    $list = local_course_class_id2($course->id);
                    $context = context_course::instance($course->lcocourseid);
                    $path = local_course_get_imgpath($context->id);
            ?>
            <div class="main-course">
                <div class="m-3 shadow bg-white border-radius">
                    <img class="img-fluid lazyload" loading="lazy" data-src="<?php echo $path; ?>" alt="">
                    <div class="main-course-text">
                        <h2> <a class="block-ellipsis-title ellipsis" href="/local/course/detail.php?id=<?php echo $course->id; ?>"> <?php echo $course->title; ?> </a></h2>
                        <p class="ellipsis-1 ellipsis">
                            <b><?php echo get_string('gangsa', 'local_course') ?>: </b> <?php echo $list->teacher_list; ?>
                        </p>
                        <p class="">
                            <b><?php echo get_string('courseperiod2', 'local_course')  ?>: </b> <?php echo $list->courseperiod_txt ?>
                        </p>
                        <p class="duration">
                            <span class="ellipsis ellipsis-1">
                                <b><?php echo get_string('unit_number', 'local_course') ?>: </b>
                                <?php echo $list->lecturecnt ?><?php echo get_string('class', 'local_course') ?>
                            </span>
                        </p>
                        <div class="price row">
                            <div class="col-md-6 ">
                                <?php if ($course->discount != 0) { ?>
                                <span class="older">
                                    <del><?php echo number_format($course->lcoprice + $course->ltprice) ?> vnđ</del>
                                </span>
                                    <br>
                                <?php } ?>
                                <span class="last"><b> <?php echo number_format($course->price); ?> vnđ</b></span>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-main float-right" href="/local/course/detail.php?id=<?php echo $course->id; ?>">Học ngay <i class="fa fa-long-arrow-alt-right"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
