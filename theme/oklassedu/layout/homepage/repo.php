<?php

require_once($CFG->dirroot . '/local/course/lib.php');
require_once($CFG->dirroot . '/local/job/lib/common_functions.php');

global $DB;

$langurl = oklassedu_change_lang($url, 'lang');

$currlang = current_language();


$login = isloggedin();

$isPc = isPC();


if (optional_param('re', 0, PARAM_INT) == 6) {
    echo '<script type="text/javascript">
            alert("교육생은 접근할 수 없습니다");
            location.href="' . $CFG->wwwroot . '";
        </script>';
}

//sql
 $sql = "SELECT ad.employer_id as val, em.company_name
            FROM {vi_admin_employers} ad
            JOIN {vi_employers} em ON em.id = ad.employer_id 
            WHERE ad.user_id = :id";
$comid = $DB->get_record_sql($sql, array('id' => $USER->id));
// search
$sql = "SELECT ms.id,ms.title,ms.titleen,ms.titlevn, ms.isused, ms.timecreated, ms.divide FROM {lmsdata_manage_search} ms WHERE ms.isused = 1 and divide = 0 ORDER BY id DESC";
$searchtext = $DB->get_records_sql($sql, array());

// course
$limit = $isPc ? 6 : 3;

//$upCourses = local_course_recommend($login, $limit);

$upPackage = local_course_class_recommend_list(3, $limit);

// jobs
$jobs = mkjobs_get_recommend_jobs(isloggedin(), $limit);


if (isloggedin()) {

    $currentTime = time();

    $sql = "select m_enrol.courseid from m_user_enrolments inner join  m_enrol on m_user_enrolments.enrolid = m_enrol.id 
        where m_user_enrolments.userid = $USER->id 
        and m_user_enrolments.timeend <= 604800 + $currentTime
";


  $courseEnrolls = $DB->get_records_sql($sql);

    if (! empty($courseEnrolls)) {
        foreach ($courseEnrolls as $courseEnroll) {

            $sql = "select * from m_lmsdata_course_like where moodlecourseid = $courseEnroll->courseid and userid = $USER->id";

            if (! $DB->get_records_sql($sql)) {
                $sql = "select id from m_lmsdata_class where courseid = $courseEnroll->courseid ";

                if ($class = $DB->get_record_sql($sql)) {

                    if ($courseReview = local_course_class_id2($class->id)) {

                        require('commons/review-popup.php');
                        break;
                    }
                }

            }
        }
    }

}
