<div class="feedback-banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-6" style="padding-right: 0px;">
                <img class="w-100 img-feedback lazyload"  loading="lazy" data-src="/theme/oklassedu/pix/banner/feedback-left.webp" alt="">
            </div>
            <div class="col-sm-6" style="padding-left: 0px">
                <img class="w-100 img-feedback lazyload"  loading="lazy" data-src="/theme/oklassedu/pix/banner/feedback-right.webp" alt="">
            </div>
        </div>
    </div>

</div>
<div class="container feedback-form">
    <div class="row">
        <div class="col-md-5">
            <img class="img-fluid img-feedback lazyload" loading="lazy" data-src="/theme/oklassedu/pix/banner/banner-4.webp" alt="">
        </div>
        <div class="form col-md-7">
            <div class="col">
                <h2 class="text-main h2-title"><?php echo get_string('feedback', 'theme_oklassedu') ?></h2>
            </div>
            <form method="post" id="mail-form" name="myform" accept-charset=”utf-8”  >
                <div class="col">
                    <input required class="form-control" id="name" name="subject" type="text" required placeholder="<?php echo get_string('fbfullname', 'theme_oklassedu') ?>(*)"> 
                </div>
                <div class="col">
                    <input class="form-control" id="email" name="emainsend" type="email" required placeholder="<?php echo get_string('fbyouremail', 'theme_oklassedu') ?>(*)" >           
                </div>
                <div class="col">
                    <input class="form-control" id="phone"  name="phone" type="phone" required placeholder="<?php echo get_string('fbphonenumber', 'theme_oklassedu') ?>(*)">
                </div>
                <div class="col">
                    <textarea class="form-control"  name="message" ></textarea>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-main btn-feedback"><?php echo get_string('fbbutton', 'theme_oklassedu') ?></button>
                </div>
            </form>
            <div class="col">
                <p class="feedback-note">
                <?php echo get_string('fbnotice', 'theme_oklassedu') ?>
                </p>
            </div>
        </div>
    </div>
</div>
<div id="overlay">
  <div class="cv-spinner">
    <span class="spinner"></span>
  </div>
</div>


<style>
.btn-feedback{
  display:block;
}
#overlay{ 
  position: fixed;
  top: 0;
  z-index: 1000;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;  
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% { 
    transform: rotate(360deg); 
  }
}
.is-hide{
  display:none;
}
</style>
