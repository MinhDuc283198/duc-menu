<div class="bg-course-popular bg-course">
    <h2 class="text-center text-white text-uppercase">
        <?php echo get_string('recommendedcourse' . $login, 'theme_oklassedu') ?>
    </h2>
    <div class="course-popular container">
        <button class="course-popular-slider-button  course-popular-slider-pre">
            <i class="fa fa-caret-left"></i>
        </button>
        <button class="course-popular-slider-button course-popular-slider-next">
            <i class="fa fa-caret-right"></i>
        </button>
        <div class="slider slider-for course-popular-slider">
            <!--topik-->
            <div class="main-course">
                <div class="m-3 shadow bg-white">
                    <div class="main-course-text">
                        <h2><a class="block-ellipsis-title ellipsis" href="/local/course/detail.php?id=91">
                                <?php echo get_string('topik_course', 'theme_oklassedu') ?></a>
                        </h2>
                        <p class="block-ellipsis-description ellipsis mb-2">
                            <a href="/local/course/detail.php?id=91">
                                <?php echo get_string('topik_course', 'theme_oklassedu') ?>
                            </a>
                        </p>
                        <div class="main-course-cate mb-2">
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                        </div>
                        <?php if(partnerDiscount()) { ?>
                            <p class="d-inline-block"><del><?php echo number_format(1050000); ?><span> VND</span></del></p>
                            <p class="d-inline-block discount"><?php echo partnerDiscount(); ?><span>% for Lotte</span></p>
                            <p><?php echo number_format(1050000 - 1050000 * partnerDiscount() / 100)?><span> VND</span></p>
                        <?php } else { ?>
                            <p><?php echo number_format(1050000); ?><span> VND</span></p>
                        <?php } ?>
                    </div>
                    <a href="/local/course/detail.php?id=91">
                        <img class="img-fluid lazyload" loading="lazy" data-src="/pluginfile.php/2969/local_courselist/thumbnailimg/110/Thumbnail%20TOPIK.webp" alt="">
                    </a>
                </div>
            </div>
                <!--end topik-->
                <!--business-->
            <div class="main-course">
                <div class="m-3 shadow bg-white">
                    <div class="main-course-text">
                        <h2><a class="block-ellipsis-title ellipsis" href="/local/course/detail.php?id=92">
                                <?php echo get_string('business_course', 'theme_oklassedu') ?></a>
                        </h2>
                        <p class="block-ellipsis-description ellipsis mb-2">
                            <a href="/local/course/detail.php?id=92">
                                <?php echo get_string('business_course', 'theme_oklassedu') ?>
                            </a>
                        </p>
                        <div class="main-course-cate mb-2">
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                        </div>
                        <?php if(partnerDiscount()) { ?>
                            <p class="d-inline-block"><del><?php echo number_format(560000); ?><span> VND</span></del></p>
                            <p class="d-inline-block discount"><?php echo partnerDiscount(); ?><span>% for Lotte</span></p>
                            <p><?php echo number_format(560000 - 560000 * partnerDiscount() / 100)?><span> VND</span></p>
                        <?php } else { ?>
                            <p><?php echo number_format(560000); ?><span> VND</span></p>
                        <?php } ?>
                    </div>
                    <a href="/local/course/detail.php?id=92">
                        <img class="img-fluid lazyload" loading="lazy" data-src="/pluginfile.php/2925/local_courselist/thumbnailimg/109/Tieng%20Han%20Business%20co%20Ok.webp" alt="">
                    </a>
                </div>
            </div>
                <!--end business-->
                <!--beginner 1-->
            <div class="main-course">
                <div class="m-3 shadow bg-white">
                    <div class="main-course-text">
                        <h2><a class="block-ellipsis-title ellipsis" href="/local/course/detail.php?id=87">
                                <?php echo get_string('beginner1_course', 'theme_oklassedu') ?></a>
                        </h2>
                        <p class="block-ellipsis-description ellipsis mb-2">
                            <a href="/local/course/detail.php?id=87">
                                <?php echo get_string('beginner1_course', 'theme_oklassedu') ?>
                            </a>
                        </p>
                        <div class="main-course-cate mb-2">
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                        </div>
                        <?php if(partnerDiscount()) { ?>
                            <p class="d-inline-block"><del><?php echo number_format(840000); ?><span> VND</span></del></p>
                            <p class="d-inline-block discount"><?php echo partnerDiscount(); ?><span>% for Lotte</span></p>
                            <p><?php echo number_format(840000 - 840000 * partnerDiscount() / 100)?><span> VND</span></p>
                        <?php } else { ?>
                            <p><?php echo number_format(840000); ?><span> VND</span></p>
                        <?php } ?>
                    </div>
                    <a href="/local/course/detail.php?id=87">
                        <img class="img-fluid lazyload" loading="lazy" data-src="/pluginfile.php/2702/local_courselist/thumbnailimg/107/Thumbnail%20Ti%E1%BA%BFng%20H%C3%A0n%20S%C6%A1%20c%E1%BA%A5p%201.webp" alt="">
                    </a>
                </div>
            </div>
                <!--end beginner 1-->
                <!--beginner 2-->
            <div class="main-course">
                <div class="m-3 shadow bg-white">
                    <div class="main-course-text">
                        <h2><a class="block-ellipsis-title ellipsis" href="/local/course/detail.php?id=88">
                                <?php echo get_string('beginner2_course', 'theme_oklassedu') ?></a>
                        </h2>
                        <p class="block-ellipsis-description ellipsis mb-2">
                            <a href="/local/course/detail.php?id=88">
                                <?php echo get_string('beginner2_course', 'theme_oklassedu') ?>
                            </a>
                        </p>
                        <div class="main-course-cate mb-2">
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                        </div>
                        <?php if(partnerDiscount()) { ?>
                            <p class="d-inline-block"><del><?php echo number_format(840000); ?><span> VND</span></del></p>
                            <p class="d-inline-block discount"><?php echo partnerDiscount(); ?><span>% for Lotte</span></p>
                            <p><?php echo number_format(840000 - 840000 * partnerDiscount() / 100)?><span> VND</span></p>
                        <?php } else { ?>
                            <p><?php echo number_format(840000); ?><span> VND</span></p>
                        <?php } ?>
                    </div>
                    <a href="/local/course/detail.php?id=88">
                        <img class="img-fluid lazyload" loading="lazy" data-src="/pluginfile.php/2817/local_courselist/thumbnailimg/108/Thumbnail%20Ti%E1%BA%BFng%20H%C3%A0n%20S%C6%A1%20c%E1%BA%A5p%202.webp" alt="">
                    </a>
                </div>
            </div>
                <!--end beginner 2-->
                <!--Intermediate  1-->
            <div class="main-course">
                <div class="m-3 shadow bg-white">
                    <div class="main-course-text">
                        <h2><a class="block-ellipsis-title ellipsis" href="/local/course/detail.php?id=89">
                                <?php echo get_string('intermediate1_course', 'theme_oklassedu') ?></a>
                        </h2>
                        <p class="block-ellipsis-description ellipsis mb-2">
                            <a href="/local/course/detail.php?id=89">
                                <?php echo get_string('intermediate1_course', 'theme_oklassedu') ?>
                            </a>
                        </p>
                        <div class="main-course-cate mb-2">
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                        </div>
                        <?php if(partnerDiscount()) { ?>
                            <p class="d-inline-block"><del><?php echo number_format(840000); ?><span> VND</span></del></p>
                            <p class="d-inline-block discount"><?php echo partnerDiscount(); ?><span>% for Lotte</span></p>
                            <p><?php echo number_format(840000 - 840000 * partnerDiscount() / 100)?><span> VND</span></p>
                        <?php } else { ?>
                            <p><?php echo number_format(840000); ?><span> VND</span></p>
                        <?php } ?>
                    </div>
                    <a href="/local/course/detail.php?id=89">
                        <img class="img-fluid lazyload" loading="lazy" data-src="/pluginfile.php/10421/local_courselist/thumbnailimg/122/Thumbnail%20Trung%20cap%201.webp" alt="">
                    </a>
                </div>
            </div>
                <!--end Intermediate 1-->
                <!--Intermediate  2-->
            <div class="main-course">
                <div class="m-3 shadow bg-white">
                    <div class="main-course-text">
                        <h2><a class="block-ellipsis-title ellipsis" href="/local/course/detail.php?id=90">
                                <?php echo get_string('intermediate2_course', 'theme_oklassedu') ?></a>
                        </h2>
                        <p class="block-ellipsis-description ellipsis mb-2">
                            <a href="/local/course/detail.php?id=90">
                                <?php echo get_string('intermediate2_course', 'theme_oklassedu') ?>
                            </a>
                        </p>
                        <div class="main-course-cate mb-2">
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                            <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                        </div>
                        <?php if(partnerDiscount()) { ?>
                            <p class="d-inline-block"><del><?php echo number_format(840000); ?><span> VND</span></del></p>
                            <p class="d-inline-block discount"><?php echo partnerDiscount(); ?><span>% for Lotte</span></p>
                            <p><?php echo number_format(840000 - 840000 * partnerDiscount() / 100)?><span> VND</span></p>
                        <?php } else { ?>
                            <p><?php echo number_format(840000); ?><span> VND</span></p>
                        <?php } ?>
                    </div>
                    <a href="/local/course/detail.php?id=90">
                        <img class="img-fluid lazyload" loading="lazy" data-src="/pluginfile.php/10533/local_courselist/thumbnailimg/123/Thumbnail%20Trung%20cap%202.webp" alt="">
                    </a>
                </div>
            </div>
            <!--end Intermediate 2-->
        </div>
    </div>
</div>
