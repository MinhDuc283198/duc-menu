<div class="bg-course-employer bg-course">
    <h2 class="text-center text-white text-uppercase"><?php echo ($login) ? get_string('RecommendedJobs', 'local_job') : get_string('jobinfo', 'theme_oklassedu') ?></h2>
    <div class="course-popular container">
        <button class="course-popular-slider-button  course-employer-slider-pre">
            <i class="fa fa-caret-left"></i>
        </button>
        <button class="course-popular-slider-button course-employer-slider-next">
            <i class="fa fa-caret-right"></i>
        </button>
        <div class="slider slider-for course-employer-slider">

            <?php foreach ($jobs as $job) {
            $logopath = $job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/banner/banner-4.webp' : $CFG->wwwroot . '/pluginfile.php' . '/' . $job->company_logo;
            ?>
            <div class="main-course" style="margin-top: 28px;">
                <div class="m-3 shadow bg-white">
                    <div class="employer-img">
                        <img class="lazyload" loading="lazy" data-src="<?php echo $logopath ?>" alt="">
                    </div>
                    <div class="main-course-text">
                        <p><a class="ellipsis ellipsis-2 font-700"" href="<?php echo $CFG->job_wwwroot . '/local/job/jobs/view.php?jobid=' . $job->id; ?>"><?php echo $job->company_name; ?></a></p>
                        <p class="ellipsis ellipsis-2"><strong><?php echo $job->title; ?></strong></p>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>