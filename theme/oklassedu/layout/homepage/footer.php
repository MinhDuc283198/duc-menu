<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-sm-12">
                <a href="/">
                    <img class="lazyload" loading="lazy" data-src="/theme/oklassedu/pix/images/ft_logo.png" alt="master korean" />
                </a>
            </div>
            <div class="col-lg-7 col-sm-12 mb-4">
                <ul class="term">
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=3')" ><?php echo get_string('c_TermsofUse', 'local_job'); ?></li>
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2&category=3')" ><?php echo get_string('c_regulations', 'local_job'); ?></li>
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=3')" ><?php echo get_string('c_Privacypolicy', 'local_job'); ?></li>
                    <li><a href="/local/jinoboard/index.php?type=1"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></li>
                </ul>
                <div class="bt">
                    <?php echo get_string('footer', 'theme_oklassedu'); ?>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <div class="f-r">
                    <a class="foot-icon" href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a class="foot-icon" href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank">
                        <i class="fab fa-youtube"></i>
                    </a>
                    <a class="foot-icon" href="<?php echo $CFG->job_url; ?>/local/job" target="_blank">
                        <img class="lazyload" loading="lazy" data-src="/theme/oklassedu/pix/images/logo_btn_footer.png" alt="">
                    </a>
                    <a id="logoCCDV" href='http://online.gov.vn/Home/WebDetails/66177' target="_blank">
                        <img alt='vcbfg' title='' class="lazyload" loading="lazy" data-src='/theme/oklassedu/pix/images/logoSaleNoti.png'/>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer end -->
</footer>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/local/search/index.php" class="search-form container">
                <div class="p-1 bg-light rounded rounded-pill shadow-sm mb-4 search-main">
                    <div class="input-group">
                        <input type="text" name="searchtext" placeholder="Nhập từ bạn cần tìm?"  class="form-control border-0 bg-light rounded-pill search-input">
                        <div class="input-group-append">
                            <button id="button-addon1" type="submit" class="btn btn-link text-main search-button">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <ul class="search-key">
                <?php
                    foreach ($searchtext as $text) {
                        switch ($currlang) {
                            case 'ko':
                                $search_text_lang = $text->title;
                                break;
                            case 'en':
                                $search_text_lang = $text->titleen;
                                break;
                            case 'vi':
                                $search_text_lang = $text->titlevn;
                                break;
                        }
                ?>
                    <li><a href="/local/search/index.php?searchtext=<?php echo $search_text_lang; ?>">#<?php echo $search_text_lang; ?></a></li>
                <?php } ?>
                </ul>

            </form>
        </div>
    </div>
</div>

<style>
    .search-key {
        padding: 0px;
        margin-bottom: 40px;
    }
    .search-key li {
        list-style: none;
        display: inline-block;
    }
    .search-key li a {
        color: #555;
        font-size: 17px;
    }
    #exampleModal .modal-dialog {
        width: 100vw;
        margin-top: 100px;
    }
    #exampleModal .modal-dialog form {
        margin-top: 130px;
        margin-left: auto;
        margin-right: auto;
    }
    #exampleModal .modal-content {
        border: 0px;
    }
    .fixed-top {
        z-index: 1176;
    }
    @media (min-width: 576px) {
        #exampleModal .modal-dialog {
            max-width: 100vw !important;
            margin: 1.75rem auto;
        }
    }
    @media (max-width: 400px) {
        .modal-dialog {
            margin: 0px !important;
        }

        #exampleModal .modal-dialog form {
            margin-top: 90px;
        }
        .form-control-lg {
            padding: ;
        }
    }
    .search-main {
        border: 2px solid #485cc7;
    }
    .form-control:focus {
        box-shadow: none;
    }

    .search-button {
        font-size: 24px;
        padding: 0px;
        padding-right: 10px;
    }
    .form-control::placeholder {
        font-size: 0.95rem;
        color: #aaa;
        font-style: italic;
    }
    
</style>