<?php
$context = context_system::instance();
global $DB, $CFG;
require_once($CFG->dirroot . '/local/management/bannerV/lib.php');
$banner = $DB->get_record('banner_video', array('isused' => 1));

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
$pathvs = video_banner_get_file($context->id, 'banner', $banner->id, 1);
$temp->itemid = $banner->id;
?>
<div class="slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php if($banner): ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <?php else : ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <?php endif;?>
        </ol>
        <div class="carousel-inner">
            <?php if($banner): ?>
                <div class="carousel-item active">
                    <video autoplay muted loop>
                        <source src="<?php echo $pathvs; ?>" type="video/mp4">
                    </video>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100 lazyload" loading="lazy"
                             data-src="/theme/oklassedu/pix/home/pc/banner/banner-1.webp">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100 lazyload" loading="lazy"
                             data-src="/theme/oklassedu/pix/home/pc/banner/banner-2.webp">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100 lazyload" loading="lazy"
                             data-src="/theme/oklassedu/pix/home/pc/banner/banner-3.webp">
                    </a>
                </div>
            <?php else : ?>
                <div class="carousel-item active">
                    <a href="#">
                        <img class="d-block w-100 lazyload" loading="lazy"
                             data-src="/theme/oklassedu/pix/home/pc/banner/banner-1.webp">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100 lazyload" loading="lazy"
                             data-src="/theme/oklassedu/pix/home/pc/banner/banner-2.webp">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100 lazyload" loading="lazy"
                             data-src="/theme/oklassedu/pix/home/pc/banner/banner-3.webp">
                    </a>
                </div>
            <?php endif;?>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>