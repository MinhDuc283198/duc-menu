<header class="bg-white">
    <nav class="container navbar navbar-expand-lg navbar-light bg-white">
        <span class="icon-height search-icon mobile"  data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-search"></i>
        </span>
        <a class="nav-link d-sm mobile" href="#"><img class="lazyload" loading="lazy" data-src="/theme/oklassedu/pix/images/logo_beta_m.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto main-menu">
                <li class="nav-item active pc">
                    <a class="nav-link" href="/"><img class="lazyload" loading="lazy" data-src="/theme/oklassedu/pix/images/logo_not_beta.png" alt=""></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo get_string('course', 'theme_oklassedu'); ?>
                    </a>
                    <div class="dropdown-menu categories-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/local/course/list.php?id=10"><?php echo get_string('visang_korean', 'theme_oklassedu') ?></a>
                        <a class="dropdown-item" href="/local/course/list.php?id=19"><?php echo get_string('exam_practice', 'theme_oklassedu') ?></a>
                        <a class="dropdown-item" href="/local/course/list.php?id=22"><?php echo get_string('package_of_korean', 'theme_oklassedu') ?></a>
                        <a class="dropdown-item" href="/local/course/list.php?id=5"><?php echo get_string('business_korean', 'theme_oklassedu') ?></a>
                        <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/book/list.php"><?php echo get_string('book', 'theme_oklassedu'); ?></a>
                    </div>
                </li>
               
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $CFG->wwwroot; ?>/local/jinoboard/community.php"><?php echo get_string('community', 'theme_oklassedu'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $CFG->wwwroot; ?>/local/instructor/list.php"><?php echo get_string('Instructor', 'local_course'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/local/management/board/view.php" >
                       FAQ
                    </a>
                </li>
                <?php if (isloggedin()) { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $USER->firstname . $USER->lastname; ?>
                    </a>
                    <div class="dropdown-menu categories-menu" aria-labelledby="navbarDropdown">
                        <?php
                        $context = context_system::instance();
                        $user_group=$DB->get_record('lmsdata_user',array('userid'=>$USER->id));
                        if($user_group->usergroup!='pr'&&$user_group->usergroup!='bp'){
                            if (has_capability('moodle/course:view', $context)) { ?>
                                <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/management" target="_blank"><?php echo get_string('gomanagement', 'theme_oklassedu'); ?></a>
                            <?php } }else{?>
                            <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/management" target="_blank"><?php echo get_string('gomanagement', 'theme_oklassedu'); ?></a>
                        <?php }?>
                        <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/mypage/mypage.php"><?php echo get_string('myinfo', 'theme_oklassedu'); ?></a>
                        <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/mypage/mycourse.php<?php
                        $userinfos = $DB->get_records_sql('SELECT * FROM {user} u JOIN {lmsdata_user} lu on u.id=lu.userid WHERE lu.userid = :userid', array('userid' => $USER->id));
                        foreach ($userinfos as $userinfo) {
                            $prof = $userinfo->usergroup == 'pr' ? 'pr' : '';
                        }
                        if ($prof == 'pr') {
                            echo '?type=1';
                        }
                        ?>"><?php echo get_string('mycourse', 'theme_oklassedu'); ?></a>
                        <?php if($userinfo->statuscode){ ?>
                            <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/mypage/inter.php "><?php echo get_string('Interestinformation', 'local_job'); ?></a>
                        <?php } ?>
                        <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/mypage/notification.php"><?php echo get_string('notice2', 'theme_oklassedu'); ?></a>
                        <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/jinoboard/index.php?type=2"><?php echo get_string('qna', 'local_mypage'); ?></a>
                        <a class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/mypage/mypayment.php"><?php echo get_string('paymentlist', 'local_mypage'); ?></a>
                        <?php if($comid == null){?>
                            <a  class="dropdown-item" href="<?php echo $CFG->wwwroot; ?>/local/my/info.php"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a>
                        <?php  }?>
                        <a  class="dropdown-item" href="javascript:void(0);" onclick="confirmLogout();"><?php echo get_string('logout', 'theme_oklassedu'); ?></a>
                    </div>
                </li>
                <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $CFG->wwwroot; ?>/login/index.php">
                            <?php echo get_string('signup', 'theme_oklassedu'); ?>/<?php echo get_string('login', 'theme_oklassedu'); ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>

            <ul class="my-2 my-lg-0 navbar-nav main-menu menu-right">
                <li class="nav-item">
                    <span class="icon-height search-icon pc"  data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-search"></i>
                    </span>
                </li>
                <li class="nav-item li-job">
                    <div class="job-logo">
                        <a href="<?php echo $CFG->job_url; ?>/local/job" class="nav-link" target='_blank'>Việc làm</a>
                    </div>
                </li>
                &nbsp;
                <li class="nav-item li-job">
                    <div class="blog-logo">
                        <a href="<?php echo $CFG->blog_url; ?>" class="nav-link" target='_blank'>Việc làm</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="<?php echo $currlang == 'vi' ?  'active' : '' ?>" href="<?php echo $langurl . "lang=vi"; ?>">VN</a>
                    <a class="<?php echo $currlang == 'EN' ?  'active' : '' ?>" href="<?php echo $langurl . "lang=en"; ?>">EN</a>
                    <a class="<?php echo $currlang == 'KO' ?  'active' : '' ?>" href="<?php echo $langurl . "lang=ko"; ?>">KO</a>
                </li>
            </ul>
        </div>
    </nav>
</header>