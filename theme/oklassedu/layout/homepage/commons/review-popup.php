<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 10vh; ">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Course Review</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/local/course/view_submit.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="moodlecourseid" value=" <?php echo $courseEnroll->courseid ?>">
                    <div style="border-left: 3px solid #485cc7; padding-left: 15px">
                        <h3 style="font-size: 20px">Course Name <?php echo $courseReview->vi_title ?></h3>
                        <p>Teacher Name: <?php echo $courseReview->teacher[32]->fullname ?></p>
                        <img class="w-100" src="<?php echo $courseReview->img ?>" alt="">
                    </div>

                    <hr>
                    <div class="tp">
                        <?php echo get_string('selectstar', 'local_course') ?>

                        <div class="star-event hover" id="id_score_star" data-num="" onclick="score_star()"></div>
                        <input type="hidden" name="score" id="scope" value="">
                        <div class="f-r">

                            <div id="full-stars-example-two">
                                <div class="rating-group">
                                    <input disabled checked class="rating__input rating__input--none" name="score" id="rating3-none" value="0" type="radio">
                                    <label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                    <input class="rating__input" name="score" id="rating3-1" value="1" type="radio">
                                    <label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                    <input class="rating__input" name="score" id="rating3-2" value="2" type="radio">
                                    <label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                    <input class="rating__input" name="score" id="rating3-3" value="3" type="radio">
                                    <label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                    <input class="rating__input" name="score" id="rating3-4" value="4" type="radio">
                                    <label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                                    <input class="rating__input" name="score" id="rating3-5" value="5" type="radio">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bt">
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Please review</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" name="contents" place rows="3" placeholder="<?php echo get_string("enterreview", "local_course") ?>"></textarea>
                        </div>
                        <div class="text-center">
                            <input type="submit" style="padding: 5px 25px; background: #485cc7;color: #fff;border-radius: 5px;border: 1px solid #485cc7;" class="btns" value="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
