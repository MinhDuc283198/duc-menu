<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The two column layout.
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// Get the HTML for the settings bits.
$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);
// Set default (LTR) layout mark-up for a two column page (side-pre-only).
$regionmain = 'span10 pull-right';
$sidepre = 'span2 desktop-first-column';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmain = 'span10';
    $sidepre = 'span2 pull-right';
}

echo $OUTPUT->doctype();
global $DB, $COURSE;
//2019-11-06 추가작업
require_once $CFG->dirroot . '/theme/oklassedu/layout/lib.php';
$isediting = $PAGE->user_is_editing();
$menuitems = theme_oklassv2_get_coursemenu_items($COURSE->id);
$coursetitle = get_course_title();
$lmcdata_class = $DB->get_record('lmsdata_class', array('courseid'=>$COURSE->id));
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo strpos($_SERVER['SERVER_NAME'], "job") === false? get_string('sitename:mk', 'local_management') : get_string('sitename:mk_jobs', 'local_management'); ?></title>
        <meta name="description" content="<?php echo get_string('sitename:mk_description', 'local_management'); ?>">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet"/>
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body <?php echo $OUTPUT->body_attributes('course-layout'); ?>>
        <div id="wrap" class="course"> 
            <?php echo $OUTPUT->standard_top_of_body_html() ?>
            <!-- header start -->
            <?php require_once(dirname(__FILE__) . '/includes/header.php'); ?>   
            <div class="cont">
                <div class="group">

                    <div class="crs-left-block">
                        <?php if ($PAGE->__get('course')->id) { ?>
                            <ul>
                                <?php
                                echo get_courselayout_block();
                                ?>
                            </ul>
                            <div class="btns-area text-center mg-tp20">
                                <a href="/local/course/detail.php?id=<?php echo $lmcdata_class->id?>" class="btns br_blue w100"><?php echo get_string('courseintro','theme_oklassedu')?></a>
                            </div>
                        <?php } ?>
                        <ul>
                            <?php
                            if ($PAGE->__get('course')->id) {
                                echo get_course_adminayout_block();
                            }
                            ?>
                        </ul>
                        <!-- 기존 무들 블록-->
                        <?php // echo $OUTPUT->blocks('side-pre', $sidepre);  ?>
                        <!-- 모바일 버튼 -->
                        <div class="m-btn-area">
                            <a href="/" class="btns point">홈으로이동</a>
                            <a href="#" onclick="window.close()" class="btns br">강의실나가기</a>
                        </div><!-- 모바일 버튼 -->
                    </div>

                    <div class="crs-right-block">
                        <!-- 편집모드 버튼 -->
                        <?php if (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar()) { ?>
                            <div class="navbutton"> <?php echo $PAGE->button; ?></div>
                        <?php } ?>
                        <!-- 편집모드 버튼 -->
                        <?php echo $coursetitle ?>
                        <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                        ?>
                    </div>
                </div>
            </div>

            <!-- footer start -->
            <?php require_once(dirname(__FILE__) . '/includes/footer.php'); ?>   
            <!-- footer end --> 
            <?php echo $OUTPUT->standard_end_of_body_html() ?>

        </div>

    </div>
</body>
</html>
