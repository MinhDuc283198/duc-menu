<?php

$html = theme_oklassedu_get_html_for_settings($OUTPUT, $PAGE);

global $CFG, $PAGE, $OUTPUT, $VISANG;
require_once($CFG->dirroot . '/local/job/lib.php');

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');
require_once($VISANG->dirroot . '/lib/employers.php');

$VISANG->theme->header();

echo $OUTPUT->main_content();

$VISANG->theme->footer();
