<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is built using the bootstrapbase template to allow for new theme's using
 * Moodle's new Bootstrap theme engine
 *
 * @package     theme_oklassedu
 * @copyright   2015 Nephzat Dev Team, nephzat.com
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Load the Jquery and migration files
 * Load the our theme js file
 */
function theme_oklassedu_page_init(moodle_page $page) {
    $page->requires->jquery();
    $page->requires->jquery_plugin('migrate');
}

/**
 * Loads the CSS Styles and replace the background images.
 * If background image not available in the settings take the default images.
 *
 * @param $css 
 * @param $theme
 * @return string
 */
function theme_oklassedu_process_css($css, $theme) {
    // Set the background image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_oklassedu_set_logo($css, $logo);

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_oklassedu_set_customcss($css, $customcss);
    $css = theme_oklassedu_set_fontwww($css);

    return $css;
}

/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */
function theme_oklassedu_set_logo($css, $logo) {
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_oklassedu_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    static $theme;

    if (empty($theme)) {
        $theme = theme_config::load('oklassedu');
    }
    if ($context->contextlevel == CONTEXT_SYSTEM) {
        if ($filearea === 'logo') {
            return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
        } else if ($filearea === 'footerlogo') {
            return $theme->setting_file_serve('footerlogo', $args, $forcedownload, $options);
        } else if ($filearea === 'style') {
            theme_oklassedu_serve_css($args[1]);
        } else if ($filearea === 'pagebackground') {
            return $theme->setting_file_serve('pagebackground', $args, $forcedownload, $options);
        } else if (preg_match("/slide[1-9][0-9]*image/", $filearea) !== false) {
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else {
            send_file_not_found();
        }
    } else {
        send_file_not_found();
    }
}

/**
 * Serves CSS for image file updated to styles.
 *
 * @param string $filename
 * @return string
 */
function theme_oklassedu_serve_css($filename) {
    global $CFG;
    if (!empty($CFG->themedir)) {
        $thestylepath = $CFG->themedir . '/oklassedu/style/';
    } else {
        $thestylepath = $CFG->dirroot . '/theme/oklassedu/style/';
    }
    $thesheet = $thestylepath . $filename;

    /* http://css-tricks.com/snippets/php/intelligent-php-cache-control/ - rather than /lib/csslib.php as it is a static file who's
      contents should only change if it is rebuilt.  But! There should be no difference with TDM on so will see for the moment if
      that decision is a factor. */

    $etagfile = md5_file($thesheet);
    // File.
    $lastmodified = filemtime($thesheet);
    // Header.
    $ifmodifiedsince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
    $etagheader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

    if ((($ifmodifiedsince) && (strtotime($ifmodifiedsince) == $lastmodified)) || $etagheader == $etagfile) {
        theme_oklassedu_send_unmodified($lastmodified, $etagfile);
    }
    theme_oklassedu_send_cached_css($thestylepath, $filename, $lastmodified, $etagfile);
}

// Set browser cache used in php header.
function theme_oklassedu_send_unmodified($lastmodified, $etag) {
    $lifetime = 60 * 60 * 24 * 60;
    header('HTTP/1.1 304 Not Modified');
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $lifetime) . ' GMT');
    header('Cache-Control: public, max-age=' . $lifetime);
    header('Content-Type: text/css; charset=utf-8');
    header('Etag: "' . $etag . '"');
    if ($lastmodified) {
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $lastmodified) . ' GMT');
    }
    die;
}

// Cached css.
function theme_oklassedu_send_cached_css($path, $filename, $lastmodified, $etag) {
    global $CFG;
    require_once($CFG->dirroot . '/lib/configonlylib.php'); // For min_enable_zlib_compression().
    // 60 days only - the revision may get incremented quite often.
    $lifetime = 60 * 60 * 24 * 60;

    header('Etag: "' . $etag . '"');
    header('Content-Disposition: inline; filename="'.$filename.'"');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $lastmodified) . ' GMT');
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $lifetime) . ' GMT');
    header('Pragma: ');
    header('Cache-Control: public, max-age=' . $lifetime);
    header('Accept-Ranges: none');
    header('Content-Type: text/css; charset=utf-8');
    if (!min_enable_zlib_compression()) {
        header('Content-Length: ' . filesize($path . $filename));
    }

    readfile($path . $filename);
    die;
}

/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_oklassedu_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);
    return $css;
}

/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * Do not add Clean specific logic in here, child themes should be able to
 * rely on that function just by declaring settings with similar names.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_oklassedu_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }

    $return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote text-center">'.format_text($page->theme->settings->footnote).'</div>';
    }

    return $return;
}

/**
 * Loads the CSS Styles and put the font path
 *
 * @param $css
 * @return string
 */
function theme_oklassedu_set_fontwww($css) {
    global $CFG, $PAGE;
    if (empty($CFG->themewww)) {
        $themewww = $CFG->wwwroot."/theme";
    } else {
        $themewww = $CFG->themewww;
    }

    $tag = '[[setting:fontwww]]';
    $theme = theme_config::load('oklassedu');
    $css = str_replace($tag, $themewww.'/oklassedu/fonts/', $css);
    //$css = str_replace($tag, 'fonts/', $css);
    return $css;
}

/**
 * Logo Image URL Fetch from theme settings
 *
 * @return string
 */
function theme_oklassedu_get_logo_url($type='header') {
    global $OUTPUT,$SITE;
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('oklassedu');
    }
    $sitename = format_string($SITE->fullname, true, array('context' => context_course::instance(SITEID)));
    if ($type == "header") {
        $logo = $theme->setting_file_url('logo', 'logo');
        $logo = empty($logo) ? $sitename : '<img src="'.$logo.'" alt="'.$sitename.'"/>';
    } else if ($type == "footer") {
        $logo = $theme->setting_file_url('footerlogo', 'footerlogo');
        $logo = empty($logo) ? 'JINOTECH' : '<img src="'.$logo.'" alt="'.$sitename.'"/>';
    }
    return $logo;
}

function theme_oklassedu_render_slideimg($p, $sliname) {
    global $PAGE, $OUTPUT;

    $nos = theme_oklassedu_get_setting('numberofslides');
    $i = $p % 3;
    $slideimage = $OUTPUT->pix_url('home/slide'.$i, 'theme');

    // Get slide image or fallback to default.
    if (theme_oklassedu_get_setting($sliname)) {
        $slideimage = $PAGE->theme->setting_file_url($sliname , $sliname);
    }
    return $slideimage;
}

function theme_oklassedu_get_setting($setting, $format = false) {
    global $CFG;
    require_once($CFG->dirroot . '/lib/weblib.php');
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('oklassedu');
    }
    if (empty($theme->settings->$setting)) {
        return false;
    } else if (!$format) {
        return $theme->settings->$setting;
    } else if ($format === 'format_text') {
        return format_text($theme->settings->$setting, FORMAT_PLAIN);
    } else if ($format === 'format_html') {
        return format_text($theme->settings->$setting, FORMAT_HTML, array('trusted' => true, 'noclean' => true));
    } else {
        return format_string($theme->settings->$setting);
    }
}

/**
 * Return the current theme url
 *
 * @return string
 */
function theme_oklassedu_theme_url() {
    global $CFG, $PAGE;
    $themeurl = $CFG->wwwroot.'/theme/'. $PAGE->theme->name;
    return $themeurl;
}

/**
 * Display Footer Block Custom Links
 * @param string $menu_name Footer block link name.
 * @return string The Footer links are return.
 */
function theme_oklassedu_generate_links($menuname = '') {
    global $CFG, $PAGE;
    $htmlstr = '';
    $menustr = theme_oklassedu_get_setting($menuname);
    $menusettings = explode("\n", $menustr);
    foreach ($menusettings as $menukey => $menuval) {
        $expset = explode("|", $menuval);
        list($ltxt, $lurl) = $expset;
        $ltxt = trim($ltxt);
        $ltxt = theme_oklassedu_lang($ltxt);
        $lurl = trim($lurl);
        if (empty($ltxt)) {
            continue;
        }
        if (empty($lurl)) {
            $lurl = 'javascript:void(0);';
        }

        $pos = strpos($lurl, 'http');
        if ($pos === false) {
            $lurl = new moodle_url($lurl);
        }
        $htmlstr .= '<li><a href="'.$lurl.'">'.$ltxt.'</a></li>'."\n";
    }

    return $htmlstr;
}

/**
 * Fetch the hide course ids
 *
 * @return array
 */
function theme_oklassedu_hidden_courses_ids() {
    global $DB;
    $hcourseids = array();
    $result = $DB->get_records_sql("SELECT id FROM {course} WHERE visible='0' ");
    if (!empty($result)) {
        foreach ($result as $row) {
            $hcourseids[] = $row->id;
        }
    }
    return $hcourseids;
}

/**
 * Remove the html special tags from course content.
 * This function used in course home page.
 *
 * @param string $text
 * @return string
 */
function theme_oklassedu_strip_html_tags( $text ) {
    $text = preg_replace(
        array(
            // Remove invisible content.
            '@<head[^>]*?>.*?</head>@siu',
            '@<style[^>]*?>.*?</style>@siu',
            '@<script[^>]*?.*?</script>@siu',
            '@<object[^>]*?.*?</object>@siu',
            '@<embed[^>]*?.*?</embed>@siu',
            '@<applet[^>]*?.*?</applet>@siu',
            '@<noframes[^>]*?.*?</noframes>@siu',
            '@<noscript[^>]*?.*?</noscript>@siu',
            '@<noembed[^>]*?.*?</noembed>@siu',
            // Add line breaks before and after blocks.
            '@</?((address)|(blockquote)|(center)|(del))@iu',
            '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
            '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
            '@</?((table)|(th)|(td)|(caption))@iu',
            '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
            '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
            '@</?((frameset)|(frame)|(iframe))@iu',
        ),
        array(
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
            "\n\$0", "\n\$0",
        ),
        $text
    );
    return strip_tags( $text );
}

/**
 * Cut the Course content.
 *
 * @param $str
 * @param $n
 * @param $end_char
 * @return string
 */
function theme_oklassedu_course_trim_char($str, $n = 500, $endchar = '&#8230;') {
    if (strlen($str) < $n) {
        return $str;
    }

    $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));
    if (strlen($str) <= $n) {
        return $str;
    }

    $out = "";
    $small = substr($str, 0, $n);
    $out = $small.$endchar;
    return $out;
}

function theme_oklassedu_lang($key = '') {
    $pos = strpos($key, 'lang:');
    if ($pos !== false) {
        list($l, $k) = explode(":", $key);
        $v = get_string($k, 'theme_oklassedu');
        return $v;
    } else {
        return $key;
    }

}

function theme_oklassedu_get_file($contextid, $filearea, $itemid=0) {
    global $CFG, $OUTPUT;
    
    $fs = get_file_storage();
    $files = $fs->get_area_files($contextid, 'local_courselist', "$filearea", $itemid, 'id');
    $fileobj = '';
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
        
        
        if ($file->get_filesize() > 0) {
            $fileobj = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_courselist/'.$filearea.'/' . $itemid . '/' . $filename);
        }
    }
    return $fileobj;
}
function theme_oklassedu_teacher() {
    global $DB, $USER;
    
$sql_from = "from {lmsdata_class} lc
            join {lmsdata_course} lco on lc.parentcourseid = lco.id
            join {course} co on co.id = lc.courseid
            join {enrol} e on e.courseid = co.id 
            join {user_enrolments} ue on ue.enrolid = e.id and ue.userid = $USER->id 
            JOIN {context} ctx ON ctx.instanceid = co.id AND contextlevel = 50 
            join {role_assignments} ra on ra.contextid = ctx.id and ra.userid = ue.userid
            JOIN {role} ro ON ra.roleid = ro.id and ro.id IN (3, 4) ";

$courses_count = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from);
    return $courses_count;
}


function jh_courselist_get_file($contextid, $filearea, $itemid=0) {
    global $CFG, $OUTPUT;
    
    $fs = get_file_storage();
    $files = $fs->get_area_files($contextid, 'local_courselist', "$filearea", $itemid, 'id');
    $fileobj = '';
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_courselist/'.$filearea.'/' . $itemid . '/' . $filename);
        if ($file->get_filesize() > 0) {
            $fileobj = $path;
        }
    }
    return $fileobj;
}

function oklassedu_change_lang($url, $key) {
    global $CFG, $DB, $USER;
    
    $request_uri = $_SERVER['REQUEST_URI'];

    if (strpos($request_uri, '?')) {
        $url = $CFG->wwwroot . $request_uri . '&';
    } else {
        $url = $CFG->wwwroot . $request_uri . '?';
    }

    if (strpos($url, '?') === false) {
        return $url;
    }
    list($url, $query) = explode('?', $url);
    $temp = explode('&', $query);
    foreach($temp as $k => $v) {
        if(substr($v, 0, strlen($key) + 1) == $key.'=') {
            $lang = explode('=', $temp[$k]);
            $USER->lang = $lang[1];
            $DB->update_record('user', $USER);
            unset($temp[$k]);
        }
    }
    return $url.'?'.implode('&',$temp);
}
function visang_job_sharing_user(){
    global $DB, $USER;
    $query = "select lu.* from {user} u JOIN {lmsdata_user} lu ON u.id = lu.userid AND lu.userid=:userid WHERE ( lu.statuscode is null or lu.statuscode = 0 or lu.statuscode = '') AND lu.joinstatus = :joinstatus";
    $user = $DB->get_record_sql($query,array('userid'=>$USER->id, 'joinstatus'=>'j'));
    //if($USER->id == 33){
        if(((strpos($_SERVER['SCRIPT_NAME'], '/local/job/') !== true) || $_SERVER['HTTP_HOST'] != 'job.masterkorean.vn')){
            if(!empty($user)) {
                if(strpos($_SERVER['REQUEST_URI'], "information_sharing") === false){
                    return true;
                }
            }
        }
    //}
    return false;
}