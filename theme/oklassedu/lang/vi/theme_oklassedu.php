<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_oklassedu', language 'en'
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>oklassedu</h2>
<p><img class=img-polaroid src="oklassedu/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>oklassedu is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
<h3>Parents</h3>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
<h3>Theme Credits</h3>
<p>Authors: Bas Brands, David Scotson, Mary Evans<br>
Contact: bas@sonsbeekmedia.nl<br>
Website: <a href="http://www.basbrands.nl">www.basbrands.nl</a>
</p>
<h3>Report a bug:</h3>
<p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
<h3>More information</h3>
<p><a href="oklassedu/README.txt">How to copy and customise this theme.</a></p>
</div></div>';

$string['pluginname'] = 'oklassedu';
$string['configtitle'] = 'oklassedu';

//모바일 헤더 타이틀
$string['mtitle'] = 'Giảng đường';

//메뉴
$string['menu:mypage'] = 'Trang của tôi';
$string['menu:mypage02'] = 'Trang của tôi';
$string['menu:dashboard'] = 'Khóa học của tôi';
$string['menu:filemanage'] = 'Quản lý tệp';
$string['menu:coursemanage'] = 'Quảng lý khóa học';
$string['menu:lcmsmanage'] = 'Quản lý bài giảng';
$string['menu:schedule'] = 'Lịch biểu';
$string['menu:message'] = 'Tin nhắn';
$string['menu:coursenotification'] = 'Thông báo';
$string['menu:courseboard'] = 'Bản tin khóa học';
$string['menu:evaluation'] = 'Đánh giá';
$string['menu:gradeview'] = 'Xem điểm';
$string['menu:editprofile'] = 'Sửa thông tin cá nhân';

$string['menu:regular'] = 'Khóa học tiêu chuẩn';
$string['menu:irregular'] = 'Khóa học đặc biệt';
$string['menu:elearning'] = 'Công đồng UST';
$string['menu:mycourse'] = 'Khóa học của tôi';
$string['menu:auditenrol'] = 'Đăng ký bài giảng';
$string['menu:courseenrol'] = 'Đăng ký khóa học';
$string['menu:completionview'] = 'Xác nhận hoàn thành';
$string['menu:addcourse'] = 'Tạo khóa học';
$string['menu:allcourse'] = 'Tất cả khóa học';
$string['menu:assistant'] = 'Trợ giảng';

$string['menu:guide'] = 'Hướng dẫn sử dụng';
$string['menu:notice'] = 'Thống báo';
$string['menu:qna'] = 'Hỏi đáp';
$string['menu:faq'] = 'FAQ';
$string['menu:reference'] = 'Tài liệu tham khảo';
$string['menu:manual'] = 'Hướng dẫn sử dụng';

//settings
$string['sitelogo'] = 'Logo';
$string['sitename'] = 'Hiển thị tên trang và logo với kích thước nhỏ';
$string['sitenamedesc'] = 'If there is no small logo, the site name is always displayed in the navigation bar. If a small logo is set, it may be displayed with or without the site name.';
$string['smalllogo'] = 'Small logo';
$string['smalllogodesc'] = 'The small logo is displayed in the navigation bar. If there is a header logo for the front page and login page, the small logo is not displayed on these pages.';

$string['connectus'] = 'Connect with us';
$string['contact'] = 'Contact';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';
$string['defaultaddress'] = '308 Negra Narrow Lane, Albeeze, New york, 87104';
$string['defaultemailid'] = 'info@example.com';
$string['defaultphoneno'] = '(000) 123-456';
$string['emailid'] = 'Email';
$string['fburl'] = 'Facebook';
$string['fburl_default'] = 'https://www.facebook.com/yourfacebookid';
$string['fburldesc'] = 'The Facebook url of your organisation.';
$string['pcourses'] = 'Recommend Courses';
$string['pcoursesdesc'] = 'Please give the recommended courses id should separated by comma.';
$string['promotedcoursesheading'] = 'Recommended Courses';
$string['footbgimg'] = 'Background Image';
$string['footbgimgdesc'] = 'Background Image size should be following size 1345 X 517';
$string['footerheading'] = 'Footer Blocks';
$string['footerlogo'] = 'Footer Logo';
$string['footerlogodesc'] = 'Please upload your custom footer logo here if you want to add it to the footer.
                       <br>The image should be 80px high and any reasonable width (minimum:205px) that suits.';
$string['footnote'] = 'Footnote';
$string['footnotedefault'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and tronic typesetting, sheets taining Lorem Ipsum passages.';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.Please give the either language key or text.For ex: lang:display or Display';
$string['frontpageheading'] = 'Front page';
$string['gpurl'] = 'Google+';
$string['gpurl_default'] = 'https://www.google.com/+yourgoogleplusid';
$string['gpurldesc'] = 'The Google+ url of your organisation.';
$string['headerheading'] = 'Header';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.
                       <br>The image should be 50px high and any reasonable width (minimum:235px) that suits.';
$string['numberofslides'] = 'Number of slides';
$string['numberofslides_desc'] = 'Number of slides on the slider.';
$string['numberoftmonials'] = 'Number of Testimonials';
$string['numberoftmonials_desc'] = 'Number of Testimonials on the Home Page.';
$string['phoneno'] = 'Phone No';
$string['pinurl'] = 'Pinterest';
$string['pinurl_default'] = 'https://in.pinterest.com/yourpinterestname/';
$string['pinurldesc'] = 'The Pinterest url of your organisation.';
$string['knowmore'] = 'Know More';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['signup'] = 'Sign up';
$string['slidecaption'] = 'Slide caption';
$string['slidecaptiondefault'] = 'Moodle Learning Management System';
$string['slidecaptiondesc'] = 'Enter the caption text to use for the slide';
$string['slideimage'] = 'Slide image';
$string['slideimagedesc'] = 'The image should be 1366px X 385px.';
$string['slideno'] = 'Slide {$a->slide}';
$string['slidenodesc'] = 'Enter the settings for slide {$a->slide}.';
$string['slideshowdesc'] = 'This creates a slide show of up to twelve slides for you to promote important elements of your site.  The show is responsive where image height is set according to screen size.If no image is selected for a slide, then the default_slide images in the pix folder is used.';
$string['slideshowheading'] = 'Home Page Slider';
$string['slideshowheadingsub'] = 'Slide show for the front page';
$string['slideurl'] = 'Slide link';
$string['slideurldesc'] = 'Enter the target destination of the slide\'s image link';
$string['toggleslideshow'] = 'Slide show display';
$string['toggleslideshowdesc'] = 'Choose if you wish to hide or show the slide show.';
$string['twurl'] = 'Twitter';
$string['twurl_default'] = 'https://twitter.com/yourtwittername';
$string['twurldesc'] = 'The Twitter url of your organisation.';
$string['themegeneralsettings'] = 'General';
$string['patternselect'] = 'Site Color Scheme';
$string['patternselectdesc'] = 'Select the color scheme you want to have for your site.';
$string['colorscheme'] = 'Color Scheme';
$string['footerblock'] = 'Footer Block';
$string['title'] = 'Title';
$string['footerbtitle_desc'] = 'Please give the footer block title either language key or text.For ex: lang:display or Display';
$string['footerbtitle2default'] = 'Quick Links';
$string['footerbtitle3default'] = 'Follow Us';
$string['footerbtitle4default'] = 'Contact';
$string['footerblink'] = 'Footer Block Link';
$string['footerblink2default'] = 'lang:aboutus|http://www.example.com/about-us.php
lang:termsofuse|http://www.example.com/terms-of-use.php
lang:faq|http://www.example.com/faq.php
lang:support|http://www.example.com/support.php
lang:contact|http://www.example.com/contact.php';
$string['footerblink_desc'] = 'You can configure a Footer Block Links here to be shown by themes. Each line consists of some menu text either language key or text, a link URL (optional),separated by pipe characters.For example:
<pre>
lang:moodlecommunity|https://moodle.org
Moodle Support|https://moodle.org/support
</pre>';
$string['medianame1'] = 'Facebook';
$string['medianame2'] = 'Twitter';
$string['medianame3'] = 'Google Plus';
$string['medianame4'] = 'Pinterest';

$string['mediaicon1'] = 'fa-facebook-f';
$string['mediaicon2'] = 'fa-twitter';
$string['mediaicon3'] = 'fa-google-plus';
$string['mediaicon4'] = 'fa-pinterest-p';
$string['default_color'] = 'Default color scheme';
$string['color_schemes_heading'] = 'Color Schemes';
$string['promotedtitledefault'] = 'Recommended courses';
$string['promotedtitledesc'] = 'Meet the recommended course Oklass';
$string['pcourseenable'] = 'Enable Recommended courses';
$string['login'] = 'Login';
$string['link'] = 'Link';
$string['text'] = 'Text';
$string['icon'] = 'Icon';
$string['loginheader'] = 'Login into your account';
$string['marketingspotsheading'] = 'Marketing Spots';
$string['marketingspot'] = 'Marketing Spot';
$string['marketingspot1desc'] = 'Enter the Marketing Spot 1 block content either language key or text here.For ex: lang:display or Display';
$string['marketingspot2desc'] = 'Enter the Marketing Spot 2 block content either language key or text here.For ex: lang:display or Display';
$string['mspottitledesc'] = 'Enter the Marketing Spot Title either language key or Text.For ex: lang:display or Display';
$string['mspotdescdesc'] = 'Enter the Marketing Spot Description either language key or Text.For ex: lang:display or Display';
$string['mspot1titledefault'] = 'Multilingual';
$string['mspot1descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot2titledefault'] = 'Online Study';
$string['mspot2descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot3titledefault'] = 'Community Support';
$string['mspot3descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot4titledefault'] = 'Responsive Design';
$string['mspot4descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['faicondesc'] = 'Enter the name of the icon you wish to use.  List is <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target="_new">here</a>. Just enter what is after the "fa-".';
$string['slideurltext'] = 'Slide link text';
$string['slideurltextdesc'] = 'Enter the target destination of the slide\'s image link text,
either language key or Text.For ex: lang:display or Display';
$string['termsofuse'] = 'Terms of use';
$string['faq'] = 'FAQ';
$string['support'] = 'Support';
$string['copyright'] = '<p class="text-center">Copyright &copy; 2015 - Developed by 
            <a href="http://www.nephzat.com/">Nephzat.com</a>.Powered by <a href="https://moodle.org">Moodle</a></p>';
$string['footeremail'] = 'E-mail:';
$string['irregularcourse'] = 'Iregular Course';
$string['regularcourse'] = 'Regular Course';

//내 강좌
$string['regular'] = 'Thường xuyên';
$string['irregular'] = 'Không thường xuyên';
$string['elearning'] = 'Công đồng UST';
$string['teacher'] = 'Giáo sư';
$string['teachername'] = 'Tên giáo sư';
$string['coursename'] = 'Tên khóa học';
$string['all'] = 'Tất cả';
$string['year'] = 'Năm';
$string['term'] = 'Thời gian khóa học';
$string['star_score'] = 'Xếp hạng';
$string['year_term'] = 'Thời gian khóa học';
$string['application'] = 'Đăng ký';


//회원가입,로그인
$string['signup'] = 'Đăng ký';
$string['login'] = 'Đăng nhập';
$string['findid'] = 'Tìm ID';
$string['findpw'] = 'Tìm mật khẩu';
$string['findidpw'] = 'Tìm ID / Mật khẩu';
$string['saveid'] = 'Lưu ID';

$string['learningstats'] = 'Tình trạng học vấn';
$string['progressstats'] = 'Tình trạng tiến độ';
$string['frontext'] = 'Hãy tham gia bài giảng trực tuyến OKlass ngây bây giờ!';

$string['coursemove'] = 'Tên khóa học';
$string['course'] = 'Khóa học';
$string['notice2'] = 'Thông báo';
$string['please_login'] = 'Bạn phải đăng nhập để xem';
$string['showall'] = 'Hiển thị tất cả';
$string['message'] = 'Tin nhắn';
$string['showallmessage'] = 'Hiện tất cả tin nhắn';

$string['openingrecommendedcourse'] = 'Gợi ý khóa học đang mở';
$string['regulartitle'] = 'Thường xuyên / không thường xuyên';
$string['COOC'] = 'Cộng đồng UST';
$string['OCW'] = 'OCW';
$string['MOOC'] = 'MOOC';
$string['regularcontent'] = 'OKlass LMS có thể tự động mở các khóa học thông thường được liên kết với hệ thống ERP và mở thêm các khóa học đặc biệt theo nhu cầu của người hướng dẫn / tổ chức.';
$string['cooccontent'] = 'OKlass 1.5 cung cấp khả năng tự do tạo cộng đồng và tổ chức cộng đồng học tập trong một hệ thống quản lý học tập (LMS).';
$string['ocwcontent'] = 'Thông qua các khóa học mở, bất kỳ ai trên Internet đều có thể truy cập và sử dụng các tài liệu mà họ muốn.';
$string['mooccontent'] = 'Đây là dịch vụ bài giảng trực tuyến miễn phí, bất cứ ai cũng có thể nghe và sử dụng các bài giảng mà họ thích';
//footer
$string['footerbtitle'] = 'Trang liên quan';
$string['homepage'] = 'Trang chủ';
$string['manage'] = 'Bảo trì';
$string['contact'] = 'Liên hệ';
$string['address'] = 'Đường số 3,Tòa nhà Dongun, 454-9 Bangbae-dong, Seocho-gu, Seoul';

$string['summer'] = 'Thời gian khóa học Hè';
$string['winter'] = 'Thời gian khóa học Đông';

$string['common'] = 'Thường gặp';
$string['progressdate'] = 'Ngày kết thúc';
$string['completioncriteria'] = 'Tiêu chí hoàn thành';
$string['overallprogressrate'] = 'Tiến độ chung';
$string['courselist'] = 'Danh sách khóa học';
$string['notice'] = 'Thông báo khóa học';
$string['quiz'] = 'Trắc nghiệm';
$string['reference'] = 'Tài liệu tham khảo';
$string['qna'] = 'Hỏi đáp Khóa học';
$string['coursereview'] = 'Đánh giá khóa học';
$string['courseintro'] = 'Giới thiệu khóa học';
$string['courselist'] = 'Danh sách khóa học';
$string['facebooklogin'] = 'Đăng nhập bằng tài khoản Facebook';
$string['googlelogin'] = 'Đăng nhập bằng tài khoản Google';
$string['zalologin'] = 'Đăng nhập bằng tài khoản Zalo';

$string['tplan'] = 'Kế hoạch Giảng dạy';
$string['coursecompletesetting'] = 'Cài đặt khóa học hoàn thành';
$string['quizbank'] = 'Kho câu hỏi trắc nghiệm';
$string['learningstatus'] = 'Tình trạng học vấn';
$string['participantlist'] = 'Danh sách thành viên tham gia';


$string['myinfo'] = 'Thông tin của tôi';
$string['mycoupons'] = 'Phiếu giảm giá của tôi';
$string['mycourse'] = 'Khóa học của tôi';
$string['withdrawal'] = 'Rút thành viên';

$string['search_txt'] = 'Tìm kiếm tên khóa học, giáo trình';
$string['search'] = 'Tìm kiếm';
$string['learning'] = 'Học';
$string['recommendedcourse'] = 'Các khóa học phổ biến';
$string['recommendedcourse1'] = 'Lớp học đề xuất';
$string['package'] = 'Gói tiếng Hàn đề xuất';
$string['recommended'] = 'Gợi ý';

$string['sir'] = ', Chào!';
$string['viewmycourse'] = 'Xem khóa học của tôi';
$string['chargelecture'] = 'Phí giảng bài';
$string['getlogin'] = 'Khả dụng sau khi đăng nhập';
$string['customerservice'] = 'Dịch vụ khách hàng';
$string['job'] = 'Việc làm';
$string['facebook'] = 'Tư vấn qua ';
$string['youtube'] = 'Youtube';
$string['counseling'] = 'Facebook';
$string['logout'] = 'Đăng xuất';
$string['book'] = 'Giáo trình';
$string['board'] = 'Bản tin';
$string['community'] = 'Cộng đồng';
$string['service'] = 'Dịch vụ';
$string['terms'] = 'Điều kiện';

$string['customerservice2'] = 'Hỗ trợ khách hàng';
$string['terms2'] = 'Chính sách bảo mật';
$string['footer'] = '<p>Tên công ty: Công ty TNHH Giáo dục Visang Việt Nam</p>
            <p>Trụ sở chính: Tầng 2, FLC Landmark Tower, đường Lê Đức Thọ, phường Mỹ Đình 2, quận Nam Từ Liêm, thành phố Hà Nội, Việt Nam</p>
            <p>MST: 0109066143 do Sở KH & ĐT thành phố Hà Nội cấp ngày 14/01/2020  </p>
            <p>Người đại diện: Mr. Lee Young Geun  </p>
            <p>Điện thoại: 0243-6886-333 | E-mail: visang@masterkorean.vn</p>
            <p class="copy">Copyright © VISANG Education Group Vietnam Company</p>';
$string['ftaddress']='Địa chỉ : Tầng 2, FLC Landmark Tower, đường Lê Đức Thọ, phường Mỹ Đình 2, quận Nam Từ Liêm, thành phố Hà Nội, Việt Nam';
$string['fttel']='Điện thoại : 0243-6886-333';
$string['ftmail']=' E-mail : visang@masterkorean.vn';
$string['ftcopyright']='Copyright © VISANG Education Group Vietnam Company';

$string['getlogin2'] = 'Đăng kí và đăng nhập để nhận những thông tin gợi ý việc làm hữu ích.';
$string['viewmasterkorea'] = 'Tận hưởng nhiều trải nghiệm<br />khác nhau từ Master Korean.';
$string['qna2'] = 'Hỏi đáp <br />1:1';
$string['jobinfo'] = 'VIỆC LÀM ĐỀ XUẤT';
$string['jobinfologin'] = 'Công việc được đề xuất';
$string['notice2'] = 'Thông báo';
$string['close'] = 'Đóng';
$string['things'] = 'Nhiều thứ';
$string['terms3'] = 'Chính sách sử dụng dịch vụ';
$string['korean'] = 'Tiếng Hàn';
$string['gomanagement'] = 'Mở trang quản trị';
$string['logout:confirm'] = 'Bạn có muốn đăng xuất?';
$string['feedback'] = 'Vui lòng chia sẻ cho chúng tôi biết về trải nghiệm học tập của bạn trên trang học tập trực tuyến Master Korean!';
$string['fbfullname'] = 'Tên đầy đủ';
$string['fbyouremail'] = 'Email của bạn';
$string['fbphonenumber'] = 'Số điện thoại của bạn';
$string['fbbutton'] = 'Gửi phản hồi';
$string['fbnotice'] = '(Master Korean sẽ giải đáp các thắc mắc của bạn trong vòng 1-2h. Cảm ơn bạn.)';
$string['fbnoticename'] = 'Vui lòng nhập tên';
$string['fbnoticeemail'] = 'Vui lòng nhập email';
$string['fbnoticephone'] = 'Vui lòng nhập số điện thoại';
$string['fbnoticesuccess'] = 'Gửi email thành công';
$string['fbnoticenosuccess'] = 'Gửi email thất bại';
$string['feedback'] = 'Vui lòng chia sẻ cho chúng tôi biết về trải nghiệm học tập của bạn trên trang học tập trực tuyến Master Korean!';
$string['fbfullname'] = 'Tên đầy đủ';
$string['fbyouremail'] = 'Email của bạn';
$string['fbphonenumber'] = 'Số điện thoại của bạn';
$string['fbbutton'] = 'Gửi phản hồi';
$string['fbnotice'] = '(Master Korean sẽ trả lời trong vòng 1-2h giải đáp các thắc mắc về quá trình học tập của bạn trên hệ thống. Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi)';

$string['account_exist'] = 'Bạn chưa có tài khoản?';
$string['visang_korean'] = 'Tiếng Hàn Visang';
$string['exam_practice'] = 'Luyện Giải Đề Thi';
$string['package_of_korean'] = 'Gói Khoá Học';
$string['business_korean'] = 'Tiếng Hàn Business';


$string['topik_course'] = '[-30%] Luyện Thi TOPIK II (43 Bài Giảng)';
$string['business_course'] = '[-30%] Tiếng Hàn Business [GV Cheon Seong Ok] - 24 Bài Giảng';
$string['beginner1_course'] = '[-30%] Tiếng Hàn Sơ Cấp 1 (100 Bài Giảng)';
$string['beginner2_course'] = '[-30%] Tiếng Hàn Sơ Cấp 2 (100 Bài Giảng) ';
$string['intermediate1_course'] = '[-30%] Tiếng Hàn Trung Cấp 1 (80 Bài Giảng)';
$string['intermediate2_course'] = '[-30%] Tiếng Hàn Trung Cấp 2 (80 Bài Giảng)';