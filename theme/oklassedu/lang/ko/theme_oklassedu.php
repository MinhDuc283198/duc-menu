<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_oklassedu', language 'en'
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>oklassedu</h2>
<p><img class=img-polaroid src="oklassedu/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>oklassedu is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
<h3>Parents</h3>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
<h3>Theme Credits</h3>
<p>Authors: Bas Brands, David Scotson, Mary Evans<br>
Contact: bas@sonsbeekmedia.nl<br>
Website: <a href="http://www.basbrands.nl">www.basbrands.nl</a>
</p>
<h3>Report a bug:</h3>
<p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
<h3>More information</h3>
<p><a href="oklassedu/README.txt">How to copy and customise this theme.</a></p>
</div></div>';


$string['pluginname'] = 'oklassedu';
$string['configtitle'] = 'oklassedu';

//모바일 헤더 타이틀
$string['mtitle'] = '강의실';

//메뉴
$string['menu:mypage'] = 'My Page';
$string['menu:mypage02'] = '마이페이지';
$string['menu:dashboard'] = '내 강좌';
$string['menu:filemanage'] = '파일관리';
$string['menu:coursemanage'] = '강의관리';
$string['menu:lcmsmanage'] = '강의콘텐츠관리';
$string['menu:schedule'] = '개인일정표';
$string['menu:message'] = '메시지';
$string['menu:coursenotification'] = '강의알림';
$string['menu:courseboard'] = '강의게시판';
$string['menu:evaluation'] = '강의평가';
$string['menu:gradeview'] = '성적확인';
$string['menu:editprofile'] = '개인정보수정';

$string['menu:regular'] = '교과 과정';
$string['menu:irregular'] = '비교과 과정';
$string['menu:elearning'] = 'SNUT 커뮤니티';
$string['menu:mycourse'] = '수강강좌';
$string['menu:auditenrol'] = '청강신청';
$string['menu:courseenrol'] = '수강신청';
$string['menu:completionview'] = '이수확인';
$string['menu:addcourse'] = '강좌개설';
$string['menu:allcourse'] = '전체강좌';
$string['menu:assistant'] = '조교/청강신청';

$string['menu:guide'] = '이용안내';
$string['menu:notice'] = '공지사항';
$string['menu:qna'] = 'Q&AMP;A';
$string['menu:faq'] = 'FAQ';
$string['menu:reference'] = '자료실';
$string['menu:manual'] = '매뉴얼';

//settings
$string['sitelogo'] = 'Site logo';
$string['sitename'] = 'Display site name along with small logo';
$string['sitenamedesc'] = 'If there is no small logo, the site name is always displayed in the navigation bar. If a small logo is set, it may be displayed with or without the site name.';
$string['smalllogo'] = 'Small logo';
$string['smalllogodesc'] = 'The small logo is displayed in the navigation bar. If there is a header logo for the front page and login page, the small logo is not displayed on these pages.';

$string['connectus'] = 'Connect with us';
$string['contact'] = 'Contact';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';
$string['defaultaddress'] = '308 Negra Narrow Lane, Albeeze, New york, 87104';
$string['defaultemailid'] = 'info@example.com';
$string['defaultphoneno'] = '(000) 123-456';
$string['emailid'] = 'Email';
$string['fburl'] = 'Facebook';
$string['fburl_default'] = 'https://www.facebook.com/yourfacebookid';
$string['fburldesc'] = 'The Facebook url of your organisation.';
$string['pcourses'] = 'Recommend Courses';
$string['pcoursesdesc'] = 'Please give the recommended courses id should separated by comma.';
$string['promotedcoursesheading'] = 'Recommended Courses';
$string['footbgimg'] = 'Background Image';
$string['footbgimgdesc'] = 'Background Image size should be following size 1345 X 517';
$string['footerheading'] = 'Footer Blocks';
$string['footerlogo'] = 'Footer Logo';
$string['footerlogodesc'] = 'Please upload your custom footer logo here if you want to add it to the footer.
                       <br>The image should be 80px high and any reasonable width (minimum:205px) that suits.';
$string['footnote'] = 'Footnote';
$string['footnotedefault'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and tronic typesetting, sheets taining Lorem Ipsum passages.';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.Please give the either language key or text.For ex: lang:display or Display';
$string['frontpageheading'] = 'Front page';
$string['gpurl'] = 'Google+';
$string['gpurl_default'] = 'https://www.google.com/+yourgoogleplusid';
$string['gpurldesc'] = 'The Google+ url of your organisation.';
$string['headerheading'] = 'Header';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.
                       <br>The image should be 50px high and any reasonable width (minimum:235px) that suits.';
$string['numberofslides'] = 'Number of slides';
$string['numberofslides_desc'] = 'Number of slides on the slider.';
$string['numberoftmonials'] = 'Number of Testimonials';
$string['numberoftmonials_desc'] = 'Number of Testimonials on the Home Page.';
$string['phoneno'] = 'Phone No';
$string['pinurl'] = 'Pinterest';
$string['pinurl_default'] = 'https://in.pinterest.com/yourpinterestname/';
$string['pinurldesc'] = 'The Pinterest url of your organisation.';
$string['knowmore'] = 'Know More';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['signup'] = 'Sign up';
$string['slidecaption'] = 'Slide caption';
$string['slidecaptiondefault'] = 'Moodle Learning Management System';
$string['slidecaptiondesc'] = 'Enter the caption text to use for the slide';
$string['slideimage'] = 'Slide image';
$string['slideimagedesc'] = 'The image should be 1366px X 385px.';
$string['slideno'] = 'Slide {$a->slide}';
$string['slidenodesc'] = 'Enter the settings for slide {$a->slide}.';
$string['slideshowdesc'] = 'This creates a slide show of up to twelve slides for you to promote important elements of your site.  The show is responsive where image height is set according to screen size.If no image is selected for a slide, then the default_slide images in the pix folder is used.';
$string['slideshowheading'] = 'Home Page Slider';
$string['slideshowheadingsub'] = 'Slide show for the front page';
$string['slideurl'] = 'Slide link';
$string['slideurldesc'] = 'Enter the target destination of the slide\'s image link';
$string['toggleslideshow'] = 'Slide show display';
$string['toggleslideshowdesc'] = 'Choose if you wish to hide or show the slide show.';
$string['twurl'] = 'Twitter';
$string['twurl_default'] = 'https://twitter.com/yourtwittername';
$string['twurldesc'] = 'The Twitter url of your organisation.';
$string['themegeneralsettings'] = 'General';
$string['patternselect'] = 'Site Color Scheme';
$string['patternselectdesc'] = 'Select the color scheme you want to have for your site.';
$string['colorscheme'] = 'Color Scheme';
$string['footerblock'] = 'Footer Block';
$string['title'] = 'Title';
$string['footerbtitle_desc'] = 'Please give the footer block title either language key or text.For ex: lang:display or Display';
$string['footerbtitle2default'] = 'Quick Links';
$string['footerbtitle3default'] = 'Follow Us';
$string['footerbtitle4default'] = 'Contact';
$string['footerblink'] = 'Footer Block Link';
$string['footerblink2default'] = 'lang:aboutus|http://www.example.com/about-us.php
lang:termsofuse|http://www.example.com/terms-of-use.php
lang:faq|http://www.example.com/faq.php
lang:support|http://www.example.com/support.php
lang:contact|http://www.example.com/contact.php';
$string['footerblink_desc'] = 'You can configure a Footer Block Links here to be shown by themes. Each line consists of some menu text either language key or text, a link URL (optional),separated by pipe characters.For example:
<pre>
lang:moodlecommunity|https://moodle.org
Moodle Support|https://moodle.org/support
</pre>';
$string['medianame1'] = 'Facebook';
$string['medianame2'] = 'Twitter';
$string['medianame3'] = 'Google Plus';
$string['medianame4'] = 'Pinterest';

$string['mediaicon1'] = 'fa-facebook-f';
$string['mediaicon2'] = 'fa-twitter';
$string['mediaicon3'] = 'fa-google-plus';
$string['mediaicon4'] = 'fa-pinterest-p';
$string['default_color'] = 'Default color scheme';
$string['color_schemes_heading'] = 'Color Schemes';
$string['promotedtitledefault'] = 'Recommended courses';
$string['promotedtitledesc'] = 'Meet the recommended course Oklass';
$string['pcourseenable'] = 'Enable Recommended courses';
$string['login'] = 'Login';
$string['link'] = 'Link';
$string['text'] = 'Text';
$string['icon'] = 'Icon';
$string['loginheader'] = 'Login into your account';
$string['marketingspotsheading'] = 'Marketing Spots';
$string['marketingspot'] = 'Marketing Spot';
$string['marketingspot1desc'] = 'Enter the Marketing Spot 1 block content either language key or text here.For ex: lang:display or Display';
$string['marketingspot2desc'] = 'Enter the Marketing Spot 2 block content either language key or text here.For ex: lang:display or Display';
$string['mspottitledesc'] = 'Enter the Marketing Spot Title either language key or Text.For ex: lang:display or Display';
$string['mspotdescdesc'] = 'Enter the Marketing Spot Description either language key or Text.For ex: lang:display or Display';
$string['mspot1titledefault'] = 'Multilingual';
$string['mspot1descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot2titledefault'] = 'Online Study';
$string['mspot2descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot3titledefault'] = 'Community Support';
$string['mspot3descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot4titledefault'] = 'Responsive Design';
$string['mspot4descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['faicondesc'] = 'Enter the name of the icon you wish to use.  List is <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target="_new">here</a>. Just enter what is after the "fa-".';
$string['slideurltext'] = 'Slide link text';
$string['slideurltextdesc'] = 'Enter the target destination of the slide\'s image link text,
either language key or Text.For ex: lang:display or Display';
$string['termsofuse'] = 'Terms of use';
$string['faq'] = 'FAQ';
$string['support'] = 'Support';
$string['copyright'] = '<p class="text-center">Copyright &copy; 2015 - Developed by 
            <a href="http://www.nephzat.com/">Nephzat.com</a>.Powered by <a href="https://moodle.org">Moodle</a></p>';
$string['footeremail'] = 'E-mail:';
$string['irregularcourse'] = '비교과과정';
$string['regularcourse'] = '교과과정';

//내 강좌
$string['regular'] = '교과';
$string['irregular'] = '비교과';
$string['elearning'] = 'SNUT 커뮤니티';
$string['teacher'] = '교수';
$string['teachername'] = '교수명';
$string['coursename'] = '강좌명';
$string['all'] = '전체';
$string['year'] = '년';
$string['term'] = '학기';
$string['star_score'] = '별점';
$string['year_term'] = '년도 학기';
$string['application'] = '신청 내역';


//회원가입,로그인
$string['signup'] = '회원가입';
$string['login'] = '로그인';
$string['findid'] = '아이디찾기';
$string['findpw'] = '비밀번호 찾기';
$string['findidpw'] = '아이디/비밀번호 찾기';
$string['saveid'] = '아이디저장';

$string['learningstats'] = '학습현황';
$string['progressstats'] = '진도현황';
$string['frontext'] = '배움과 나눔이 있는 OKlass 온라인 강의에 지금 참여하세요!';

$string['coursemove'] = '강좌로 이동';
$string['course'] = '강좌';
$string['notice2'] = '알림';
$string['please_login'] = '로그인을 하셔야 볼 수 있습니다.';
$string['showall'] = '모두보기';
$string['message'] = '메시지';
$string['showallmessage'] = '모든 메시지 보기';
//메인페이지
$string['openingrecommendedcourse'] = '열린 추천 강좌';
$string['regulartitle'] = '정규/비정규';
$string['COOC'] = 'SNUT 커뮤니티';
$string['OCW'] = 'OCW';
$string['MOOC'] = 'MOOC';
$string['regularcontent'] = 'OKlass LMS는 ERP시스템에서 연계된 정규과정 자동개설 기능 및 교수자/기관의 소요에 따른 비정규과정 추가 개설기능이 가능합니다.';
$string['cooccontent'] = 'OKlass 1.5는 학습관리시스템(LMS)내에 자유롭게 커뮤니티를 생성하고 학습공동체를 구성할 수 있는 기능을 제공합니다.';
$string['ocwcontent'] = '오픈코스웨어를 통해 전 세계 인터넷 사용자는 누구든지 자신이 원하는 수업 자료들을 공개적으로 접속, 활용합니다.';
$string['mooccontent'] = '온라인을 통해 누구나,어디서나 원하는 강의를 무료로 들을수 있는 온라인 공개강좌 서비스 입니다.';
//footer
$string['footerbtitle'] = '관련사이트';
$string['homepage'] = '홈페이지';
$string['manage'] = '유지보수';
$string['contact'] = '연락처';
$string['address'] = '서울특별시 서초구 방배동 454-9번지 동운빌딩 3층 (주)지노테크';

$string['summer'] = '여름학기';
$string['winter'] = '겨울학기';

$string['common'] = '공통';
$string['progressdate'] = '진행일시';
$string['completioncriteria'] = '이수기준';
$string['overallprogressrate'] = '전체 진도율';
$string['courselist'] = '강의목록';
$string['notice'] = '강좌공지';
$string['quiz'] = '퀴즈';
$string['reference'] = '자료실';
$string['qna'] = '강좌 QnA';
$string['coursereview'] = '강좌 후기';
$string['courseintro'] = '강좌 소개';
$string['courselist'] = '강의 목록';
$string['facebooklogin'] = 'Facebook 계정으로 로그인';
$string['googlelogin'] = 'Google 계정으로 로그인';
$string['zalologin'] = 'zalo 계정으로 로그인';

$string['tplan'] = '교안';
$string['coursecompletesetting'] = '강좌 이수 설정';
$string['quizbank'] = '문제은행';
$string['learningstatus'] = '학습현황';
$string['participantlist'] = '참여자 목록';


$string['myinfo'] = '내정보';
$string['mycoupons'] = '내 쿠폰';
$string['mycourse'] = '나의 강좌';
$string['withdrawal'] = '회원탈퇴';

$string['search_txt'] = '강좌명, 교재명 검색';
$string['search'] = '검색';
$string['learning'] = '학습하기';
$string['recommendedcourse'] = '인기강좌';
$string['recommendedcourse1'] = '추천강좌';
$string['package'] = '한국어 / 토픽 추천 패키지';
$string['recommended'] = '추천';


$string['sir'] = '님';
$string['viewmycourse'] = '수강중인 강좌 보기';
$string['chargelecture'] = '담당 강좌 보기';
$string['getlogin'] = '로그인 후 이용가능합니다.';
$string['customerservice'] = '고객센터';
$string['job'] = '구인구직';
$string['facebook'] = '페이스북 ';
$string['youtube'] = '유튜브 ';
$string['counseling'] = '상담';
$string['logout'] = '로그아웃';
$string['book'] = '교재';
$string['board'] = '게시판';
$string['community'] = '커뮤니티';
$string['service'] = '서비스 소개';
$string['terms'] = '이용약관';
$string['customerservice2'] = '고객지원';
$string['terms2'] = '개인정보보호방침';
$string['footer'] = '<p>회사명 : Visang Vietnam Education Company Limited</p>
            <p>본사 : 베트남 하노이시 남 투림 지구 My Dinh 2 Ward Le Duc Tho Street FLC Landmark Tower 2 층</p>
            <p>2020 년 1 월 14 일 하노이시 기획 투자국의 세금 코드 : 0109066143 </p>  <p>대표자 : 이영근</p>
            <p>전화 : 0243-6886-333 | 이메일 : visang@masterkorean.vn</p>
            <p class="copy">Copyright © VISANG Education Group Vietnam Company</p>';
$string['ftaddress']='주소 : 베트남 하노이시 남 투림 지구 My Dinh 2 Ward Le Duc Tho Street FLC Landmark Tower 2 층';
$string['fttel']='전화 : 0243-6886-333';
$string['ftmail']=' 이메일 : visang@masterkorean.vn';
$string['ftcopyright']='Copyright © VISANG Education Group Vietnam Company';
$string['getlogin2'] = '회원가입/로그인 후 맞춤 채용정보 추천받으세요.';
$string['viewmasterkorea'] = 'Master Korean의 다양한<br />콘텐츠를 경험하세요.';
$string['qna2'] = '1:1 상담<br />문의';
$string['jobinfo'] = '인기 채용정보';
$string['jobinfologin'] = '추천 채용정보';
$string['notice'] = '공지';
$string['close'] = '닫기';
$string['things'] = '개';
$string['terms3'] = '서비스운영규정';
$string['korean'] = '한국어';
$string['gomanagement'] = '관리자 페이지로 이동';
$string['logout:confirm'] = '로그아웃 하시겠습니까?';
$string['feedback'] = 'Master Korean 온라인 학습 사이트에서 학습 경험에 대해 알려주세요!';
$string['fbfullname'] = '성명';
$string['fbyouremail'] = '귀하의 이메일';
$string['fbphonenumber'] = '전화번호';
$string['fbbutton'] = '피드백 보내기';
$string['fbnotice'] = '(Master Korean은 문의에 1~2시간 이내에 답변 해드립니다. Master Korean을 동행 해 주셔서 감사합니다.)';
$string['fbnoticename'] = '당신의 이름을 입력 해주세요';
$string['fbnoticeemail'] = '이메일을 입력하세요';
$string['fbnoticephone'] = '전화 번호를 입력하세요';
$string['fbnoticesuccess'] = '이메일이 성공적으로 전송되었습니다';
$string['fbnoticenosuccess'] = '이메일 전송 실패';
$string['feedback'] = 'Master Korean 온라인 학습 사이트에서 학습 경험에 대해 알려주세요!';
$string['fbfullname'] = '성명';
$string['fbyouremail'] = '귀하의 이메일';
$string['fbphonenumber'] = '전화 번호';
$string['fbbutton'] = '피드백 보내기';
$string['fbnotice'] = '(Master Korean은 시스템에서 학습 과정에 대한 질문에 대해 1 ~ 2 시간 이내에 답변을 드릴 것입니다. 저희 서비스를 이용해 주셔서 감사합니다)';
$string['account_exist'] = 'ID 없으세요?';

$string['visang_korean'] = '비상 한국어';
$string['exam_practice'] = '문제풀이';
$string['package_of_korean'] = '한국어패키지';
$string['business_korean'] = '비즈니스';

$string['topik_course'] = '[-30%] TOPIK II 실전모의고사 (43강)';
$string['business_course'] = '[-30%] 비즈니스 한국어 [천성옥 강사] - 24강';
$string['beginner1_course'] = '[-30%] 한국어 초급 1 (100강)';
$string['beginner2_course'] = '[-30%] 한국어 초급 2 (100강)';
$string['intermediate1_course'] = '[-30%] 한국어 중급 1 (80강)';
$string['intermediate2_course'] = '[-30%] 한국어 중급 2 (80강)';
