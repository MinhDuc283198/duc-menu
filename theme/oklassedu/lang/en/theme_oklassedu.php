<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_oklassedu', language 'en'
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>oklassedu</h2>
<p><img class=img-polaroid src="oklassedu/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>oklassedu is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
<h3>Parents</h3>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
<h3>Theme Credits</h3>
<p>Authors: Bas Brands, David Scotson, Mary Evans<br>
Contact: bas@sonsbeekmedia.nl<br>
Website: <a href="http://www.basbrands.nl">www.basbrands.nl</a>
</p>
<h3>Report a bug:</h3>
<p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
<h3>More information</h3>
<p><a href="oklassedu/README.txt">How to copy and customise this theme.</a></p>
</div></div>';

$string['pluginname'] = 'oklassedu';
$string['configtitle'] = 'oklassedu';

//모바일 헤더 타이틀
$string['mtitle'] = 'course';

//메뉴
$string['menu:mypage'] = 'My Page';
$string['menu:mypage02'] = 'my page';
$string['menu:dashboard'] = 'My Course';
$string['menu:filemanage'] = 'File management';
$string['menu:coursemanage'] = 'Course management';
$string['menu:lcmsmanage'] = 'Contents management';
$string['menu:schedule'] = 'Schedule';
$string['menu:message'] = 'Message';
$string['menu:coursenotification'] = 'Notification';
$string['menu:courseboard'] = 'Course board';
$string['menu:evaluation'] = 'Evaluation';
$string['menu:gradeview'] = 'Grade view';
$string['menu:editprofile'] = 'Edit profile';

$string['menu:regular'] = 'Regular courses';
$string['menu:irregular'] = 'Irregular courses';
$string['menu:elearning'] = 'UST Community';
$string['menu:mycourse'] = 'My course';
$string['menu:auditenrol'] = 'Enrol audit';
$string['menu:courseenrol'] = 'Enrol course';
$string['menu:completionview'] = 'Completion view';
$string['menu:addcourse'] = 'Create course';
$string['menu:allcourse'] = 'All courses';
$string['menu:assistant'] = 'assistant/Enrol audit';

$string['menu:guide'] = 'Guide';
$string['menu:notice'] = 'Notice';
$string['menu:qna'] = 'Q&AMP;A';
$string['menu:faq'] = 'FAQ';
$string['menu:reference'] = 'Reference';
$string['menu:manual'] = 'Manual';

//settings
$string['sitelogo'] = 'Site logo';
$string['sitename'] = 'Display site name along with small logo';
$string['sitenamedesc'] = 'If there is no small logo, the site name is always displayed in the navigation bar. If a small logo is set, it may be displayed with or without the site name.';
$string['smalllogo'] = 'Small logo';
$string['smalllogodesc'] = 'The small logo is displayed in the navigation bar. If there is a header logo for the front page and login page, the small logo is not displayed on these pages.';

$string['connectus'] = 'Connect with us';
$string['contact'] = 'Contact';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';
$string['defaultaddress'] = '308 Negra Narrow Lane, Albeeze, New york, 87104';
$string['defaultemailid'] = 'info@example.com';
$string['defaultphoneno'] = '(000) 123-456';
$string['emailid'] = 'Email';
$string['fburl'] = 'Facebook';
$string['fburl_default'] = 'https://www.facebook.com/yourfacebookid';
$string['fburldesc'] = 'The Facebook url of your organisation.';
$string['pcourses'] = 'Recommend Courses';
$string['pcoursesdesc'] = 'Please give the recommended courses id should separated by comma.';
$string['promotedcoursesheading'] = 'Recommended Courses';
$string['footbgimg'] = 'Background Image';
$string['footbgimgdesc'] = 'Background Image size should be following size 1345 X 517';
$string['footerheading'] = 'Footer Blocks';
$string['footerlogo'] = 'Footer Logo';
$string['footerlogodesc'] = 'Please upload your custom footer logo here if you want to add it to the footer.
                       <br>The image should be 80px high and any reasonable width (minimum:205px) that suits.';
$string['footnote'] = 'Footnote';
$string['footnotedefault'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and tronic typesetting, sheets taining Lorem Ipsum passages.';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.Please give the either language key or text.For ex: lang:display or Display';
$string['frontpageheading'] = 'Front page';
$string['gpurl'] = 'Google+';
$string['gpurl_default'] = 'https://www.google.com/+yourgoogleplusid';
$string['gpurldesc'] = 'The Google+ url of your organisation.';
$string['headerheading'] = 'Header';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.
                       <br>The image should be 50px high and any reasonable width (minimum:235px) that suits.';
$string['numberofslides'] = 'Number of slides';
$string['numberofslides_desc'] = 'Number of slides on the slider.';
$string['numberoftmonials'] = 'Number of Testimonials';
$string['numberoftmonials_desc'] = 'Number of Testimonials on the Home Page.';
$string['phoneno'] = 'Phone No';
$string['pinurl'] = 'Pinterest';
$string['pinurl_default'] = 'https://in.pinterest.com/yourpinterestname/';
$string['pinurldesc'] = 'The Pinterest url of your organisation.';
$string['knowmore'] = 'Know More';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['signup'] = 'Sign up';
$string['slidecaption'] = 'Slide caption';
$string['slidecaptiondefault'] = 'Moodle Learning Management System';
$string['slidecaptiondesc'] = 'Enter the caption text to use for the slide';
$string['slideimage'] = 'Slide image';
$string['slideimagedesc'] = 'The image should be 1366px X 385px.';
$string['slideno'] = 'Slide {$a->slide}';
$string['slidenodesc'] = 'Enter the settings for slide {$a->slide}.';
$string['slideshowdesc'] = 'This creates a slide show of up to twelve slides for you to promote important elements of your site.  The show is responsive where image height is set according to screen size.If no image is selected for a slide, then the default_slide images in the pix folder is used.';
$string['slideshowheading'] = 'Home Page Slider';
$string['slideshowheadingsub'] = 'Slide show for the front page';
$string['slideurl'] = 'Slide link';
$string['slideurldesc'] = 'Enter the target destination of the slide\'s image link';
$string['toggleslideshow'] = 'Slide show display';
$string['toggleslideshowdesc'] = 'Choose if you wish to hide or show the slide show.';
$string['twurl'] = 'Twitter';
$string['twurl_default'] = 'https://twitter.com/yourtwittername';
$string['twurldesc'] = 'The Twitter url of your organisation.';
$string['themegeneralsettings'] = 'General';
$string['patternselect'] = 'Site Color Scheme';
$string['patternselectdesc'] = 'Select the color scheme you want to have for your site.';
$string['colorscheme'] = 'Color Scheme';
$string['footerblock'] = 'Footer Block';
$string['title'] = 'Title';
$string['footerbtitle_desc'] = 'Please give the footer block title either language key or text.For ex: lang:display or Display';
$string['footerbtitle2default'] = 'Quick Links';
$string['footerbtitle3default'] = 'Follow Us';
$string['footerbtitle4default'] = 'Contact';
$string['footerblink'] = 'Footer Block Link';
$string['footerblink2default'] = 'lang:aboutus|http://www.example.com/about-us.php
lang:termsofuse|http://www.example.com/terms-of-use.php
lang:faq|http://www.example.com/faq.php
lang:support|http://www.example.com/support.php
lang:contact|http://www.example.com/contact.php';
$string['footerblink_desc'] = 'You can configure a Footer Block Links here to be shown by themes. Each line consists of some menu text either language key or text, a link URL (optional),separated by pipe characters.For example:
<pre>
lang:moodlecommunity|https://moodle.org
Moodle Support|https://moodle.org/support
</pre>';
$string['medianame1'] = 'Facebook';
$string['medianame2'] = 'Twitter';
$string['medianame3'] = 'Google Plus';
$string['medianame4'] = 'Pinterest';

$string['mediaicon1'] = 'fa-facebook-f';
$string['mediaicon2'] = 'fa-twitter';
$string['mediaicon3'] = 'fa-google-plus';
$string['mediaicon4'] = 'fa-pinterest-p';
$string['default_color'] = 'Default color scheme';
$string['color_schemes_heading'] = 'Color Schemes';
$string['promotedtitledefault'] = 'Recommended courses';
$string['promotedtitledesc'] = 'Meet the recommended course Oklass';
$string['pcourseenable'] = 'Enable Recommended courses';
$string['login'] = 'Login';
$string['link'] = 'Link';
$string['text'] = 'Text';
$string['icon'] = 'Icon';
$string['loginheader'] = 'Login into your account';
$string['marketingspotsheading'] = 'Marketing Spots';
$string['marketingspot'] = 'Marketing Spot';
$string['marketingspot1desc'] = 'Enter the Marketing Spot 1 block content either language key or text here.For ex: lang:display or Display';
$string['marketingspot2desc'] = 'Enter the Marketing Spot 2 block content either language key or text here.For ex: lang:display or Display';
$string['mspottitledesc'] = 'Enter the Marketing Spot Title either language key or Text.For ex: lang:display or Display';
$string['mspotdescdesc'] = 'Enter the Marketing Spot Description either language key or Text.For ex: lang:display or Display';
$string['mspot1titledefault'] = 'Multilingual';
$string['mspot1descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot2titledefault'] = 'Online Study';
$string['mspot2descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot3titledefault'] = 'Community Support';
$string['mspot3descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['mspot4titledefault'] = 'Responsive Design';
$string['mspot4descdefault'] = 'This is Photoshop\'s version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.';
$string['faicondesc'] = 'Enter the name of the icon you wish to use.  List is <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target="_new">here</a>. Just enter what is after the "fa-".';
$string['slideurltext'] = 'Slide link text';
$string['slideurltextdesc'] = 'Enter the target destination of the slide\'s image link text,
either language key or Text.For ex: lang:display or Display';
$string['termsofuse'] = 'Terms of use';
$string['faq'] = 'FAQ';
$string['support'] = 'Support';
$string['copyright'] = '<p class="text-center">Copyright &copy; 2015 - Developed by 
            <a href="http://www.nephzat.com/">Nephzat.com</a>.Powered by <a href="https://moodle.org">Moodle</a></p>';
$string['footeremail'] = 'E-mail:';
$string['irregularcourse'] = 'Iregular Course';
$string['regularcourse'] = 'Regular Course';

//내 강좌
$string['regular'] = 'Regular';
$string['irregular'] = 'Irregular';
$string['elearning'] = 'UST Community';
$string['teacher'] = 'Professor';
$string['teachername'] = 'Professor name';
$string['coursename'] = 'Course name';
$string['all'] = 'All';
$string['year'] = 'Year';
$string['term'] = 'Semester';
$string['star_score'] = 'Scope';
$string['year_term'] = 'year_term';
$string['application'] = 'application';


//회원가입,로그인
$string['signup'] = 'Sign up';
$string['login'] = 'Login';
$string['findid'] = 'Find ID';
$string['findpw'] = 'Find PW';
$string['findidpw'] = 'Find ID/PW';
$string['saveid'] = 'Save ID';

$string['learningstats'] = 'Learning Stats';
$string['progressstats'] = 'Progress Stats';
$string['frontext'] = 'Let \'s join the OKlass online lecture now!';

$string['coursemove'] = 'course move';
$string['course'] = 'course';
$string['notice2'] = 'notice';
$string['please_login'] = 'You must be logged in to view.';
$string['showall'] = 'show all';
$string['message'] = 'message';
$string['showallmessage'] = 'show all message';

$string['openingrecommendedcourse'] = 'Opening Recommended Course';
$string['regulartitle'] = 'Regular / irregular';
$string['COOC'] = 'UST Community';
$string['OCW'] = 'OCW';
$string['MOOC'] = 'MOOC';
$string['regularcontent'] = 'OKlass LMS is capable of automatic opening of regular course linked with ERP system and additionally opening of irregular course according to the requirement of the instructor / institution.';
$string['cooccontent'] = 'OKlass 1.5 provides the ability to create communities freely within the Learning Management System (LMS) and to organize a learning community.';
$string['ocwcontent'] = 'Through OpenCourseWare, anyone around the world can openly access and use the materials they want.';
$string['mooccontent'] = 'It is an online open lecture service where you can listen to lectures you want anywhere, anytime, through online.';
//footer
$string['footerbtitle'] = 'Related site';
$string['homepage'] = 'Hompage';
$string['manage'] = 'Maintenance';
$string['contact'] = 'Contact';
$string['address'] = '3rd Floor, Dongun Building, 454-9 Bangbae-dong, Seocho-gu, Seoul';

$string['summer'] = 'summer';
$string['winter'] = 'winter';

$string['common'] = 'common';
$string['progressdate'] = 'progressdate';
$string['completioncriteria'] = 'completioncriteria';
$string['overallprogressrate'] = 'overallprogressrate';
$string['courselist'] = 'courselist';
$string['notice'] = 'course notice';
$string['quiz'] = 'quiz';
$string['reference'] = 'reference';
$string['qna'] = ' course QnA';
$string['coursereview'] = 'course review';
$string['courseintro'] = 'course intro';
$string['courselist'] = 'course list';
$string['facebooklogin'] = 'Login to Facebook account';
$string['googlelogin'] = 'Login to google account';
$string['zalologin'] = 'Login to zalo account';

$string['tplan'] = 'Teaching plan';
$string['coursecompletesetting'] = 'Course completion setting';
$string['quizbank'] = 'quizbank';
$string['learningstatus'] = 'learningstatus';
$string['participantlist'] = 'participantlist';


$string['myinfo'] = 'My Info';
$string['mycoupons'] = 'My coupons';
$string['mycourse'] = 'my course';
$string['withdrawal'] = 'Membership Withdrawal';

$string['search_txt'] = 'Course name, textbook search';
$string['search'] = 'search';
$string['learning'] = 'Learning';
$string['recommendedcourse'] = 'Popular Courses';
$string['recommendedcourse1'] = 'Recommended course';
$string['package'] = 'Korean Recommended Package';
$string['recommended'] = 'Recommended';

$string['sir'] = 'sir';
$string['viewmycourse'] = 'view my course';
$string['chargelecture'] = 'chargelecture';
$string['getlogin'] = 'Available after login';
$string['customerservice'] = 'customer service';
$string['job'] = 'job';
$string['facebook'] = 'facebook ';
$string['youtube'] = 'youtube';
$string['counseling'] = 'counseling';
$string['logout'] = 'Logout';
$string['book'] = 'book';
$string['board'] = 'board';
$string['community'] = 'community';
$string['service'] = 'service';
$string['terms'] = 'terms';

$string['customerservice2'] = 'Customer Support';
$string['terms2'] = 'Privacy policy';
$string['footer'] = '<p>Company name: Visang Vietnam Education Company Limited</p>
            <p>Headquarters: 2nd Floor, FLC Landmark Tower, Le Duc Tho Street, My Dinh 2 Ward, Nam Tu Liem District, Hanoi City, Vietnam</p>
            <p>Tax code: 0109066143 by Department of Planning and Investment of Hanoi City on January 14, 2020 </p>
            <p>Representative: Mr. Lee Young Geun</p>
            <p>Phone: 0243-6886-333 | E-mail: visang@masterkorean.vn</p>
            <p class="copy">Copyright © VISANG Education Group Vietnam Company</p>';
$string['ftaddress']='Address : 2nd Floor, FLC Landmark Tower, Le Duc Tho Street, My Dinh 2 Ward, Nam Tu Liem District, Hanoi City, Vietnam';

$string['fttel']='Phone : 0243-6886-333';
$string['ftmail']='E-mail : visang@masterkorean.vn';
$string['ftcopyright']='Copyright © VISANG Education Group Vietnam Company';




$string['getlogin2'] = 'Sign up/log in and get a recommendation for customized job information.';
$string['viewmasterkorea'] = 'Experience the various contents <br /> of Master Korean.';
$string['qna2'] = '1:1 qna<br />Inquiry';
$string['jobinfo'] = 'Popular recruitment information';
$string['jobinfologin'] = 'Recommended Jobs';
$string['notice2'] = 'Alarm';
$string['close'] = 'close';
$string['things'] = 'things';
$string['terms3'] = 'Service Usage Policy';
$string['korean'] = 'korean';
$string['gomanagement'] = 'Go to Admin page';
$string['logout:confirm'] = 'Do you want to log out?';
$string['feedback'] = 'Please tell us about your learning experience on the Master Korean online learning site!';
$string['fbfullname'] = 'Fullname';
$string['fbyouremail'] = 'Your email';
$string['fbphonenumber'] = 'Your phone number';
$string['fbbutton'] = 'Send feedback';
$string['fbnotice'] = '(Master Korean will answer your questions in 1 to 2 hours. Thank you.)';
$string['fbnoticename'] = 'Please enter your name';
$string['fbnoticeemail'] = 'Please enter your email';
$string['fbnoticephone'] = 'Please enter your phone';
$string['fbnoticesuccess'] = 'Send email success';
$string['fbnoticenosuccess'] = 'Email sending failed';
$string['account_exist'] = 'Do not have an account?';
$string['visang_korean'] = 'Visang Korean';
$string['exam_practice'] = 'Exam Practice';
$string['package_of_korean'] = 'Package of Korean';
$string['business_korean'] = 'Business Korean';


$string['feedback'] = 'Please tell us about your learning experience on the Master Korean online learning site!';
$string['fbfullname'] = 'Fullname';
$string['fbyouremail'] = 'Your email';
$string['fbphonenumber'] = 'Your phone number';
$string['fbbutton'] = 'Send feedback';
$string['fbnotice'] = '(Master Korean will reply within 1-2 hours to answer questions about your learning process on the system. Thank you for using our service)';
$string['topik_course'] = '[-30%] TOPIK II - Exam Practice (43 Lessons)';
$string['business_course'] = '[-30%] Business Korean [Teacher Cheon Seong Ok] - 24 Lessons';
$string['beginner1_course'] = '[-30%] Korean - Beginner 1 (100 Lessons)';
$string['beginner2_course'] = '[-30%] Korean - Beginner 2 (100 Lessons)';
$string['intermediate1_course'] = '[-30%] Korean - Intermediate 1 (80 Lessons)';
$string['intermediate2_course'] = '[-30%] Korean - Intermediate 2 (80 Lessons)';
