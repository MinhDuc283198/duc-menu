<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's Clean theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_oklassedu
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$THEME->name = 'oklassedu';

/////////////////////////////////
// The only thing you need to change in this file when copying it to
// create a new theme is the name above. You also need to change the name
// in version.php and lang/en/theme_oklassedu.php as well.
//////////////////////////////////
//
$THEME->doctype = 'html5';
$THEME->parents = array('bootstrapbase');
$THEME->sheets = array('lightslider', 'oklassedu', 'sub', 'custom', 'common', 'style.old', 'style', 'media', 'slick', 'slick-theme');
$THEME->supportscssoptimisation = false;
$THEME->yuicssmodules = array();
$THEME->enable_dock = true;
$THEME->editor_sheets = array();

$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->csspostprocess = 'theme_oklassedu_process_css';

//if ($no) {
//    $THEME->sheets[] = 'color_scheme-'.$no;
//} else {
//    $THEME-> sheets[] = 'color_scheme-default';
//}
if ($CFG->lotte_wwwroot == $CFG->wwwroot) {
    $join_link = 'lotte_join.php';
} else {
    $join_link = 'join.php';
}

$THEME->layouts = array(
    // The site home page.
    'frontpage' => array(
        'file' => isPC() ?  'frontpage.php' : 'm_frontpage.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'popup' => array(
        'file' => 'popup.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'login' => array(
        'file' => 'login.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'join' => array(
        'file' => $join_link,
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'join02' => array(
        'file' => 'join02.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'forgotpw' => array(
        'file' => 'forgotpw.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'okocw' => array(
        'file' => 'columns1.php',
        'regions' => array(),
        //'defaultregion' => 'side-pre',
        'options' => array('nonavbar' => true),
    ),
    'okmooc' => array(
        'file' => 'columns1.php',
        'regions' => array(),
        //'defaultregion' => 'side-pre',
        'options' => array('nonavbar' => true),
    ),
    'mydashboard' => array(
        'file' => 'edu.php',
        'regions' => array('side-post'),
        'defaultregion' => 'side-post',
        'options' => array('nonavbar' => true, 'nocourseheaderfooter' => true),
    ),
    // Standard layout with blocks, this is recommended for most pages with general information.
    'standard' => array(
        'file' => 'incourse.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'mypage' => array(
        'file' => 'mypage.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    // Main course page.
    'edu' => array(
        'file' => 'edu.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    // Main course page.
    'edu-full' => array(
        'file' => 'edu-full.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'edu02' => array(
        'file' => 'edu02.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'edu03' => array(
        'file' => 'edu03.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'course' => array(
        'file' => 'course.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    // Part of course, typical for modules - default page layout if $cm specified in require_login().
    'incourse' => array(
        'file' => 'incourse.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    // Part of course, typical for modules - default page layout if $cm specified in require_login().
    'admin' => array(
        'file' => 'admin.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'report' => array(
        'file' => 'incourse.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'mypublic' => array(
        'file' => 'incourse.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'coursecategory' => array(
        'file' => 'incourse.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'base' => array(
        'file' => 'incourse.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'adminmanagement' => array(
        'file' => 'adminmanagement.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    'coupon' => array(
        'file' => 'coupon.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
);

/** List of javascript files that need to be included on each page */
$THEME->javascripts = array('common', 'script', "jquery-ui.min", 'slick.min', 'dragscroll');
$THEME->javascripts_footer = array('');
