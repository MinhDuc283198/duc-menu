<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require('../config.php');
require_once('lib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->dirroot . '/user/editlib.php');
require_once($CFG->dirroot . '/user/profile/lib.php');

if (isloggedin()) {
    redirect($CFG->wwwroot);
}

$token = optional_param('token', '', PARAM_TEXT);
$username = optional_param('username', '', PARAM_TEXT);
$password = optional_param('password', '', PARAM_TEXT);
$autologin = optional_param('autologin', 0, PARAM_INT);
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('login');
echo $OUTPUT->header();

$templatecontext = [
    'output' => $OUTPUT
];

//username 과 비밀번호가 있을때 로그인 시켜줌
if (!empty($username) && !empty($password)) {

    if ($user = authenticate_user_login($username, $password)) {
        // 토큰이 있으면 업데이트 없으면 insert진행 
        $tokeninform = $DB->get_record('lmsdata_pushtoken', array('token' => $token));
        if ($tokeninform) {
            $tokeninform->autologin = $autologin;
            $tokeninform->username = $username;
            $DB->update_record('lmsdata_pushtoken', $tokeninform);
        } else {
            $inform = new stdClass();
            $inform->token = $token;
            $inform->username = $username;
            $inform->autologin = $autologin;
            $tokenid = $DB->insert_record('lmsdata_pushtoken', $inform);
        }
        complete_user_login($user);
        echo "<script>location.href='" . $CFG->wwwroot . "/index.php'</script>";
    } else {
//            redirect($CFG->wwwroot.'/login/mobilelogin.php', '비밀번호 틀렸어');
        echo '<script>alert("ID 또는 비밀번호가 정확하지 않습니다."); localStorage.removeItem("autologinval");</script>';
    }
}
?>
<!--<form class="login-area form none" id="login_form" action="<?php echo $CFG->wwwroot . '/login/mobilelogin.php'; ?>" method="post">
    <div class="lg-top">
        <img src="/theme/oklassedu/pix/images/mobile_logo_white.png" width="55" alt="한양브레인소프트">
        <h5>한양브레인소프트 로그인</h5>
        <p>한양브레인소프트 서비스를 이용하시려면 <br/> 아이디와 비밀번호를 입력하시기 바랍니다.</p>
        <input type="hidden" name ="token" value ="">
        <input type="text" class="id" name="username" id="username" placeholder="ID" autocomplete="false" autocapitalize="off" value="<?php p($username) ?>"/>
        <input type="password" class="pw" name="password" id="password" placeholder="PW"  autocomplete="new-password"/>
        <input type="submit" class="btn white w100" id="login_submit_button" value="LOGIN" />
        <div class="mg-bt10">
            <input type="checkbox" name="autologin" id ="autologin" value="1"/>
            <label for="autologin">자동 로그인</label>
            <a href="/login/forgot_password.php" style ="color: #fff;font-size: 13px;font-weight: 300;padding:20px">아이디/비밀번호 찾기</a>
        </div>
        <p class="text-left"><br><br>
            ※ 아이디는 학번/사번이며, 비밀번호는 초기값 생년월일 앞6자리입니다. 로그인 후 비밀번호를 반드시 변경하시기 바랍니다. 
        </p>
    </div>
</form>-->



<script>
    $(".login-area.form").show();
    var autologinval = localStorage.getItem('autologinval');
    var usernamecookie = localStorage.getItem('username');
    var passwordcookie = localStorage.getItem('password');
    var token = localStorage.getItem('pushtoken');

    if (autologinval) {
        $('#autologin').attr('checked', true);

        //로그인 처리 해주면 될거같음
        if (usernamecookie && passwordcookie) {
            $('#username').val(usernamecookie);
            $('#password').val(passwordcookie);
            $('input[name=token]').val(token);
            $('#login_form').submit();
        }
    }

    function setMessage(token, userid) {
        localStorage.setItem('pushtoken', token);
        $.ajax({
            url: '<?php echo $CFG->wwwroot . "/login/pushtokensave.php" ?>',
            method: 'POST',
            dataType: "json",
            data: {
                pushtoken: token,
                username: userid
            },
            success: function (data) {
            }
        });
    }

    $('#login_form').submit(function () {
        var username = $('#username').val();
        var password = $('#password').val();
        var pushtoken = localStorage.getItem('pushtoken');

        if (!username.trim() || username.length == 0) {
            alert('아이디를 입력하세요');
            $('#username').focus();
            return false;
        }
        if (!password.trim() || password.length == 0) {
            alert('패스워드를 입력하세요');
            $('#password').focus();
            return false;
        }

        if (pushtoken) {
            $('input[name=token]').val(pushtoken);
        }

        //로그인시 로컬에 쿠키 저장
        if ($('#autologin').is(":checked")) {
            localStorage.setItem('autologinval', $('#autologin').val());
            localStorage.setItem('username', username);
            localStorage.setItem('password', password);
        }
        $('#login_form').submit();
        return true;
    });

//localStorage.setItem('username', $('#username').val());  
//localStorage.setItem('password', $('#password').val());
//localStorage.removeItem('test2');
//console.log(localStorage);
//console.log(localStorage.removeItem('test2'));
</script>
<?php
echo $OUTPUT->footer();
?>


