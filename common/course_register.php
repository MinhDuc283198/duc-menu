<style>
.content-34 {
    margin-top: 34px;
    margin-bottom: 34px;
}
.form-bg {
    background: #485BC6;
    padding: 34px;
    color: #fff;
    max-width: 400px;
    margin: auto;
    margin-top: 34px;
    border-radius: 14px;
}
.text-center {
    text-align: center;
}
.form-group {
    margin-bottom: 1rem;
}
.form-group label {
    margin-bottom: 8px;
}
.form-control {
    display: block;
    width: 100% !important;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.vs-btn-primary {
    color: #fff !important;
    background-color: #007bff !important;
    border-color: #007bff !important;
}
.vs-btn {
    display: inline-block !important;
    font-weight: 400 !important;
    text-align: center !important;
    white-space: nowrap !important;
    vertical-align: middle !important;
    -webkit-user-select: none !important;
    -moz-user-select: none !important;
    -ms-user-select: none !important;
    user-select: none !important;
    border: 1px solid transparent !important;
    padding: .375rem .75rem !important;
    font-size: 1rem !important;
    line-height: 1.5 !important;
    border-radius: .25rem !important;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out !important;
}
.form-bg h4 {
    font-size: 1.5rem;
    color: #fff;
}

.form-bg p {
    color: #fff;
    margin-top: 20px;
    margin-bottom: 20px;
}
</style>

<div class="container">
    <div class="form-bg content-34">
        <h4 class="text-center"><?php echo l('form.register_form_title') ?></h4>
        <p class="text-center"><?php echo l('form.register_form_description') ?></p>
        <form>
            <input type="hidden" name="classid" value="<?php echo $list->id ?>">                    
            <div class="form-group">
                <label for="exampleInputEmail1"><?php echo l('form.register_form_name') ?> </label>
                <input type="text" name="name" class="form-control" placeholder="Nguyễn Văn A" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1"><?php echo l('form.register_form_phone') ?> </label>
                <input type="text" name="phone" class="form-control" placeholder="0352.638.***" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1"><?php echo l('form.register_form_email') ?> </label>
                <input type="text" name="email" class="form-control" placeholder="info@masterkorean.vn" required>
            </div>
           
            <button style="width: 100%" type="button" class="vs-btn vs-btn-primary vs-submit-form"><?php echo l('form.register_form_submit') ?></button>
            <br>
            <p class="message"></p>
        </form>
    </div>
</div>

<script>
    $(".vs-submit-form").click(function(event){
    event.preventDefault();

    let name = $("input[name=name]").val();
    let email = $("input[name=email]").val();
    let phone = $("input[name=phone]").val();
    let classid = $("input[name=classid]").val();
    
    if ( name == "" || email == "" || phone == "") {
        alert('Vui lòng điền đầy đủ thông tin');
        return;
    }

    $.ajax({
        url: "/apiv2/customer_register.php",
        type:"POST",
        data:{
        name: name,
        email: email,
        phone: phone,
        classid: classid
    },
    success:function(response){
            console.log(response);
            if(response) {
                window.location.href = "/common/ladi/thank-you.php";
            }
        },
    });
});
</script>