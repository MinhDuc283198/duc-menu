<!doctype html>
<html lang="en">
<head>
    <?php include('../../config.php'); ?>
    <?php echo get_visang_config('javascript_header'); ?>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,700;0,900;1,700&display=swap" rel="stylesheet">

    <title>Thank you</title>
    <style>
        *{
            box-sizing:border-box;
            /* outline:1px solid ;*/
        }
        body{
            background: #ffffff;
            background: linear-gradient(to bottom, #ffffff 0%,#e1e8ed 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1e8ed',GradientType=0 );
            height: 100%;
            margin: 0;
            background-repeat: no-repeat;
            background-attachment: fixed;

        }

        .wrapper-1{
            width:100%;
            height:100vh;
            display: flex;
            flex-direction: column;
        }
        .wrapper-2{
            padding :30px;
            text-align:center;
        }
        h1{
            font-family: 'Roboto', cursive;
            font-size:4em;
            letter-spacing:3px;
            color:#5892FF ;
            margin:0;
            margin-bottom:20px;
        }
        .wrapper-2 p{
            margin:0;
            font-size:1.3em;
            font-weight: bold;
            color:#aaa;
            font-family: 'Roboto', sans-serif;
            letter-spacing:1px;
        }
        .go-home{
            color:#fff;
            background:#5892FF;
            border:none;
            padding:10px 50px;
            margin:30px 0;
            border-radius:30px;
            text-transform:capitalize;
            box-shadow: 0 10px 16px 1px rgba(174, 199, 251, 1);
        }
        .footer-like{
            margin-top: auto;
            background:#D7E6FE;
            padding:6px;
            text-align:center;
        }
        .footer-like p{
            margin:0;
            padding:4px;
            color:#5892FF;
            font-family: 'Roboto', sans-serif;
            letter-spacing:1px;
        }
        .footer-like p a{
            text-decoration:none;
            color:#5892FF;
            font-weight:600;
        }

        @media (min-width:360px){
            h1{
                font-size:4.5em;
            }
            .go-home{
                margin-bottom:20px;
            }
        }

        @media (min-width:600px){
            .content{
                max-width:1000px;
                margin:0 auto;
            }
            .wrapper-1{
                height: initial;
                max-width:620px;
                margin:0 auto;
                margin-top:50px;
                box-shadow: 4px 8px 40px 8px rgba(88, 146, 255, 0.2);
            }

        }
    </style>
</head>
<body>
<?php echo get_visang_config('javascript_body'); ?>
<div class=content>
    <div class="wrapper-1">
        <div class="wrapper-2">
            <h1>Cảm ơn !</h1>
            <p>Cảm ơn bạn đã đăng ký mua khóa học!  </p>
            <p>Master Korean sẽ liên hệ với bạn ngay.  </p>
            <a href="https://www.masterkorean.vn">
                <button class="go-home">
                        Trang chủ
                </button>
            </a>

        </div>
        <div class="footer-like">
            <p>Facebook:
                <a href="https://www.facebook.com/masterkoreanvietnam">www.facebook.com/masterkoreanvietnam</a>
            </p>
            <p>Website:
                <a href="https://www.masterkorean.vn">https://www.masterkorean.vn</a>
            </p>
            <p>Email:
                <a href="mailto:visang@masterkorean.vn">visang@masterkorean.vn</a>
            </p>
            <p>SĐT:
                <a href="tel:02436886333">0243-6886-333</a>
            </p>
        </div>
    </div>
</div>
<?php echo get_visang_config('javascript_footer'); ?>
</body>
</html>