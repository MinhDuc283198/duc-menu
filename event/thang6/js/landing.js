var domain = document.domain;
var protocol = window.location.protocol;
if (domain == "127.0.0.1") {
  domain = protocol + "//" + domain + ":8000/";
}
else if (domain == "localhost")
  domain = protocol + '//' + domain + '/landing_visang2/';
else {
  domain = protocol + "//" + domain + "/";
}
var _token = $('meta[name="csrf-token"]').attr('content');
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$('document').ready(function(){
  $('html, body').animate({
    scrollTop: 1
  });
});
$('#btn_reg').click(function(){
  var phone = $('#phone_reg').val();
  var email = $('#email_reg').val();
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if(phone == '' || email == ''){
    thongbao('Chưa điền đủ thông tin','error');
    return;
  }
  if(!email.match(mailformat)){
    thongbao('Email sai định dạng','error');
    return;
  }
  if(isNaN(phone)){
    thongbao('Sai kiểu số điện thoại','error');
    return;
  }
  if(phone.length < 10){
    thongbao('Số điện thoại quá ngắn','error');
    return;
  }
  var data={
        phone:phone,
        email:email
      }
  $.ajax({
    url:window.location+'function.php',
    type:"POST",
    dataType: "json",
    data:data,
  }).done(function(data){
    console.log(data);
    if(data.error){
      thongbao(data.error,'error');
    }else{
      window.location.replace(window.location.href+'/thank_you');
    }
  });
});
header_height = $('#header').height();
$(window).scroll(function(){
  $('.scroll-animations .animated').each(function() {
    if (isScrolledIntoView(this) === true) {
      $(this).css('opacity',1);
      $(this).addClass($(this).attr('animate'));
    }
  });
});
function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  var elemTop = $(elem).offset().top;
  // var elemBottom = $(elem).offset().top;
  var elemBottom = elemTop + ($(elem).height()/2);

  return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

//toastr
toastr.options={
  "positionClass": "toast-bottom-right",
}
thongbao = (string = 'xử lý dữ liệu thành công',type='success') => {
  if(type=='success'){
    toastr.success(string);
  }
  if(type=='error'){
    toastr.error(string);
  }
  if(type=='info'){
    toastr.info(string);
  }
  if(type=='warning'){
    toastr.warning(string);
  }
}