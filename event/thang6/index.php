<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" itemprop="thumbnailUrl" content="https://www.masterkorean.vn/event/thang6/images/han-business.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="337">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.masterkorean.vn/event/thang6/"/>
    <meta property="og:title" content="Tiếng Hàn Business - Giảng viên Ninh Thị Thúy" />
    <meta property="og:description" content="Cơ hội thăng tiến, nâng cao thu nhập tại Công ty Hàn Quốc trong tầm tay" />
    <title>Tiếng Hàn Business - Giảng viên Ninh Thị Thúy</title>
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/toastr.min.css" rel="stylesheet">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link href="css/landing.css" rel="stylesheet">
</head>
<body>
    <div id="header">
        <div id="frame_header" class="max1145">
            <img src="images/logow.png" alt="">
            <h1 class="animated zoomInDown" style="opacity:1 !important">Khóa học <br>Tiếng Hàn Business</h1>
            <p class="animated fadeInLeft">Cơ hội thăng tiến, nâng cao thu nhập tại Công ty Hàn Quốc trong tầm tay</p>
            <p style="margin-bottom:65px" class="animated fadeInRight">Giảng viên: Ninh Thị Thúy</p>
        </div>
    </div>
    <div id="middle" class="scroll-animations">
        <div id="customer" class="max1145">
            <div class="row">
                <div class="col-md-6" id="customer_left">
                    <h2 class="animated" animate="fadeInDown">Đối tượng nghe giảng</h2>
                    <div class="each_type_cus animated" animate="fadeInLeft">
                        <p class="float-left" style="margin-right:5px">-</p>
                        <p>Ứng viên chuẩn bị ứng tuyển vào các công ty Hàn Quốc.</p>
                    </div>
                    <div class="each_type_cus animated" animate="fadeInLeft">
                        <p class="float-left" style="margin:0 5px 50px 0;">-</p>
                        <p>Nhân viên đang làm việc tại công ty Hàn Quốc nhưng muốn nâng cao khả năng giao tiếp Tiếng Hàn nơi công sở.  </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="images/customer.jpg" alt="" class="animated" animate="fadeInUp">
                </div>
            </div>
        </div>
        <div id="reason_business">
            <div id="business_frame">
                <h2 class="animated" animate="fadeInDown">Lý do lựa chọn khóa học <br>Tiếng Hàn Business </h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card text-left animated" animate="fadeInLeft">
                            <div class="card-body">
                                <div class="title_reason_business">
                                    <img src="images/566_04.png" alt="">
                                    <h3>Thực tế</h3>
                                </div>
                                <p>Các đoạn phim ngắn giúp học viên gián tiếp trải nghiệm những tình huống thực tế tại công ty Hàn Quốc.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-left animated" animate="fadeInUp">
                            <div class="card-body">
                                <div class="title_reason_business">
                                    <img src="images/566_05.png" alt="">
                                    <h3>Thuận lợi</h3>
                                </div>
                                <p>Thuận lợi khi xin việc vào doanh nghiệp Hàn Quốc từ vòng phỏng vấn, cho đến khi làm việc chính thức.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-left animated" animate="fadeInRight">
                            <div class="card-body">
                                <div class="title_reason_business">
                                    <img src="images/566_06.png" alt="">
                                    <h3>Giao tiếp</h3>
                                </div>
                                <p>Dễ dàng giao tiếp trôi chảy với đồng nghiệp và cấp trên người Hàn, có thêm cơ hội thăng tiến và nâng cao thu nhập.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="reason_choose">
            <h2 class="animated" animate="zoomIn">Lý do lựa chọn Master Korean</h2>
            <div class="row">
                <div class="col-md-3 animated" animate="fadeInRight">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason1.png" alt="">
                        </div>
                        <h3>Thương hiệu uy tín</h3>
                        <p>Đến từ Visang - Tập đoàn giáo dục hàng đầu đến từ Hàn Quốc.</p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInDown">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason2.jpg" alt="">
                        </div>
                        <h3>Hỗ trợ nhiệt tình </h3>
                        <p>Được tư vấn trực tiếp 1:1 với giảng viên lâu năm của Master Korean.</p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInUp">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason3.jpg" alt="">
                        </div>
                        <h3>Cách học dễ dàng</h3>
                        <p>Chỉ cần một thiết bị có kết nối internet, bạn có thể học Tiếng Hàn với Master Korean dễ dàng ở bất cứ đâu, bất kỳ lúc nào.</p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInLeft">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason4.png" alt="">
                        </div>
                        <h3>Tìm việc nhanh chóng</h3>
                        <p>Master Korean liên kết trực tiếp với Master Korean Jobs, giúp bạn nhận đề xuất công việc chất lượng từ các công ty Hàn Quốc.</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="about_teacher">
            <div id="frame_about_teacher" class="max1145 animated" animate="fadeIn">
                <div class="row">
                    <div class="col-md-6 d-flex align-items-center">
                        <div id="about_teacher_text" class="animated" animate="fadeInRight">
                            <h2>Giới thiệu giảng viên</h2>
                            <p style="margin-bottom:50px">Giảng viên Ninh Thị Thúy</p>
                            <p>
                                - Thạc sĩ khoa Phúc lợi xã hội, Đại học nữ sinh Ewha.<br>
                                - Cử nhân khoa Tiếng Hàn Quốc, tại Đại học Hà Nội.<br>
                                - Giảng viên Trung tâm tiếng Hàn, Đại học Công nghiệp Hà Nội.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div style="padding:0 5px;" class="animated" animate="fadeInLeft">
                            <img src="images/about_teacher.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <h3 class="animated" animate="heartBeat"> Hãy đón chờ các video bài giảng<br>Tiếng Hàn Business vào Tháng 6 - 2020!</h3>
        </div>
        <div id="send_info">
            <div class="max1145">
                <div class="row">
                    <div class="col-md-4 animated" animate="fadeInLeft">
                        <div style="padding:0 10%">
                            <input type="text" placeholder="Số điện thoại*" id="phone_reg">
                            <hr>
                        </div>
                    </div>
                    <div class="col-md-4 animated" animate="fadeInDown">
                        <div style="padding:0 10%">
                            <input type="text" placeholder="Email của bạn*" id="email_reg">
                            <hr>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex justify-content-center" animate="fadeInRight">
                        <div id="outline_button">
                            <button id="btn_reg">Đăng ký ngay</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/toastr.min.js"></script>
    <!-- <script src="fontawesome/js/all.js"></script> -->
    <script src="js/landing.js"></script>
</body>
</html>
<?php
// $to      = 'g.nam256@gmail.com';
// $subject = 'the subject';
// $message = 'hello';
// $headers = 'From: tkchoigame256@masterlorean.vn' . "\r\n" .
//     'Reply-To: webmaster@example.com' . "\r\n" .
//     'X-Mailer: PHP/' . phpversion();

// mail($to, $subject, $message, $headers);
?> 