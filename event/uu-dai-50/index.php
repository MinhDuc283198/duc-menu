<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" itemprop="thumbnailUrl" content="images/han-business1.png">
    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="310" />
    <meta property="og:description" content="Nhanh tay lên nào ! Chương trình ưu đãi đặc biệt 50% các khóa học của Masterkorean !">
    <title>Master Korean | Ưu đãi 50% các khóa học</title>
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="css/toastr.min.css" rel="stylesheet"> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link href="css/landing.css" rel="stylesheet">
</head>
<body>
    <div id="header">
        <img src="images/banner.png">
    </div>
    <div id="middle" class="scroll-animations">
        <div id="off_50">
            <div style="background:url('images/bg2.png');padding:30px 0 40px;">
                <h2 class="text-center animated" animate="zoomInDown">Ưu đãi 50% trọn bộ khóa học Tiếng Hàn <br> từ Master Korean </h2>
                <div style="max-width:850px;margin:0 auto;">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="padding10horizontal">
                                <p class="animated" animate="bounceInLeft">Các khóa học được xây dựng dựa theo giáo trình Tiếng Hàn Visang Hàn Quốc, được các giáo viên thạc sĩ, tiến sĩ của Việt Nam và Hàn Quốc giảng dạy.</p>
                                <ul>
                                    <li class="animated" animate="fadeInLeft"><a href="https://www.masterkorean.vn/local/course/detail.php?id=87" target="_blank"><b>Tiếng Hàn Sơ cấp 1</b></a> |100 video|<span>1.200.000</span> nay <b style="color:red">600.000 đ</b></li>
                                    <li class="animated" animate="fadeInLeft"><a href="https://www.masterkorean.vn/local/course/detail.php?id=88" target="_blank"><b>Tiếng Hàn Sơ cấp 2</b></a> |100 video|<span>1.200.000</span> nay <b style="color:red">600.000 đ</b></li>
                                    <li class="animated" animate="fadeInLeft"><a href="https://www.masterkorean.vn/local/course/detail.php?id=89" target="_blank"><b>Tiếng Hàn Trung cấp 1</b></a> |80 video|<span>1.200.000</span> nay <b style="color:red">600.000 đ</b></li>
                                    <li class="animated" animate="fadeInLeft"><a href="https://www.masterkorean.vn/local/course/detail.php?id=90" target="_blank"><b>Tiếng Hàn Trung cấp 2</b></a> |80 video|<span>1.200.000</span> nay <b style="color:red">600.000 đ</b></li>
                                    <li class="animated" animate="fadeInLeft"><a href="https://www.masterkorean.vn/local/course/detail.php?id=94" target="_blank"><b>Luyện thi TOPIK</b></a> |43 video|<span>1.500.000</span> nay <b style="color:red">750.000 đ</b></li>
                                    <li class="animated" animate="fadeInLeft"><a href="https://www.masterkorean.vn/local/course/list.php?id=5" target="_blank"><b>Tiếng Hàn Business</b></a> |24 video|<span>800.000</span> nay <b style="color:red">400.000 đ</b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="padding10horizontal">
                                <img src="images/50off1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="reason_choose">
            <h2 class="animated" animate="zoomIn">Lý do lựa chọn Master Korean</h2>
            <div class="row">
                <div class="col-md-3 animated" animate="fadeInRight">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason1.png" alt="">
                        </div>
                        <h3>Thương hiệu uy tín</h3>
                        <p>Đến từ Visang - Tập đoàn giáo dục hàng đầu đến từ Hàn Quốc.</p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInDown">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason2.jpg" alt="">
                        </div>
                        <h3>Hỗ trợ nhiệt tình </h3>
                        <p>Được tư vấn trực tiếp 1:1 với giảng viên lâu năm của <a href="https://www.masterkorean.vn/" target="_blank">Master Korean.</a></p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInUp">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason3.jpg" alt="">
                        </div>
                        <h3>Cách học dễ dàng</h3>
                        <p>Chỉ cần một thiết bị có kết nối internet, bạn có thể học Tiếng Hàn với <a href="https://www.masterkorean.vn/" target="_blank">Master Korean</a> dễ dàng ở bất cứ đâu, bất kỳ lúc nào.</p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInLeft">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason4.png" alt="">
                        </div>
                        <h3>Tìm việc nhanh chóng</h3>
                        <p><a href="https://www.masterkorean.vn/" target="_blank">Master Korean</a> liên kết trực tiếp với <a href="https://job.masterkorean.vn/" target="_blank">Master Korean Jobs</a>, giúp bạn nhận đề xuất công việc chất lượng từ các công ty Hàn Quốc.</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="register_form">
            <h2 class="text-center animated" animate="fadeInDown">Đăng ký mua các khóa học ưu đãi 50% ngay!</h2>
            <p class="text-center animated" animate="fadeIn">Master Korean sẽ gán khóa học và liên hệ ngay sau khi bạn hoàn tất thông tin đăng ký dưới đây.</p>
            <div id="register_info" class="animated" animate="flipInY">
                <label for="">Họ Tên</label>
                <input type="text" name="" id="register_name" class="form-control">
                <label for="">Email đã đăng ký trên masterkorean.vn *</label>
                <input type="text" name="" id="register_email" class="form-control">
                <label for="">SĐT</label>
                <input type="text" name="" id="register_phone" class="form-control">
                <label for="">Các khóa học đăng ký mua</label>
                <textarea name="" id="register_course" class="form-control"></textarea>
                <div class="text-center" style="margin-top:20px">
                    <button class="btn btn-primary" id="btn_register">Đăng ký ngay</button>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="js/toastr.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- <script src="fontawesome/js/all.js"></script> -->
    <script src="js/landing.js"></script>
</body>
</html>