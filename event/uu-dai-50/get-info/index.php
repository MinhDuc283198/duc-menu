<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Get Info Registed</title>
    <link href="../css/animate.min.css" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet"> -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/toastr.min.css" rel="stylesheet">
    <link href="../fontawesome/css/all.css" rel="stylesheet">
    <!-- <link href="../css/getinfo.css" rel="stylesheet"> -->
    <style>
        body{
            background: url("../images/bg5.jpg") center top no-repeat fixed;
            background-size:cover;
        }
    </style>
</head>
<body>
    <div class="text-center">
        <h1 style="color:#fff; margin:20px 0">Danh sách thông tin đăng ký</h1>
        <table class="table table-striped table-inverse" id="infotbl" style="margin:0 auto; max-width:1145px; background:#fff">
            <thead class="thead-inverse">
            <tr>
                <th>STT</th>
                <th>Tên</th>
                <th>SĐT</th>
                <th>Email</th>
                <th>Khóa học</th>
            </tr>
            </thead>
            <tbody>
                <?php
                    $data = read_file('../info.txt');
                    foreach ($data as $key => $value) {
                        echo '<tr><td>'.($key+1).'</td><td>'.$value["name"].'</td><td>'.$value["phone"].'</td><td>'.$value["email"].'</td><td><div style="white-space: pre-line">'.$value["course"].'</div></td></tr>';
                    }
                ?>
            </tbody>
        </table>
        <button class="btn btn-primary" id="excel" style="margin-top:20px">Xuất file Excel</button>
    </div>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.table2excel.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script>
        $('#excel').click(function(){
            $("#infotbl").table2excel({
                exclude:".noExl",
                name:"Worksheet Name",
                filename:"Danh sach dang ky",
                fileext:".xls"
            });
        });
    </script>
</body>
</html>

<?php
    function read_file($file){
        $temp = fopen($file,'r');
        $data_read = fread($temp,filesize($file));
        $data_read = json_decode($data_read,true);
        fclose($temp);
        return $data_read;
    }
?>