var domain = document.domain;
var protocol = window.location.protocol;
if (domain == "127.0.0.1") {
  domain = protocol + "//" + domain + ":8000/";
}
else if (domain == "localhost")
  domain = protocol + '//' + domain + '/landing_visang2/';
else {
  domain = protocol + "//" + domain + "/";
}
var _token = $('meta[name="csrf-token"]').attr('content');
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$('document').ready(function(){
  $('html, body').animate({
    scrollTop: 1
  });
  let reason_height = 0;
  $('.reason_course').each(function(){
    if($(this).height() > reason_height){
      reason_height = $(this).height();
      $('.reason_course').height(reason_height);
    }
  });
});
$('#register_phone,#register_email,#register_name').keyup(function(e){
  if(e.keyCode == 13)
  {
      $('#btn_register').click();
  }
});
$('#btn_register').click(function(){
  var phone = $('#register_phone').val();
  var email = $('#register_email').val();
  const name = $('#register_name').val();
  const course = $('#register_course').val();
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if(phone==''||email==''||name==''||course==''){
    thongbao('Chưa điền đủ thông tin','error');
    return;
  }
  if(!email.match(mailformat)){
    thongbao('Email sai định dạng','error');
    return;
  }
  if(isNaN(phone)){
    thongbao('Sai kiểu số điện thoại','error');
    return;
  }
  if(phone.length < 10){
    thongbao('Số điện thoại quá ngắn','error');
    return;
  }
  $.ajax({
    url:"function.php",
    type:"POST",
    data:{
      email:$('#register_email').val(),
      phone:$('#register_phone').val(),
      name:$('#register_name').val(),
      course:$('#register_course').val(),
    }
  }).done(function(data){
    data = JSON.parse(data);
    if(data.error){
      thongbao(data.error,'error');
    }else{
      window.location = window.location.href+'/thank_you';
    }
  });
});
header_height = $('#header').height();
$(window).scroll(function(){
  $('.scroll-animations .animated').each(function() {
    if (isScrolledIntoView(this) === true) {
      $(this).css('opacity',1);
      $(this).addClass($(this).attr('animate'));
    }
  });
});
function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  var elemTop = $(elem).offset().top;
  // var elemBottom = $(elem).offset().top;
  // var elemBottom = elemTop + ($(elem).height()/2);
  var elemBottom = elemTop + 20;

  return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

//toastr
toastr.options={
  "positionClass": "toast-bottom-right",
}
function thongbao(string = 'xử lý dữ liệu thành công',type='success'){
// thongbao = (string = 'xử lý dữ liệu thành công',type='success') => {
  if(type=='success'){
    // toastr.success(string);
    toastr.success(string);
  }
  if(type=='error'){
    toastr.error(string);
  }
  if(type=='info'){
    toastr.info(string);
  }
  if(type=='warning'){
    toastr.warning(string);
  }
}