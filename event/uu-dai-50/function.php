<?php
require('../../config.php');

include "PHPMailer-master/src/PHPMailer.php";
include "PHPMailer-master/src/Exception.php";
include "PHPMailer-master/src/OAuth.php";
include "PHPMailer-master/src/POP3.php";
include "PHPMailer-master/src/SMTP.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

    $post = $_POST;
    $email = trim($post['email']);
    $user = $DB->get_record("user",array('email'=>$email));
    if($user==false){
        echo json_encode(['error'=>'Email chưa đăng ký']);
        die();
    }
    $data_info = read_file('info.txt');
    array_push($data_info,$post);
    write_file('info.txt',json_encode($data_info));
    try {
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        $mail->SetLanguage("vi", 'PHPMailer-master/language/');
        //Server settings
        $mail->CharSet = 'UTF-8';
        $mail->Encoding = 'base64';
        $mail->SMTPDebug = 2;         
        $mail->Host = 'mail.masterkorean.vn';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'visang@masterkorean.vn';                 // SMTP username
        $mail->Password = 'visang12#$';                           // SMTP password
        $mail->SMTPSecure = '';                        // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 25;                                    // TCP port to connect to
        //Recipients
        $mail->setFrom('visang@masterkorean.vn', 'masterkorean');
        $mail->addAddress('anhlk@masterkorean.vn');     // Add a recipient
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Thông tin đăng ký khóa học event giảm 50% từ '.$email;
        $mail->Body    = '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
        </head>
        <body>
            <b>Họ tên :</b> <br>'.$post['name'].'<br><br>
            <b>Email :</b><br>'.$post['email'].'<br><br>
            <b>SĐT :</b><br>'.$post['phone'].'<br><br>
            <b>Khóa học :</b><br>'.$post['course'].'
        </body>
        </html>
        ';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        $mail->send();
        echo json_encode(['message'=>'Lưu thông tin thành công']);
    } catch (Exception $e) {
        echo json_encode(['error'=>'Lưu thông tin thất bại, vui lòng thử lại']);
        // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
    
    

    function read_file($file){
        $temp = fopen($file,'r');
        $data_read = fread($temp,filesize($file));
        $data_read = json_decode($data_read,true);
        fclose($temp);
        return $data_read;
    }
    function write_file($file,$data){
        $temp = fopen($file,'w');
        fwrite($temp,$data);
        fclose($temp);
    }
?>