<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" itemprop="thumbnailUrl" content="images/han-business.png">
    <title>Master Korean | Ưu đãi 30% các khóa Topik và khóa Tiếng Hàn Business </title>
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/toastr.min.css" rel="stylesheet">
    <!-- <link href="fontawesome/css/all.css" rel="stylesheet"> -->
    <link href="css/landing.css" rel="stylesheet">
</head>
<body>
    <div id="header">
        <img src="images/banner.png">
    </div>
    <div id="middle" class="scroll-animations">
        <div id="two_course">
            <div class="max1145">
                <div class="row">
                    <div class="col-md-6 animated" animate="fadeInRight">
                        <div class="each_course">
                            <img src="images/imgcourse1.png" alt="">
                            <h2>Khóa học Luyện thi TOPIK</h2>
                            <hr>
                            <p><span>1.500.000</span>  nay chỉ <b style="color:red">1.050.000 đ</b></p>
                            <hr>
                            <div class="reason_course">
                                <b>Lý do lựa chọn:</b>
                                <ul>
                                    <li>43 video bài giảng từ giáo trình Tiếng Hàn Visang Hàn Quốc.</li>
                                    <li>Phân tích chi tiết các dạng đề thi Topik.</li>
                                    <li>Hướng dẫn giải đề thi theo từng phần Nghe - Đọc - Viết.</li>
                                    <li>Học với chuyên gia Topik người Việt của Master Korean.</li>
                                    <li>Nắm bắt các mẹo và chiến lược giải đề để đạt điểm số cao trong thời gian ngắn nhất.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="https://www.masterkorean.vn/local/course/detail.php?id=94" target="_blank"><button>Đăng ký ngay</button></a>
                        </div>
                    </div>
                    <div class="col-md-6 animated" animate="fadeInLeft">
                        <div class="each_course">
                            <img src="images/imgcourse2.png" alt="">
                            <h2>Khóa học Tiếng Hàn Business</h2>
                            <hr>
                            <p><span>800.000</span>  nay chỉ <b style="color:red">560.000 đ</b></p>
                            <hr>
                            <div class="reason_course">
                                <b>Lý do lựa chọn:</b>
                                <ul>
                                    <li>24 video bài giảng từ giáo trình Tiếng Hàn Visang Hàn Quốc.</li>
                                    <li>Gồm các đoạn phim ngắn giúp học viên gián tiếp trải nghiệm những tình huống thực tế tại công ty Hàn Quốc.</li>
                                    <li>Thuận lợi khi xin việc vào doanh nghiệp Hàn Quốc từ vòng phỏng vấn, cho đến khi làm việc chính thức.</li>
                                    <li>Dễ dàng giao tiếp trôi chảy với đồng nghiệp và cấp trên người Hàn, có thêm cơ hội thăng tiến và thu nhập.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="https://www.masterkorean.vn/local/course/list.php?id=5" target="_blank"><button>Đăng ký ngay</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="other_course">
            <div class="max1145">
                <h2 class="text-center animated" animate="zoomInDown">Các khóa học khác từ Master Korean</h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="each_other_course animated" animate="bounceInRight">
                            <img src="images/each_other_1.png" alt="">
                            <div style="clear:both"></div>
                            <a href="https://www.masterkorean.vn/local/course/detail.php?id=87" target="_blank">Tiếng Hàn Sơ cấp 1</a>
                            <p>Học đúng cách ngay từ đầu.<br>100 video bài giảng.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="each_other_course animated" animate="fadeInDown">
                            <img src="images/each_other_2.png" alt="">
                            <div style="clear:both"></div>
                            <a href="https://www.masterkorean.vn/local/course/detail.php?id=88" target="_blank">Tiếng Hàn Sơ cấp 2</a>
                            <p>Lộ trình học tỉ mỉ, đúng cách.<br>100 video bài giảng.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="each_other_course animated" animate="fadeInUp">
                            <img src="images/each_other_3.png" alt="">
                            <div style="clear:both"></div>
                            <a href="https://www.masterkorean.vn/local/course/detail.php?id=89" target="_blank">Tiếng Hàn Trung cấp 1</a>
                            <p>Topik 3 trong tầm tay.<br>80 video bài giảng.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="each_other_course animated" animate="bounceInLeft">
                            <img src="images/each_other_4.png" alt="">
                            <div style="clear:both"></div>
                            <a href="https://www.masterkorean.vn/local/course/detail.php?id=90" target="_blank">Tiếng Hàn Trung cấp 2</a>
                            <p>Dễ dàng chinh phục Topik 4!<br>80 video bài giảng.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="reason_choose">
            <h2 class="animated" animate="zoomIn">Lý do lựa chọn Master Korean</h2>
            <div class="row">
                <div class="col-md-3 animated" animate="fadeInRight">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason1.png" alt="">
                        </div>
                        <h3>Thương hiệu uy tín</h3>
                        <p>Đến từ Visang - Tập đoàn giáo dục hàng đầu đến từ Hàn Quốc.</p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInDown">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason2.jpg" alt="">
                        </div>
                        <h3>Hỗ trợ nhiệt tình </h3>
                        <p>Được tư vấn trực tiếp 1:1 với giảng viên lâu năm của <a href="https://www.masterkorean.vn/" target="_blank">Master Korean.</a></p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInUp">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason3.jpg" alt="">
                        </div>
                        <h3>Cách học dễ dàng</h3>
                        <p>Chỉ cần một thiết bị có kết nối internet, bạn có thể học Tiếng Hàn với <a href="https://www.masterkorean.vn/" target="_blank">Master Korean</a> dễ dàng ở bất cứ đâu, bất kỳ lúc nào.</p>
                    </div>
                </div>
                <div class="col-md-3 animated" animate="fadeInLeft">
                    <div class="each_reason">
                        <div class="w-100" style="overflow:hidden">
                            <img src="images/reason4.png" alt="">
                        </div>
                        <h3>Tìm việc nhanh chóng</h3>
                        <p><a href="https://www.masterkorean.vn/" target="_blank">Master Korean</a> liên kết trực tiếp với <a href="https://job.masterkorean.vn/" target="_blank">Master Korean Jobs</a>, giúp bạn nhận đề xuất công việc chất lượng từ các công ty Hàn Quốc.</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="view_more" class="d-flex align-items-center justify-content-center">
            <div>
                <p>Khóa học yêu thích của bạn ở ngay đây</p>
                <div class="text-center">
                    <a href="https://www.masterkorean.vn/" target="_blank"><button>Tìm hiểu thêm</button></a>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/toastr.min.js"></script>
    <!-- <script src="fontawesome/js/all.js"></script> -->
    <script src="js/landing.js"></script>
</body>
</html>
<?php
// $to      = 'g.nam256@gmail.com';
// $subject = 'the subject';
// $message = 'hello';
// $headers = 'From: tkchoigame256@masterlorean.vn' . "\r\n" .
//     'Reply-To: webmaster@example.com' . "\r\n" .
//     'X-Mailer: PHP/' . phpversion();

// mail($to, $subject, $message, $headers);
?> 