var domain = document.domain;
var protocol = window.location.protocol;
if (domain == "127.0.0.1") {
  domain = protocol + "//" + domain + ":8000/";
}
else if (domain == "localhost")
  domain = protocol + '//' + domain + '/landing_visang2/';
else {
  domain = protocol + "//" + domain + "/";
}
var _token = $('meta[name="csrf-token"]').attr('content');
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$('document').ready(function(){
  $('html, body').animate({
    // scrollTop: 1
  });
  let reason_height = 0;
  $('.reason_course').each(function(){
    if($(this).height() > reason_height){
      reason_height = $(this).height();
      $('.reason_course').height(reason_height);
    }
  });
});
header_height = $('#header').height();
$(window).scroll(function(){
  $('.scroll-animations .animated').each(function() {
    if (isScrolledIntoView(this) === true) {
      $(this).css('opacity',1);
      $(this).addClass($(this).attr('animate'));
    }
  });
});
function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  var elemTop = $(elem).offset().top;
  // var elemBottom = $(elem).offset().top;
  // var elemBottom = elemTop + ($(elem).height()/2);
  var elemBottom = elemTop + 20;

  return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

//toastr
toastr.options={
  "positionClass": "toast-bottom-right",
}
thongbao = (string = 'xử lý dữ liệu thành công',type='success') => {
  if(type=='success'){
    toastr.success(string);
  }
  if(type=='error'){
    toastr.error(string);
  }
  if(type=='info'){
    toastr.info(string);
  }
  if(type=='warning'){
    toastr.warning(string);
  }
}