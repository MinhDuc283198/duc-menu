<?php
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $data_read = read_file('email-phone.txt');
    if(in_array($email,$data_read['email'])|| in_array($phone,$data_read['phone'])){
        echo json_encode(['error'=>'Số điện thoại hoặc email đã tồn tại']);
        die();
    }else{
        $info['email'] = $email;
        $info['phone'] = $phone;
        $data_info = read_file('info.txt');
        array_push($data_info,$info);
        write_file('info.txt',json_encode($data_info));
        array_push($data_read['email'],$email);
        array_push($data_read['phone'],$phone);
        write_file('email-phone.txt',json_encode($data_read));
        echo json_encode(['message'=>'Lưu thông tin thành công']);
    }
    

    function read_file($file){
        $temp = fopen($file,'r');
        $data_read = fread($temp,filesize($file));
        $data_read = json_decode($data_read,true);
        fclose($temp);
        return $data_read;
    }
    function write_file($file,$data){
        $temp = fopen($file,'w');
        fwrite($temp,$data);
        fclose($temp);
    }
?>